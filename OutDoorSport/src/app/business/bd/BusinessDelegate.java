package app.business.bd;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.services.ServiceBase;
import app.services.ServiceLocator;
import app.services.ServiceType;

/**
 * Classe che rappresenta il Business Delegate, ovvero un'astrazione client-side 
 * dei servizi di business. Viene creata una astrazione dove si nascondono i dettagli 
 * implementativi dei servizi di business. Utilizzando il Business Delegate � possibile 
 * ridurre l'accoppiamento tra i client e i servizi offerti dal sistema. 
 * L'obiettivo della classe � quindi quello di incapsulare l'accesso ai vari application service,
 * nascondendo i dettagli di implementazione della strategia di lookup (ritrovamento) e di accesso 
 * ad un servizio.
 * 
 * michele fornelli
 *
 */

class BusinessDelegate extends ServiceBase{
	

	private static BusinessDelegate businessDelegate = new BusinessDelegate();
	private static ServiceBase service = null;
	
	private BusinessDelegate(){
	}
	
	/**
	 * @return restituisce l'istanza del business lookup
	 */
	static BusinessDelegate getInstance(){
		if(service == null){
			try {
				service = ServiceLocator.getService(ServiceType.BusinessLookUp);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return businessDelegate;
	}
	
	/**
	 * Metodo che richiede la ricerca del servizio opportuno, mandando un richiesta alla classe di servizio del
	 * businessLookUp e ricevendo l'opportuna risposta
	 * 
	 * @param richiesta dal quale verr� identificato l'application service opportuno
	 * @return la risposta in base alla richiesta
	 */
	OutdoorResponse lookup(OutdoorRequest request){
		return this.eseguiRichiesta(request);
	}

	@Override
	public ServiceType getType() {
		return null;
	}

	@Override
	protected OutdoorResponse eseguiRichiesta(OutdoorRequest request) {
		return this.handle(request, service);
	}
}
