package app.business.as;

import java.util.List;

import app.integration.dao.EscursioneDAO;
import app.integration.dao.OptionalDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.OptionalTO;
import app.utility.Actions;
import app.utility.Rules;

/**
 * Classe che modella e implementa l'Application Service per la gestione degli Optional.
 * L'obiettivo � quello di raccogliere tutte le azioni che � possibile effettuare per 
 * un Optional, andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * L'application service utilizza i Transfer Object e i Data Access Object per occuparsi
 * della persistenza di tali oggetti, ma anche per il recupero di tali dati dal Database.
 * 
 * @author michele fornelli
 *
 */


class ASGestoreOptional implements Actions,Rules{

	
	private OptionalDAO optionalDAO= null;
	
	public ASGestoreOptional() {
		optionalDAO =  (OptionalDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.OptionalDAO);

	}

	/**
	 * Metodo che restituisce tutti gli optional
	 * 
	 * @param request: richiesta da inviare
	 * @return response: la risposta in base alla richiesta
	 */
	public OutdoorResponse getAllOptionals(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		List<OptionalTO> listaOptional = null;
		try {
			listaOptional = optionalDAO.getAll();
			response.setData(listaOptional);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		return response;
	}
}
