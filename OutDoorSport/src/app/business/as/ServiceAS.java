package app.business.as;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.services.ServiceBase;
import app.services.ServiceType;

/**
 * Classe di servizio, invocata dal Service Locator, per collegare 
 * l'Application Controller al Business Delegate. In questo modo si nascondono 
 * i vari passaggi della richiesta e non possono essere richiamati da altre
 * parti.
 * 
 * @author michele fornelli
 */

public class ServiceAS extends ServiceBase{

	/**
	 * Richiama l'istanza di ApplicationService
	 */
	private static ApplicationService applicationService = ApplicationService.getInstance();

	public ServiceAS(){
	}
	
	@Override
	public ServiceType getType() {
		return ServiceType.ApplicationService;
	}

	@Override
	protected OutdoorResponse eseguiRichiesta(OutdoorRequest request) {
		return applicationService.lookup(request);
	}

	
	protected static String getApplicationService(String suffix) {
		return applicationService.getApplicationService(suffix);

	}
	
	
}
