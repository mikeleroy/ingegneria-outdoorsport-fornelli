package app.business.as;


import java.util.List;

import app.integration.dao.OptionalEscursioneDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.OptionalEscursioneTO;
import app.utility.Actions;
import app.utility.Rules;

/**
 * Classe che modella e implementa l'Application Service per la gestione degli Optional
 * di una determinata Escursione. L'obiettivo � quello di raccogliere tutte le azioni 
 * che � possibile effettuare per un Optional di una Escursione, andando a ridurre 
 * l'accoppiamento con le altre componenti del sistema. L'application service utilizza 
 * i Transfer Object e i Data Access Object per occuparsi della persistenza di tali 
 * oggetti, ma anche per il recupero di tali dati dal Database.
 * 
 * @author michele fornelli
 *
 */


class ASGestoreOptionalEscursione implements Actions,Rules {

	
	private OptionalEscursioneDAO optionalEscursioneDAO = null;
	
	public ASGestoreOptionalEscursione() {
		optionalEscursioneDAO = (OptionalEscursioneDAO)HibFactoryDAO.getFactoryDAO(EnumDAO.OptionalEscursioneDAO);
	}

	/**
	 * Metodo che restituisce tutti gli Optional di una determinata Escursione
	 * 
	 * @param richiesta 
	 * @return risposta in base alla richiesta
	 */
	public OutdoorResponse getAllOptionalEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		OptionalEscursioneTO optionalEscursione = (OptionalEscursioneTO) request.getData();
		
		try {
			List<OptionalEscursioneTO> listaOptionalEscursione = optionalEscursioneDAO.getAllOptionalEscursione(optionalEscursione.getEscursione().getIdEscursione());
			response.setData(listaOptionalEscursione);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		
		return response;
	}
}
