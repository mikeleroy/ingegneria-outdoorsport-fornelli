package app.business.as;

import java.util.ArrayList;
import java.util.List;

import app.integration.dao.StatoEscursioneDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.StatoEscursioneTO;
import app.utility.Actions;
import app.utility.Rules;
import app.utility.Views;

/**
 * Classe che modella e implementa l'Application Service per la gestione di uno StatoEscursione.
 * L'obiettivo � quello di raccogliere tutte le azioni che � possibile effettuare per 
 * uno StatoEscursione, andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * L'application service utilizza i Transfer Object e i Data Access Object per occuparsi
 * della persistenza di tali oggetti, ma anche per il recupero di tali dati dal Database.
 * 
 * @author michele fornelli
 *
 */

class ASGestoreStatoEscursione implements Actions, Views,Rules{

	StatoEscursioneDAO statoEscursioneDAO = null;
	
	public ASGestoreStatoEscursione() {
		if(statoEscursioneDAO == null){
			statoEscursioneDAO = (StatoEscursioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.StatoEscursioneDAO);
		}
	}
	
	/**
	 * Metodo che restituisce tutti gli stati delle escursioni
	 * 
	 * @param request
	 * @return
	 */
	public OutdoorResponse getAllStatoEscursione (OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		List<StatoEscursioneTO> stati = new ArrayList<>();
		
		try {
			stati = statoEscursioneDAO.getAll();
			response.setData(stati);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		
		return response;
	}

}
