package app.business.as;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import app.integration.dao.EscursioneDAO;
import app.integration.dao.IscrizioneDAO;
import app.integration.dao.StatoIscrizioneDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EmailTO;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.ManagerDiEscursioneTO;
import app.to.OptionalEscursioneTO;
import app.to.PartecipanteTO;
import app.to.StatoIscrizioneTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Actions;
import app.utility.EmailSettings;
import app.utility.Rules;
import app.utility.SessionData;
import app.utility.Views;
import javafx.scene.control.TextInputDialog;

/**
 * Classe che modella e implementa l'Application Service per la gestione di Iscrizioni.
 * L'obiettivo � quello di raccogliere tutte le azioni che � possibile effettuare per 
 * una Iscrizione, andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * L'application service utilizza i Transfer Object e i Data Access Object per occuparsi
 * della persistenza di tali oggetti, ma anche per il recupero di tali dati dal Database.
 * 
 * @author michele fornelli
 *
 */

class ASGestoreIscrizione implements Views, Actions,Rules{

	
	private IscrizioneDAO iscrizioneDAO = null;
	private EscursioneDAO escursioneDAO = null;
	private StatoIscrizioneDAO statoIscrizioneDAO = null;
	private EmailTO email = null;
	
	
	public ASGestoreIscrizione() {
		if(iscrizioneDAO == null){
			iscrizioneDAO = (IscrizioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.IscrizioneDAO);
		}
		if(escursioneDAO == null){
			escursioneDAO = (EscursioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.EscursioneDAO);
		}
		if(statoIscrizioneDAO == null){
			statoIscrizioneDAO = (StatoIscrizioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.StatoIscrizioneDAO);
		}
		if(email == null){
			email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);
		}
	}
	
	/**
	 * Metodo che restituisce tutte le iscrizioni
	 * ad una determinata escursione.
	 * 
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse getAllIscrittiFromEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		
		try {
			List<IscrizioneTO> list_iscrizioni = iscrizioneDAO.getAllIscrittiFromEscursione((EscursioneTO)SessionData.getSessionData("Escursione"));
			response.setData(list_iscrizioni);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			e.printStackTrace();
			response.setResponse(FAILED);
		}
		return response;
	}
	
	/**
	 * Metodo che annulla l'iscrizione di
	 * un determinato partecipante
	 * 
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta in base alla richiesta
	 */
	public OutdoorResponse annullaIscrizione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		IscrizioneTO iscrizione = (IscrizioneTO) request.getData();
		try {
			int iscritti = iscrizione.getEscursione().getTotIscritti();
			iscritti--;
			iscrizione.getEscursione().setTotIscritti(iscritti);
			iscrizione.setStatoIscrizione(statoIscrizioneDAO.getStatoDisattivo());
		} catch (DatabaseException e1) {
			e1.printStackTrace();
		}
		
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("OutDoorSports 1.0");
		dialog.setHeaderText("Intendi annullare l'iscrizione del Partecipante?");
		dialog.setContentText("Motivazione:");
		
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			try {
				iscrizione = iscrizioneDAO.annullaIscrizione(iscrizione);
				escursioneDAO.update(iscrizione.getEscursione());
				
				EmailTO email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);

				String mailOggetto = "OutDoorSports | Iscrizione Annullata";
				String mailMessaggio = "Gentile ";
				mailMessaggio += iscrizione.getUtente().getNome() + " " + iscrizione.getUtente().getCognome() + ", \n";
				mailMessaggio += "La tua iscrizione per l'escursione " + iscrizione.getEscursione().getNome();
				mailMessaggio += " � stata cancellata. \n";
				mailMessaggio += "Motivo: " + result.get() + " \n";
				mailMessaggio += "Ci scusiamo per il disagio";

				email.setOggetto(mailOggetto);
				email.setMessaggio(mailMessaggio);

				ArrayList<UtenteTO> listaDestinatari = new ArrayList<>();
				listaDestinatari.add(iscrizione.getUtente());
				email.setDestinatari(listaDestinatari);

				EmailSettings emailSetting = new EmailSettings();
				emailSetting.sendEmail(email);
				
				response.setData(iscrizione);
				response.setResponse(SUCCESS);
			} catch (DatabaseException e) {
				response.setResponse(FAILED);
				e.printStackTrace();
			}
		}
		return response;
	}
	
	/**
	 * Metodo che aggiunge al database gli optional
	 * modificati di una determinata iscrizione a una 
	 * escursione
	 * 
	 * @param richiesta: iscrizione all'escursione
	 * @return risposta in base alla richiesta
	 */
	public OutdoorResponse updateOptionalFromIscrizione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		IscrizioneTO iscrizione = (IscrizioneTO) request.getData();
		PartecipanteTO partecipante = (PartecipanteTO) iscrizione.getUtente();
		
		try {
			iscrizioneDAO.createOrUpdate(iscrizione);
			response.setData(iscrizione);
			response.setResponse(SUCCESS);
			
			EmailTO email = (EmailTO)FactoryTO.getFactoryTO(EnumTO.Email);

			String mailOggetto = "OutDoorSports | Modifica Optional Scelti";
			String mailMessaggio = "Gentile ";
			mailMessaggio += partecipante.getNome() + " " + partecipante.getCognome() + ", \n";
			mailMessaggio += "La informiamo che, per l'escursione " + iscrizione.getEscursione().getNome() + ", ";
			mailMessaggio += "gli optional scelti modificati dal Manager sono: ";
			double costo = iscrizione.getEscursione().getPrezzo();
			for(OptionalEscursioneTO oe : iscrizione.getOptionals()){
				mailMessaggio += oe.getOptional().getNome() + " \n";
				costo = costo + oe.getOptional().getTipoOptional().getPrezzo();
			}
			mailMessaggio += " \n";
			mailMessaggio += "Pertanto il costo totale per la sua escursione, compresi gli optional, sar� di: ";
			mailMessaggio += costo + " �";

			email.setOggetto(mailOggetto);
			email.setMessaggio(mailMessaggio);

			ArrayList<UtenteTO> listaDestinatari = new ArrayList<>();
			listaDestinatari.add(partecipante);
			email.setDestinatari(listaDestinatari);

			EmailSettings emailSettings= new EmailSettings();
			emailSettings.sendEmail(email);
		} catch (DatabaseException e) {
			response.setResponse(SUCCESS);
			e.printStackTrace();
		}
		return response;
	}
	
	/**
	 * Metodo che crea una nuova iscrizione
	 * e inserisce gli optional scelti
	 * 
	 * @param richiesta: iscrizione all'escursione
	 * @return risposta in base alla richiesta
	 */
	public OutdoorResponse createOptionalFromIscrizione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		IscrizioneTO iscrizione = (IscrizioneTO) request.getData();
		PartecipanteTO partecipante = (PartecipanteTO) iscrizione.getUtente();
		ManagerDiEscursioneTO mde = (ManagerDiEscursioneTO) iscrizione.getEscursione().getUtente();
		
		try {
			iscrizione.setUtente(partecipante);
			iscrizione.setData(LocalDate.now().toString());
			LocalTime time = LocalTime.now();
			iscrizione.setOrario(time.getHour() + ":" + time.getMinute() + ":" + time.getSecond());
			iscrizione.setStatoIscrizione(statoIscrizioneDAO.getStatoAttivo());
			IscrizioneTO verifyIscrizione = iscrizioneDAO.getIscrizioneFromEscursione(iscrizione.getEscursione(), (PartecipanteTO) iscrizione.getUtente());
			if(verifyIscrizione == null){
				iscrizione = iscrizioneDAO.create(iscrizione);
				int iscritti = iscrizione.getEscursione().getTotIscritti();
				iscritti++;
				iscrizione.getEscursione().setTotIscritti(iscritti);				
			}else{
				verifyIscrizione.setStatoIscrizione(statoIscrizioneDAO.getStatoAttivo());
				int iscritti = verifyIscrizione.getEscursione().getTotIscritti();
				iscritti++;
				verifyIscrizione.getEscursione().setTotIscritti(iscritti);
				iscrizione = iscrizioneDAO.update(verifyIscrizione);
			}
			escursioneDAO.update(iscrizione.getEscursione());
			
			EmailTO email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);

			String mailOggetto = "OutDoorSports | Il partecipante " + partecipante.getUsername() + "si � iscritto ad una Escursione";
			String mailMessaggio = "Gentile ";
			mailMessaggio += mde.getNome() + " " + mde.getCognome() + ", \n";
			mailMessaggio += "La informiamo Il partecipante " + partecipante.getUsername() + "si � iscritto all'Escursione: \n";
			mailMessaggio += iscrizione.getEscursione().getNome();

			email.setOggetto(mailOggetto);
			email.setMessaggio(mailMessaggio);

			ArrayList<UtenteTO> listaDestinatari = new ArrayList<>();
			listaDestinatari.add(mde);
			email.setDestinatari(listaDestinatari);

			EmailSettings emailSettings = new EmailSettings();
			emailSettings.sendEmail(email);
			
			response.setData(iscrizione);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		return response;
	}
	
	/**
	 * Metodo che restituisce l'iscrizione ad una escursione
	 * per un determinato partecipante
	 * 
	 * @param request
	 * @return response: risposta in base alla richiesta
	 */
	public OutdoorResponse getIscrizioneFromEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		IscrizioneTO iscrizione = (IscrizioneTO) request.getData();
		PartecipanteTO partecipante = (PartecipanteTO) iscrizione.getUtente();
		EscursioneTO escursione = (EscursioneTO) iscrizione.getEscursione();
		
		IscrizioneTO newIscrizione = null;
		try {
			newIscrizione = iscrizioneDAO.getIscrizioneFromEscursione(escursione, partecipante);
			response.setData(newIscrizione);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		return response;
	}
	
	/**
	 * Metodo che cancella una iscrizione da parte di un partecipante
	 * 
	 * @param request
	 * @return response: risposta in base alla richiesta
	 */
	public OutdoorResponse deleteIscrizioneFromEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		IscrizioneTO iscrizione = (IscrizioneTO) request.getData();

		try {
			int iscritti = iscrizione.getEscursione().getTotIscritti();
			iscritti--;
			iscrizione.getEscursione().setTotIscritti(iscritti);
			iscrizioneDAO.annullaIscrizione(iscrizione);
			escursioneDAO.update(iscrizione.getEscursione());
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		
		return response;
	}
	
	/**
	 * Metodo che cancella una iscrizione da parte di un partecipante
	 * 
	 * @param request
	 * @return response: risposta in base alla richiesta
	 */
	public OutdoorResponse checkIscrizione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		IscrizioneTO iscrizione = (IscrizioneTO) request.getData();

		try {
			boolean bool = iscrizioneDAO.checkIscrizione(iscrizione);
			if(!bool)
				response.setResponse(SUCCESS);
			else
				response.setResponse(FAILED);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		
		return response;
	}
	
	/**
	 * Metodo che restituisce tutti gli stati di una iscrizione
	 * 
	 * @param request
	 * @return response: risposta in base alla richiesta
	 */
	public OutdoorResponse getAllStatoEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();

		try {
			List<StatoIscrizioneTO> list_stato_iscrizione = new ArrayList<>();
			list_stato_iscrizione = statoIscrizioneDAO.getAll();
			response.setData(list_stato_iscrizione);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		
		return response;
	}
}
