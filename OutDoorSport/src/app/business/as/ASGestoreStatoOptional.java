package app.business.as;

import java.util.List;

import app.integration.dao.StatoOptionalDAO;
import app.integration.dao.TipoEscursioneDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.StatoOptionalTO;
import app.utility.Actions;
import app.utility.Rules;
import app.utility.Views;

/**
 * Classe che modella e implementa l'Application Service per la gestione di uno StatoOptional.
 * L'obiettivo � quello di raccogliere tutte le azioni che � possibile effettuare per 
 * uno StatoOptional, andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * L'application service utilizza i Transfer Object e i Data Access Object per occuparsi
 * della persistenza di tali oggetti, ma anche per il recupero di tali dati dal Database.
 * 
 * @author michele fornelli
 *
 */

class ASGestoreStatoOptional implements Views, Actions,Rules {

	private StatoOptionalDAO statoOptionalDAO = null;
	
	public ASGestoreStatoOptional() {
		statoOptionalDAO = (StatoOptionalDAO)  HibFactoryDAO.getFactoryDAO(EnumDAO.StatoOptionalDAO);
	}
	
	/**
	 * Restisuisce tutti gli stati possibili per gli Optional
	 * 
	 * @param request
	 * @return response 
	 */
	public OutdoorResponse getAllStatoOptional(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		List<StatoOptionalTO> listaStatiOptional = null;
		
		try {
			listaStatiOptional = statoOptionalDAO.getAll();
			response.setData(listaStatiOptional);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		
		return response;
	}

}

