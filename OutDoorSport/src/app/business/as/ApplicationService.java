package app.business.as;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.services.ServiceBase;
import app.services.ServiceLocator;
import app.services.ServiceType;

/**
 * Classe utilizzata dal Business Delegate per identificare il giusto Application Service 
 * in base alla richiesta inviata
 * 
 * @author michele fornelli
 *
 */

class ApplicationService extends ServiceBase{
	
	
	private static ApplicationService applicationService = new ApplicationService();
	private static ServiceBase service = null;
	
	private ApplicationService(){
	}
	
	/**
	 * @return restituisce l'istanza del BusinessDelegate
	 */
	static ApplicationService getInstance(){
		if(service == null){
			try {
				service = ServiceLocator.getService(ServiceType.ApplicationService);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return applicationService;
	}
	
	
	/**
	 * Metodo che richiede la ricerca del servizio opportuno, mandando un richiesta alla classe di servizio del
	 * businessLookUp e ricevendo l'opportuna risposta
	 * 
	 * @param richiesta dal quale verr identificato l'application service opportuno
	 * @return la risposta in base alla richiesta
	 */
	OutdoorResponse lookup(OutdoorRequest request){
		return this.eseguiRichiesta(request);
	}
	
	
	
	/**
	 * Costante per recuperare il percorso del package
	 */
	private final static String PACKAGE_PATH = ApplicationService.class.getPackage().getName();
	

	/**
	 * Costante per il prefisso dei nomi delle classi GESTORI
	 */
	private final static String APPLICATION_SERVICE_PREFIX = "ASGestore";

	

	/**
	 * Metodo che restituisce il percorso completo dell'application service a cui 
	 * dovr fare riferimento il Business Delegate. 
	 * 
	 * @param suffisso per l'application service
	 * @return il nome completo di package dell'application service
	 */
	protected  String getApplicationService(String suffix) {
		return PACKAGE_PATH + "." + APPLICATION_SERVICE_PREFIX +suffix;	

	}

	@Override
	protected OutdoorResponse eseguiRichiesta(OutdoorRequest request) {
		// TODO Auto-generated method stub
		//Mi devo fermare perch sono arrivato alla fine
		return null;
	}

	@Override
	public ServiceType getType() {
		// TODO Auto-generated method stub
		return null;
	}
}
