package app.business.as;

import javax.xml.bind.ValidationException;

import app.integration.dao.ManagerDiSistemaDAO;
import app.integration.dao.StatoUtenteDAO;
import app.integration.dao.TipoUtenteDAO;
import app.integration.dao.UtenteDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EncryptPasswordTO;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.ManagerDiSistemaTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Actions;
import app.utility.Rules;
import app.utility.Views;

/**
 * <b>Business Tier</b></br>
 * La classe modella e implementa un <b>Application Service</b> e rappresenta il componente:
 * <b>Gestore Manager Di Sistema.</b><br /> 
 * L'obiettivo della classe &egrave; quello di centralizzare ed incapsulare il funzionamento
 * dei servizi andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * Il gestore utilizza una serie di Transfer Object o Data Transfer Object
 * la cui tipologia dipende dal servizio e sfrutta il {@link ManagerDiSistemaDAO} 
 * (Data Access Object) per occuparsi della persistenza di tali oggetti.</br>
 * 
 * @author michele fornelli
 *
 */
class ASGestoreManagerDiSistema extends ASGestoreUtenteBase  implements Views, Actions{

	private ManagerDiSistemaTO mds,temp = null;
	private UtenteDAO<ManagerDiSistemaTO> mdsDAO = null;
	private TipoUtenteDAO tipoUtenteDao = null;
	private StatoUtenteDAO statoUtenteDao = null;
	private EncryptPasswordTO encryptedPassword = null;


	/**
	 * Costruttore che inizializza il DAO del Manager di Escursione
	 */
	public ASGestoreManagerDiSistema() {

		if(mdsDAO == null){
			mdsDAO = (UtenteDAO<ManagerDiSistemaTO>) HibFactoryDAO.getFactoryDAO(EnumDAO.ManagerDiSistemaDAO);
		}
		if(tipoUtenteDao == null){
			tipoUtenteDao = (TipoUtenteDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.TipoUtenteDAO);
		}
		if(statoUtenteDao == null){
			statoUtenteDao = (StatoUtenteDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.StatoUtenteDAO);
		}
		if(mds == null){
			mds = (ManagerDiSistemaTO) FactoryTO.getFactoryTO(EnumTO.ManagerDiSistema);

		}
		if(temp == null){
			temp = (ManagerDiSistemaTO) FactoryTO.getFactoryTO(EnumTO.ManagerDiSistema);
		}

		if(encryptedPassword == null){
			encryptedPassword = (EncryptPasswordTO)  FactoryTO.getFactoryTO(EnumTO.EcryptPassword);
		}


	}
	
	
	
	/**
	 * Metodo che restituisce la risposta rispetto alla richiesta di modificare un partecipante
	 * esistente.
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse modificaAccessoUtente(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		
		mds = (ManagerDiSistemaTO)request.getData();
		ManagerDiSistemaTO temp = null;
		int id_temp = -1;
		
		try {
			temp = mdsDAO.getByEmail(mds.getEmail());
			if(temp.getIdUtente() != null)
				id_temp = temp.getIdUtente();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		
		try {
			if(!(mdsDAO.esisteEmail(mds) || temp == null) || (mdsDAO.esisteEmail(mds) 
					&& mds.getIdUtente() == id_temp)){
				
				
				UtenteTO findUser = mdsDAO.getByID(mds.getIdUtente());
				if(!mds.getPassword().equals(findUser.getPassword())){
					String ePassword = encryptedPassword.encryptPassword(mds.getPassword());
					mds.setPassword(ePassword);
				}
				
				temp.setUsername(mds.getUsername());
				
				mdsDAO.update(temp);
				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			}
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	

	
	
}