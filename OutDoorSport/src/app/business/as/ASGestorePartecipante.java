package app.business.as;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import app.integration.dao.EscursioneDAO;
import app.integration.dao.StatoUtenteDAO;
import app.integration.dao.TipoUtenteDAO;
import app.integration.dao.UtenteDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EmailTO;
import app.to.EncryptPasswordTO;
import app.to.EnumTO;
import app.to.EscursioneTO;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import app.to.PartecipanteTO;
import app.to.TipoUtenteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Actions;
import app.utility.EmailSettings;
import app.utility.Rules;
import app.utility.Views;

class ASGestorePartecipante extends ASGestoreUtenteBase implements Views, Actions{

	private UtenteDAO<PartecipanteTO> pDao = null;
	private TipoUtenteDAO tipoUtenteDao = null;
	private StatoUtenteDAO statoUtenteDao = null;
	private PartecipanteTO partecipante, temp = null;
	private EncryptPasswordTO encryptedPassword = null;

	
	
	/**
	 * Costruttore che inizializza il DAO dell'Utente
	 */
	@SuppressWarnings("unchecked")
	public ASGestorePartecipante() {
		if(pDao == null){
			pDao = (UtenteDAO<PartecipanteTO>) HibFactoryDAO.getFactoryDAO(EnumDAO.PartecipanteDAO);
		}
		if(tipoUtenteDao == null){
			tipoUtenteDao = (TipoUtenteDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.TipoUtenteDAO);
		}
		if(statoUtenteDao == null){
			statoUtenteDao = (StatoUtenteDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.StatoUtenteDAO);
		}
		if(partecipante == null){
			partecipante = (PartecipanteTO) FactoryTO.getFactoryTO(EnumTO.Partecipante);

		}
		if(temp == null){
			temp = (PartecipanteTO) FactoryTO.getFactoryTO(EnumTO.Partecipante);
		}
		
		if(encryptedPassword == null){
			encryptedPassword = (EncryptPasswordTO)  FactoryTO.getFactoryTO(EnumTO.EcryptPassword);
		}
	}
	
	
	/**
	 * Metodo che permette la creazione e l'inserimento di un nuovo partecipante all'interno del sistema,
	 * controllando se il partecipante stesso non � gi� stato inserito nel sistema
	 * @param request: richiesta in ingresso
	 * @return response: responso dell'operazione
	 */
	public OutdoorResponse nuovoPartecipante(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();

		try {
			if(!pDao.esisteEmail((UtenteTO)request.getData()) && 
					!pDao.esisteUsername((UtenteTO)request.getData()))
			{
				
				
				partecipante = (PartecipanteTO)request.getData();
				partecipante.setTipoUtente(tipoUtenteDao.getTipoUtentePar());
				partecipante.setStatoUtente(statoUtenteDao.getStatoAttivo());
				
				String password = partecipante.getPassword();
				partecipante.setPassword(encryptedPassword.encryptPassword(password));
				partecipante.setPassword(password);
				uploadCertificatoSRC(partecipante);

				pDao.create(partecipante);
				
				EmailTO email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);

				String oggetto = "OutDoorSports - Registrazione nuovo partecipante";
				String messaggio = "Gentile ";
				messaggio += partecipante.getNome() + " " + partecipante.getCognome() + ", \n";
				messaggio += "La registrazione � avvenuta con successo! I suoi dati di accesso sono: \n";
				messaggio += "Username: " + partecipante.getUsername() + "\n";
				messaggio += "Password: " + password + "\n";

				email.setOggetto(oggetto);
				email.setMessaggio(messaggio);

				ArrayList<UtenteTO> destinatari = new ArrayList<>();
				destinatari.add(partecipante);
				email.setDestinatari(destinatari);

				EmailSettings eSettings = new EmailSettings();
				
				Alert alert = new Alert(AlertType.INFORMATION, "Il Partecipante � stato inserito correttamente!", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");
				
				Optional<ButtonType> res = alert.showAndWait();
				
				if(res.get() == ButtonType.OK)
					eSettings.sendEmail(email);
					
				response.setResponse(SUCCESS);
			}
			else{
				response.setResponse(FAILED);
			}
			
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

		return response;
	}
	
	
	
	
	/**
	 * Metodo che restituisce la risposta rispetto alla richiesta di modificare un partecipante
	 * esistente.
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse modificaPartecipante(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		
		partecipante = (PartecipanteTO)request.getData();
		UtenteTO temp = null;
		int id_temp = -1;
		
		try {
			temp = pDao.getByEmail(partecipante.getEmail());
			if(temp.getIdUtente() != null)
				id_temp = temp.getIdUtente();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		
		try {
			if(!(pDao.esisteEmail(partecipante) || temp == null) || (pDao.esisteEmail(partecipante) 
					&& partecipante.getIdUtente() == id_temp)){
				
				partecipante.setTipoUtente(tipoUtenteDao.getTipoUtentePar());
				partecipante.setStatoUtente(statoUtenteDao.getStatoAttivo());
				
				PartecipanteTO partecipante_into_db = pDao.getByID(partecipante.getIdUtente());
				if(!partecipante.getPassword().equals(partecipante_into_db.getPassword())){
					String newPassword = encryptedPassword.encryptPassword(partecipante.getPassword());
					partecipante.setPassword(newPassword);
				}
				
				uploadCertificatoSRC(partecipante);
				pDao.update(partecipante);
				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			}
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	
	
	/**
	 * Metodo che permette la ricerca del file da caricare e ne salva
	 * il path.
	 * 
	 * @param request: richiesta in ingresso
	 * @return response: una response in base alla request
	 */
	public OutdoorResponse caricaCertificatoSRC(OutdoorRequest request){
		partecipante = (PartecipanteTO) request.getData();
		OutdoorResponse response = new OutdoorResponse();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Carica Certificato SRC");
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("TXT", "*.txt"));
		File fileCertificatoSRC = fileChooser.showOpenDialog(ViewsCache.getCurrentView());
		if(fileCertificatoSRC != null){
			partecipante.setFileSrc(fileCertificatoSRC.getPath());
			response.setResponse(SUCCESS);
		}else
			response.setResponse(FAILED);
		response.setData(partecipante);

		return response;
	}
	
	
	
	/**
	 * Metodo che crea, se non � stata gi� creata, la directory principale, e crea la directory
	 * avente il nome dell'username, che conterr� il certificato SRC. Quindi salva il 
	 * certificato SRC.
	 * 
	 * @param username
	 */
	private void uploadCertificatoSRC(PartecipanteTO partecipante){
		File rootDir = new File(FOLDER_CERTIFICATE);
		if (!rootDir.exists()) {
			try{
				rootDir.mkdir();
			}catch(SecurityException se){
				se.printStackTrace();
			}        
		}

		String path = FOLDER_CERTIFICATE + "\\" + partecipante.getUsername();
		File userCertificateDir = new File(path);
		if (!userCertificateDir.exists()) {
			try{
				userCertificateDir.mkdir();
			}catch(SecurityException se){
				se.printStackTrace();
			}        
		}else
			for(File file: userCertificateDir.listFiles()) 
				if(!file.isDirectory()) 
					if(!file.getPath().equals(partecipante.getFileSrc())) 
						file.delete();
				

		try{
			File source = new File(partecipante.getFileSrc());
			File dest = new File(userCertificateDir.getPath() + "\\" + source.getName());
			Files.copy(source.toPath(), dest.toPath());
			
			partecipante.setFileSrc((dest.getPath()));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Metodo che restituisce la risposta rispetto alla richiesta di modificare un partecipante
	 * esistente.
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse modificaAccessoUtente(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		
		partecipante = (PartecipanteTO)request.getData();
		PartecipanteTO temp = null;
		int id_temp = -1;
		
		try {
			temp = pDao.getByEmail(partecipante.getEmail());
			if(temp.getIdUtente() != null)
				id_temp = temp.getIdUtente();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		
		try {
			if(!(pDao.esisteEmail(partecipante) || temp == null) || (pDao.esisteEmail(partecipante) 
					&& partecipante.getIdUtente() == id_temp)){
				
				
				UtenteTO findUser = pDao.getByID(partecipante.getIdUtente());
				if(!partecipante.getPassword().equals(findUser.getPassword())){
					String ePassword = encryptedPassword.encryptPassword(partecipante.getPassword());
					partecipante.setPassword(ePassword);
				}
				
				temp.setUsername(partecipante.getUsername());
				
				pDao.update(temp);
				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			}
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	/**
	 * Metodo che restituisce tutte le escursione a cui il partecipante � iscritto
	 * @param request
	 * @return response: risposta in base alla richiesta
	 */
	public OutdoorResponse getAllEscursioniPAR(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		try {
			PartecipanteTO partecipante = (PartecipanteTO) request.getData();
			EscursioneDAO escursioneDAO = (EscursioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.EscursioneDAO);
			List<EscursioneTO> list_escursioni = escursioneDAO.getEscursioniPAR(partecipante);
			response.setData(list_escursioni);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			e.printStackTrace();
			response.setResponse(FAILED);
		}
		return response;
	}
	
	
	
	
	
	
}
