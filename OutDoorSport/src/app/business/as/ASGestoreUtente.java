package app.business.as;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import app.integration.dao.ManagerDiSistemaDAO;
import app.integration.dao.UtenteDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.BaseTO;
import app.to.EmailTO;
import app.to.EncryptPasswordTO;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.ManagerDiSistemaTO;
import app.to.PartecipanteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Actions;
import app.utility.EmailSettings;
import app.utility.RandomStringGenerator;
import app.utility.Rules;
import app.utility.SessionData;
import app.utility.Views;
import javafx.stage.FileChooser;

/**
 * <b>Business Tier</b></br>
 * La classe modella e implementa un <b>Application Service</b> e rappresenta il componente:
 * <b>Gestore Utente.</b><br /> 
 * L'obiettivo della classe &egrave; quello di centralizzare ed incapsulare il funzionamento
 * dei servizi andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * Il gestore utilizza una serie di Transfer Object o Data Transfer Object
 * la cui tipologia dipende dal servizio e sfrutta il {@link UtenteDAO} 
 * (Data Access Object) per occuparsi della persistenza di tali oggetti.</br>
 * 
 * @author michele fornelli
 *
 */


class ASGestoreUtente implements Views, Actions,Rules{

 
	private UtenteDAO<UtenteTO> uDao = null;
	private UtenteTO utente, temp = null;
	private EncryptPasswordTO encryptedPassword = null;

	/**
	 * Costruttore che inizializza il DAO dell'Utente
	 */
	@SuppressWarnings("unchecked")
	public ASGestoreUtente() {
		if(uDao == null){
			uDao = (UtenteDAO<UtenteTO>) HibFactoryDAO.getFactoryDAO(EnumDAO.UtenteDAO);
		}
		if(utente == null){
			utente = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente);
		}
		if(temp == null){
			temp = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente);
		}
		
		if(encryptedPassword == null){
			encryptedPassword = (EncryptPasswordTO)  FactoryTO.getFactoryTO(EnumTO.EcryptPassword);
		}
	}

	/**
	 * Autenticazione dell'Utente.
	 * 
	 * @param richiesta che contiene i dati da controllare per l'autenticazione
	 * @return la risposta in base alla richiesta. Se l'autenticazione va a buon fine viene restituito un Utente
	 */
	public OutdoorResponse actionLogin(OutdoorRequest request){
		OutdoorResponse risposta = new OutdoorResponse();
		try {
			
			temp = (UtenteTO) request.getData();
			
			String password = temp.getPassword();

			temp.setPassword(encryptedPassword.encryptPassword(password));
			
			utente = uDao.getUtente(temp);
					
			if(utente.getIdUtente() != null){
//				BaseTO newUtente = checkUserTipe(utente);	
//				response.setData(newUtente);
				
				risposta.setData(utente);
				risposta.setResponse(SUCCESS);
//				
				if(utente instanceof ManagerDiSistemaTO)
					risposta.setView(VIEW_DASHBOARD_MAN_SISTEMA);
				if(utente instanceof ManagerDiEscursioneTO)
					risposta.setView(VIEW_DASHBOARD_MAN_ESCURSIONE);
				if(utente instanceof PartecipanteTO)
					risposta.setView(VIEW_DASHBOARD_PARTECIPANTE);
			}else{
				risposta.setResponse(FAILED);
			}
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		return risposta;
	}
	
	
	/**
	 * Metodo che restituisce la risposta rispetto alla richiesta di modificare un partecipante
	 * esistente.
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse modificaAccessoUtente(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		
		utente = (UtenteTO)request.getData();
		UtenteTO temp = null;
		int id_temp = -1;
		
		try {
			temp = uDao.getByEmail(utente.getEmail());
			if(temp.getIdUtente() != null)
				id_temp = temp.getIdUtente();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		
		try {
			if(!(uDao.esisteEmail(utente) || temp == null) || (uDao.esisteEmail(utente) 
					&& utente.getIdUtente() == id_temp)){
				
				
				UtenteTO findUser = uDao.getByID(utente.getIdUtente());
				if(!utente.getPassword().equals(findUser.getPassword())){
					String newPassword = encryptedPassword.encryptPassword(utente.getPassword());
					utente.setPassword(newPassword);
				}
				
				temp.setUsername(utente.getUsername());
				
				uDao.update(temp);
				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			}
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		return response;
	}

	/**
	 * Metodo che verifica se l'utente � registrato al sistema e pu� resettare la nuova password
	 * 
	 * @param richiesta da eseguire
	 * @return risultato della richiesta
	 */
	public OutdoorResponse recuperaPassword(OutdoorRequest request){
		utente = (UtenteTO) request.getData();
		OutdoorResponse response = new OutdoorResponse();

		try {
			if(uDao.esisteEmail(utente)){
				UtenteTO findUser = uDao.getByEmail(utente.getEmail());
				response.setResponse(SUCCESS);
				response.setData(findUser);
			}else
				response.setResponse(FAILED);
			
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

		return response;
	}
	
	
	
	
//
//	/**
//	 * Metodo privato che identifica il tipo di utente che si � autenticato
//	 * 
//	 * @param utente
//	 * @return una nuova istanza del tipo di utente che si � autenticato
//	 */
//
//	private OutDoorSports checkUserTipe(UtenteTO utente){
//
//		OutDoorSports result = null;
//
//		Partecipante_DAO partecipante_dao = (Partecipante_DAO) DAOFact.getUtenteDAO(UtenteDAOEnum.Partecipante);
//		MDS_DAO mds_dao = (MDS_DAO) DAOFact.getUtenteDAO(UtenteDAOEnum.ManagerDiSistema);
//		MDE_DAO mde_dao = (MDE_DAO) DAOFact.getUtenteDAO(UtenteDAOEnum.ManagerDiEscursione);
//
//		PartecipanteTO partecipante = (PartecipanteTO) TOFact.getUtenteTO(UtenteEnum.Partecipante);
//		ManagerDiSistemaTO mds = (ManagerDiSistemaTO) TOFact.getUtenteTO(UtenteEnum.ManagerDiSistema);
//		ManagerDiEscursioneTO mde = (ManagerDiEscursioneTO) TOFact.getUtenteTO(UtenteEnum.ManagerDiEscursione);
//
//		try {
//			partecipante = partecipante_dao.findOne(utente.getIdUtente());
//			if(partecipante != null){
//				this.setData(partecipante, utente);
//				result = partecipante;
//			}
//			mds = mds_dao.findOne(utente.getIdUtente());
//			if(mds != null){
//				this.setData(mds, utente);
//				result = mds;
//			}
//			mde = mde_dao.findOne(utente.getIdUtente());
//			if(mde != null){
//				this.setData(mde, utente);
//				result = mde;
//			}
//		} catch (DatabaseException e) {
//			e.printStackTrace();
//		}
//
//		return result;
//	}
//
//
//	/**
//	 * Metodo di supporto a checkUserTipe, inserisce i dati raccolti in precedenza
//	 * nella nuova istanza dopo aver identificato il tipo 
//	 * 
//	 * @param newData
//	 * @param data
//	 */
//	private void setData(UtenteTO newData, UtenteTO data){
//		newData.setIdUtente(data.getIdUtente());
//		newData.setUsername(data.getUsername());
//		newData.setPassword(data.getPassword());
//		newData.setNome(data.getNome());
//		newData.setCognome(data.getCognome());
//		newData.setCodiceFiscale(data.getCodiceFiscale());
//		newData.setIndirizzo(data.getIndirizzo());
//		newData.setCitta(data.getCitta());
//		newData.setEmail(data.getEmail());
//		newData.setSesso(data.getSesso());
//		newData.setDataNascita(data.getDataNascita());
//	}
}
