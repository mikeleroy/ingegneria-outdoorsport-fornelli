package app.business.as;

import java.util.ArrayList;

import app.integration.dao.StatoUtenteDAO;
import app.integration.dao.TipoUtenteDAO;
import app.integration.dao.UtenteDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EmailTO;
import app.to.EncryptPasswordTO;
import app.to.EnumTO;
import app.to.PartecipanteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.EmailSettings;
import app.utility.Rules;

abstract class ASGestoreUtenteBase implements Rules{


	private UtenteDAO<UtenteTO> uDao = null;
	private UtenteTO utente, temp = null;
	private EncryptPasswordTO encryptedPassword = null;
	
	/**
	 * Costruttore che inizializza il DAO dell'Utente
	 */
	@SuppressWarnings("unchecked")
	public ASGestoreUtenteBase() {
		if(uDao == null){
			uDao = (UtenteDAO<UtenteTO>) HibFactoryDAO.getFactoryDAO(EnumDAO.UtenteDAO);
		}
		if(utente == null){
			utente = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente);
		}
		if(temp == null){
			temp = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente);
		}
		
		if(encryptedPassword == null){
			encryptedPassword = (EncryptPasswordTO)  FactoryTO.getFactoryTO(EnumTO.EcryptPassword);
		}
	}
	

	/**
	 * Metodo che invia una mail al destinatario (dopo aver verificato se la email esiste) 
	 * alla schermata per la scelta della nuova password
	 * 
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse nuovaPassword(OutdoorRequest request){
		utente = (UtenteTO) request.getData();
		OutdoorResponse response = new OutdoorResponse();
		
		try {
			
				String sendPassword = utente.getPassword();
				
					String newPassword = encryptedPassword.encryptPassword(utente.getPassword());
					utente.setPassword(newPassword);
				
				
				uDao.update(utente);
				
				EmailTO email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);
				String mailOggetto = "OutDoorSports | Recupero Password";
				String mailMessaggio = "Gentile ";
				mailMessaggio += utente.getNome() + " " + utente.getCognome() + ", \n";
				mailMessaggio += "La tue credenziali di accesso sono\n ";
				mailMessaggio += "email:" + " " + utente.getEmail() + ", \n";
				mailMessaggio +=  "password:" + " " +sendPassword + ", \n";

				email.setOggetto(mailOggetto);
				email.setMessaggio(mailMessaggio);

				ArrayList<UtenteTO> listaDestinatari = new ArrayList<>();
				listaDestinatari.add(utente);
				email.setDestinatari(listaDestinatari);

				EmailSettings emailSettings = new EmailSettings();
				emailSettings.sendEmail(email);
				
				response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setResponse(FAILED);

		}
		

		return response;
	}
	
	
	
	
	
}
