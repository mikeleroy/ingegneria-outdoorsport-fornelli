package app.business.as;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.omg.PortableInterceptor.SUCCESSFUL;

import app.integration.dao.EscursioneDAO;
import app.integration.dao.IscrizioneDAO;
import app.integration.dao.OptionalEscursioneDAO;
import app.integration.dao.OptionalIscrizioneDAO;
import app.integration.dao.StatoEscursioneDAO;
import app.integration.dao.StatoIscrizioneDAO;
import app.integration.dao.StatoOptionalDAO;
import app.integration.dao.UtenteDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EmailTO;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.ManagerDiEscursioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalIscrizioneTO;
import app.to.OptionalTO;
import app.to.StatoEscursioneTO;
import app.to.StatoIscrizioneTO;
import app.to.StatoOptionalTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Actions;
import app.utility.EmailSettings;
import app.utility.Rules;
import app.utility.SessionData;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
/**
 * Classe che modella e implementa l'Application Service per la gestione di una Escursione.
 * L'obiettivo � quello di raccogliere tutte le azioni che � possibile effettuare per 
 * una Escursione, andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * L'application service utilizza i Transfer Object e i Data Access Object per occuparsi
 * della persistenza di tali oggetti, ma anche per il recupero di tali dati dal Database.
 * 
 * @author michele fornelli
 *
 */

class ASGestoreEscursione implements Actions,Rules {


	private EscursioneDAO escursioneDAO = null;
	private StatoOptionalDAO statoOptionalDAO = null;
	private IscrizioneDAO iscrizioneDAO = null;
	private StatoEscursioneDAO statoEscursioneDAO = null;
	private OptionalEscursioneDAO optionalEscursioneDAO = null;
	private OptionalIscrizioneDAO optionalIscrizioneDAO= null;
	private StatoOptionalTO stato_disattivo = null;
	private EscursioneTO escursione = null;
	private StatoOptionalTO stato_attivo = null;
	private List<IscrizioneTO> listaIscrizioni = null;
	private StatoIscrizioneDAO statoIscrizioneDAO = null;
	private OptionalIscrizioneTO optionalEscursione = null;
	

	public ASGestoreEscursione() {
		escursioneDAO =  (EscursioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.EscursioneDAO);
		optionalEscursioneDAO = (OptionalEscursioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.OptionalEscursioneDAO);
		statoOptionalDAO = (StatoOptionalDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.StatoOptionalDAO);
		statoEscursioneDAO = (StatoEscursioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.StatoEscursioneDAO);
		iscrizioneDAO = (IscrizioneDAO)  HibFactoryDAO.getFactoryDAO(EnumDAO.IscrizioneDAO);
		optionalIscrizioneDAO = (OptionalIscrizioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.OptionalIscrizioneDAO);
		statoIscrizioneDAO = (StatoIscrizioneDAO)  HibFactoryDAO.getFactoryDAO(EnumDAO.StatoIscrizioneDAO);
	}

	/**
	 * Metodo che restituisce tutte le Escursioni 
	 * presenti nel sistema
	 * 
	 * @param request: Richiesta di tutte le escursioni
	 * @return response: tutte le escursioni presenti nel sistema
	 */
	public OutdoorResponse getAllEscursioni(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();

		try {
			List<EscursioneTO> list_escursioni = escursioneDAO.getAll();
			List<StatoEscursioneTO> list_stato_escursione = statoEscursioneDAO.getAll();
			
			for(EscursioneTO e : list_escursioni){
				LocalDate today = LocalDate.now();
				if(today.toString().equals(e.getData())){
					e.setStatoEscursione(list_stato_escursione.get(3));
					escursioneDAO.update(e);
				}else{
					LocalDate eDate = LocalDate.parse(e.getData());
					if(eDate.isBefore(today)){
						e.setStatoEscursione(list_stato_escursione.get(0));
						escursioneDAO.update(e);
					}
				}
			}
			
			response.setData(list_escursioni);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			e.printStackTrace();
			response.setResponse(FAILED);
		}
		return response;
	}

	/**
	 * Metodo che restituisce tutte le escursione aperte a cui il partecipante non � iscritto
	 * @param request
	 * @return response: risposta in base alla richiesta
	 */
	public OutdoorResponse getAllEscursioniAperte(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		try {
			List<EscursioneTO> list_escursioni = escursioneDAO.getEscursioniAperte();
			response.setData(list_escursioni);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			e.printStackTrace();
			response.setResponse(FAILED);
		}
		return response;
	}

	/**
	 * Metodo che restituisce tutte le escursioni di un determinato 
	 * Manager di Escursione
	 * 
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse getAllEscursioniMDE(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();

		try {
			List<EscursioneTO> listaEscursioni = escursioneDAO.getEscursioniByMDE((ManagerDiEscursioneTO)SessionData.getSessionData("ManagerDiEscursione"));
			response.setData(listaEscursioni);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			e.printStackTrace();
			response.setResponse(FAILED);
		}
		return response;
	}
	

	/**
	 * Metodo che restituisce una risposta in base a una richiesta, e inserisce 
	 * una nuova Escursione nel sistema
	 * 
	 * @param request
	 * @return una risposta in base alla richiesta
	 */
	public OutdoorResponse nuovaEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();

		try {
			if(!escursioneDAO.checkEscursione((EscursioneTO)request.getData())){
				escursione = (EscursioneTO) request.getData();
				escursione.setStatoEscursione(statoEscursioneDAO.getStatoEscursioneAperta());
				ManagerDiEscursioneTO mde = (ManagerDiEscursioneTO) SessionData.getSessionData("ManagerDiEscursione");	
				escursione.setUtente(mde);
				escursioneDAO.create(escursione);
				Alert alert = new Alert(AlertType.INFORMATION, "L'Escursione � stata inserita correttamente!", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");
				alert.showAndWait();
				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			} 
			response.setData(null);
		} catch (DatabaseException e) {
			Alert alert = new Alert(AlertType.ERROR, "Alcuni campi non sono corretti!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}

		return response;
	}
	
	
	

	/**
	 * Metodo che restituisce una risposta in base alla richiesta, e
	 * annulla una escursione
	 * 
	 * @param request
	 * @return response
	 */
	public OutdoorResponse annullaEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		EscursioneTO escursione = (EscursioneTO) request.getData();
		try {
			escursione.setStatoEscursione(statoEscursioneDAO.getStatoEscursioneAnnullata());
			escursioneDAO.update(escursione);
			response.setData(escursione);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
		}

		return response;
	}
	
	/**
	 * Metodo che ordina gli iscritti in base alla data e all'ora
	 * di iscrizione.
	 * 
	 * @param iscritti
	 */
	private List<IscrizioneTO> ordinaIscritti(List<IscrizioneTO> iscritti){
		Collections.sort(iscritti, new Comparator<IscrizioneTO>() {

			@Override
			public int compare(IscrizioneTO o1, IscrizioneTO o2) {
				IscrizioneTO iscrizione1 = (IscrizioneTO)o1;
				IscrizioneTO iscrizione2 = (IscrizioneTO)o2;
				int result = iscrizione1.getData().compareTo(iscrizione2.getData());
				if (result == 0) {
					// Strings are equal, sort by date
					return iscrizione1.getOrario().compareTo(iscrizione2.getOrario());
				}
				else {
					return result;
				}
			}
		});
		
		return iscritti;
	}
	
	
	/**
	 * Metodo che restituisce una risposta in base a una richiesta, e modifica
	 * una escursione presente nel sistema
	 * 
	 * @param request
	 * @return una risposta in base alla richiesta
	 */
	public OutdoorResponse modificaEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		escursione = (EscursioneTO) request.getData();
		EscursioneTO temp = null;
		int id_temp = -1;

		try {
			temp = escursioneDAO.getById(escursione.getIdEscursione().intValue());
			if(temp.getIdEscursione() != null)
				id_temp = temp.getIdEscursione().intValue();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

		try {
			if(!(escursioneDAO.checkEscursione(escursione) || temp == null) || (escursioneDAO.checkEscursione(escursione) && escursione.getIdEscursione() == id_temp)){
				escursione.setStatoEscursione(statoEscursioneDAO.getStatoEscursioneAperta());
				ManagerDiEscursioneTO mde = (ManagerDiEscursioneTO) SessionData.getSessionData("ManagerDiEscursione");
				OptionalEscursioneTO optionalEscursione = (OptionalEscursioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);

				if(mde != null)
					escursione.setUtente(mde);
				
				Set<OptionalTO> optionals = new HashSet<>();
				optionals.addAll(escursione.getOptionals());
				Set<OptionalTO> newOptionals = new HashSet<>();
				stato_attivo = statoOptionalDAO.getStatoAttivo();
				stato_disattivo = statoOptionalDAO.getStatoDisattivo();
				
				List<OptionalEscursioneTO> listaOptionalEscursione = new ArrayList<>();
				for(OptionalTO op : optionals){
					if(!op.getNome().contains(" " + stato_attivo.getNome()) && !op.getNome().contains(stato_disattivo.getNome())){
						newOptionals.add(op);
					}else{
						List<OptionalEscursioneTO> list = new ArrayList<>();
						list.addAll(optionalEscursioneDAO.getAssociationID(escursione, op));
						optionalEscursione = list.get(0);
						if(op.getNome().contains(" " + stato_attivo.getNome())){
							optionalEscursione.setStatoOptional(stato_attivo);
						}else if(op.getNome().contains(stato_disattivo.getNome())){
							optionalEscursione.setStatoOptional(stato_disattivo);
						}
						op.setNome(op.getNome().substring(0, op.getNome().indexOf(" | ")));
						
						listaOptionalEscursione.add(optionalEscursione);
						newOptionals.add(op);
					}
				}
				escursione.setOptionals(newOptionals);
				
				List<IscrizioneTO> list_iscritti = new ArrayList<>();
				list_iscritti.addAll(iscrizioneDAO.getAllIscrittiFromEscursione(escursione));
				List<IscrizioneTO> list_iscritti_temp = new ArrayList<>();
				list_iscritti_temp.addAll(iscrizioneDAO.getAllIscrittiFromEscursione(escursione));
				
				for(IscrizioneTO i : list_iscritti_temp){
					i.setOptionals(null);
					iscrizioneDAO.update(i);
				}
				
				escursioneDAO.update(escursione);
				
				List<OptionalEscursioneTO> listaOptionalEscursione2 = new ArrayList<>();
				listaOptionalEscursione2.addAll(optionalEscursioneDAO.getAllOptionalEscursione(escursione.getIdEscursione()));
				
				for(OptionalEscursioneTO oe : listaOptionalEscursione){
					for(OptionalEscursioneTO oe2 : listaOptionalEscursione2){
						if(oe.getOptional().getNome().equals(oe2.getOptional().getNome())){
							oe2.setStatoOptional(oe.getStatoOptional());
							optionalEscursioneDAO.update(oe2);
						}
					}
				}
				
				for(IscrizioneTO i : list_iscritti){
					Set<OptionalEscursioneTO> list_oe_from_iscrizione = i.getOptionals();
					for(OptionalEscursioneTO oe : list_oe_from_iscrizione){
						for(OptionalEscursioneTO oe_generic : listaOptionalEscursione2){
							if(oe.getOptional().getNome().equals(oe_generic.getOptional().getNome())){
								oe.setIdOptionalEscursione(oe_generic.getIdOptionalEscursione());
								oe.setEscursione(oe_generic.getEscursione());
								oe.setStatoOptional(oe_generic.getStatoOptional());
								oe.setOptional(oe_generic.getOptional());
							}
						}
						OptionalIscrizioneTO optionalIscrizione = (OptionalIscrizioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalIscrizione);
						optionalIscrizione.setIdIscrizione(i.getIdIscrizione());
						optionalIscrizione.setIdOptionalEscursione(oe.getIdOptionalEscursione());
						optionalIscrizioneDAO.create(optionalIscrizione);
						optionalEscursioneDAO.update(oe);
					}
				}
				
				Alert alert = new Alert(AlertType.INFORMATION, "L'Escursione � stata modificata correttamente!", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");
				alert.showAndWait();
				response.setResponse(SUCCESS);

				listaIscrizioni = iscrizioneDAO.getAllIscrittiFromEscursione(escursione);
				
				List<IscrizioneTO> lista_iscrizioni_ordinata = ordinaIscritti(listaIscrizioni);
				List<StatoIscrizioneTO> lista_stato_iscrizione = new ArrayList<>();
				List<IscrizioneTO> lista_iscrizioni_da_annullare = new ArrayList<>();
				lista_stato_iscrizione.addAll(statoIscrizioneDAO.getAll());
				
				int numero_iscritti = escursione.getTotIscritti();
				int i = 0;
				int iscritti = numero_iscritti;
				
				for(IscrizioneTO iscritto : lista_iscrizioni_ordinata){
					if(i >= (numero_iscritti - 1)){
						iscritto.setStatoIscrizione(lista_stato_iscrizione.get(0));
						iscrizioneDAO.update(iscritto);
						lista_iscrizioni_da_annullare.add(iscritto);
						iscritti--;
					}
					i++;
				}
				escursione.setTotIscritti(iscritti);
				escursioneDAO.update(escursione);
				
				EmailTO email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);

				String mailOggetto = "OutDoorSports | Modifica escursione: " + escursione.getNome();
				String mailMessaggio = "Gentile Partecipante, \n";
				mailMessaggio += "La informiamo che il Manager di Escursione ha apportato le seguenti modifiche:\n";
				mailMessaggio += "Nome: " + escursione.getNome() + "\n";
				mailMessaggio += "Numero minimo di partecipanti: " + escursione.getNumMin() + "\n";
				mailMessaggio += "Numero massimo di partecipanti: " + escursione.getNumMax() + "\n";
				mailMessaggio += "Prezzo base: " + escursione.getPrezzo() + "\n";
				mailMessaggio += "Data: " + escursione.getData() + "\n";
				mailMessaggio += "Descrizione " + escursione.getDescrizione() + "\n";
				mailMessaggio += "Tipo Escursione " + escursione.getTipoEscursione().getNome() + "\n";

				Set<OptionalEscursioneTO> set_oe = new HashSet<>();

				try {
					set_oe.addAll(optionalEscursioneDAO.getAllOptionalEscursione(escursione.getIdEscursione()));
				} catch (DatabaseException e1) {
					e1.printStackTrace();
				}

				String string = "";
				if(set_oe.isEmpty())
					string = "Nessuno";
				else
					for(OptionalEscursioneTO e : set_oe){
						if(e.getStatoOptional().getIdStatoOptional() == stato_attivo.getIdStatoOptional()){
							string += e.getOptional().getNome() + " | ";
						}
					}

				mailMessaggio += "Optional Disponibili: " + string + "\n";

				ArrayList<UtenteTO> listaDestinatari = new ArrayList<>();

				for(IscrizioneTO in : listaIscrizioni){
					in.setOptionals(set_oe);
					listaDestinatari.add(in.getUtente());
				}

				email.setOggetto(mailOggetto);
				email.setMessaggio(mailMessaggio);

				email.setDestinatari(listaDestinatari);

				EmailSettings emailConfig = new EmailSettings();
				emailConfig.sendEmail(email);
				
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						EmailTO email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);

						String mailOggetto = "OutDoorSports | Modifica escursione: " + escursione.getNome();
						String mailMessaggio = "Gentile Partecipante, \n";
						mailMessaggio += "La informiamo che il Manager di Escursione ha modificato il numero degli iscritti,\n";
						mailMessaggio += "e la sua iscrizione verr� annullata, in quanto una delle ultime. \n";
						mailMessaggio += "Ci scusiamo per il disagio. \n";

						ArrayList<UtenteTO> listaDestinatari = new ArrayList<>();
						
						for(IscrizioneTO in : lista_iscrizioni_da_annullare){
							listaDestinatari.add(in.getUtente());
						}

						email.setOggetto(mailOggetto);
						email.setMessaggio(mailMessaggio);

						email.setDestinatari(listaDestinatari);

						EmailSettings emailConfig = new EmailSettings();
						emailConfig.sendEmail(email);
					}
				}).start();
				
				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			}
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

		return response;
	}


}