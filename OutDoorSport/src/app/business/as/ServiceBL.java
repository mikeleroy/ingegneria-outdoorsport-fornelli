package app.business.as;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.services.ServiceBase;
import app.services.ServiceType;

/**
 * Classe di servizio, invocata dal Service Locator, per collegare il 
 * Business Delegate al BusinessLookup. In questo modo si nascondono 
 * i vari passaggi della richiesta e non possono essere richiamati da altre
 * parti.
 * 
 * @author michele fornelli
 * 
 */


public class ServiceBL extends ServiceBase{

	/**
	 * Richiama l'istanza di BusinessLookUp
	 */
	private static BusinessLookUp businessLoockUp = BusinessLookUp.getInstance();

	public ServiceBL(){
	}
	
	@Override
	public ServiceType getType() {
		return ServiceType.BusinessLookUp;
	}

	@Override
	protected OutdoorResponse eseguiRichiesta(OutdoorRequest request) {
		return businessLoockUp.lookup(request);
	}

}
