package app.business.as;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.omg.PortableInterceptor.SUCCESSFUL;

import app.integration.dao.StatoUtenteDAO;
import app.integration.dao.TipoUtenteDAO;
import app.integration.dao.UtenteDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EmailTO;
import app.to.EncryptPasswordTO;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.PartecipanteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.EmailSettings;
import app.utility.Rules;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

class ASGestoreManagerDiEscursione extends ASGestoreUtenteBase {



	private ManagerDiEscursioneTO mde,temp = null;
	private UtenteDAO<ManagerDiEscursioneTO> mdeDAO = null;
	private TipoUtenteDAO tipoUtenteDao = null;
	private StatoUtenteDAO statoUtenteDao = null;
	private EncryptPasswordTO encryptedPassword = null;


	/**
	 * Costruttore che inizializza il DAO del Manager di Escursione
	 */
	public ASGestoreManagerDiEscursione() {

		if(mdeDAO == null){
			mdeDAO = (UtenteDAO<ManagerDiEscursioneTO>) HibFactoryDAO.getFactoryDAO(EnumDAO.ManagerDiEscursioneDAO);
		}
		if(tipoUtenteDao == null){
			tipoUtenteDao = (TipoUtenteDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.TipoUtenteDAO);
		}
		if(statoUtenteDao == null){
			statoUtenteDao = (StatoUtenteDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.StatoUtenteDAO);
		}
		if(mde == null){
			mde = (ManagerDiEscursioneTO) FactoryTO.getFactoryTO(EnumTO.ManagerDiEscursione);

		}
		if(temp == null){
			temp = (ManagerDiEscursioneTO) FactoryTO.getFactoryTO(EnumTO.ManagerDiEscursione);
		}

		if(encryptedPassword == null){
			encryptedPassword = (EncryptPasswordTO)  FactoryTO.getFactoryTO(EnumTO.EcryptPassword);
		}


	}



	/**
	 * Metodo che restituisce la risposta rispetto alla richiesta di inserire un nuovo manager di escursione
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse nuovoManagerDiEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();

		try {
			if(!mdeDAO.esisteEmail((UtenteTO)request.getData()) && !mdeDAO.esisteUsername((UtenteTO)request.getData())){


				mde = (ManagerDiEscursioneTO)request.getData();
				mde.setTipoUtente(tipoUtenteDao.getTipoUtenteMde());
				mde.setStatoUtente(statoUtenteDao.getStatoAttivo());

				String password = mde.getPassword();
				mde.setPassword(encryptedPassword.encryptPassword(password));

				mdeDAO.create(mde);

				EmailTO email = (EmailTO) FactoryTO.getFactoryTO(EnumTO.Email);

				String mailOggetto = "OutDoorSports | Registrazione Manager Di Escursione";
				String mailMessaggio = "Gentile ";
				mailMessaggio += mde.getNome() + " " + mde.getCognome() + ", \n";
				mailMessaggio += "la registrazione come MANAGER DI ESCURSIONE � avvenuta con successo! \n";
				mailMessaggio += "I dati relativi al suo account sono stati inseriti dal Manager di Sistema! \n";
				mailMessaggio += " I suoi dati di accesso sono: \n";
				mailMessaggio += "Username: " + mde.getUsername() + "\n";
				mailMessaggio += "Password: " + password + "\n";

				email.setOggetto(mailOggetto);
				email.setMessaggio(mailMessaggio);

				ArrayList<UtenteTO> destinatari = new ArrayList<>();
				destinatari.add(mde);
				email.setDestinatari(destinatari);

				EmailSettings emailConfig = new EmailSettings();

				Alert alert = new Alert(AlertType.INFORMATION, "Il Manager di Escursione � stato inserito correttamente!", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");

				Optional<ButtonType> res = alert.showAndWait();

				if(res.get() == ButtonType.OK)
					emailConfig.sendEmail(email);

				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			}
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}

		return response;
	}
	
	
	
	/**
	 * Metodo che restituisce tutti i Manager di Escursione 
	 * presenti nel sistema
	 * 
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse listaManagerDiEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		
		try {
			List<ManagerDiEscursioneTO> listMDE = mdeDAO.getAll();
			response.setData(listMDE);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			e.printStackTrace();
			response.setResponse(FAILED);
		}
		return response;
	}
	
	
	/**
	 * Metodo che restituisce la risposta rispetto alla richiesta di modificare un partecipante
	 * esistente.
	 * @param request: Richiesta in ingresso
	 * @return response: Risposta rispetto alla richiesta
	 */
	public OutdoorResponse modificaAccessoUtente(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		
		mde = (ManagerDiEscursioneTO)request.getData();
		ManagerDiEscursioneTO temp = null;
		int id_temp = -1;
		
		try {
			temp = mdeDAO.getByEmail(mde.getEmail());
			if(temp.getIdUtente() != null)
				id_temp = temp.getIdUtente();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		
		try {
			if(!(mdeDAO.esisteEmail(mde) || temp == null) || (mdeDAO.esisteEmail(mde) 
					&& mde.getIdUtente() == id_temp)){
				
				
				UtenteTO findUser = mdeDAO.getByID(mde.getIdUtente());
				if(!mde.getPassword().equals(findUser.getPassword())){
					String ePassword = encryptedPassword.encryptPassword(mde.getPassword());
					mde.setPassword(ePassword);
				}
				
				temp.setUsername(mde.getUsername());
				
				mdeDAO.update(temp);
				response.setResponse(SUCCESS);
			}else{
				response.setResponse(FAILED);
			}
			response.setData(null);
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	


}
