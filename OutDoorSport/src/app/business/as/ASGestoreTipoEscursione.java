package app.business.as;

import java.util.List;

import app.integration.dao.EscursioneDAO;
import app.integration.dao.TipoEscursioneDAO;
import app.integration.dao.hib.HibFactoryDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.EnumDAO;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.TipoEscursioneTO;
import app.utility.Actions;
import app.utility.Rules;

/**
 * Classe che modella e implementa l'Application Service per la gestione di un TipoEscursione.
 * L'obiettivo � quello di raccogliere tutte le azioni che � possibile effettuare per 
 * un TipoEscursione, andando a ridurre l'accoppiamento con le altre componenti del sistema.
 * L'application service utilizza i Transfer Object e i Data Access Object per occuparsi
 * della persistenza di tali oggetti, ma anche per il recupero di tali dati dal Database.
 * 
 * @author michele fornelli
 *
 */


class ASGestoreTipoEscursione implements Actions,Rules{

	
	private TipoEscursioneDAO tipoEscursioneDAO = null;
	
	public ASGestoreTipoEscursione() {
		tipoEscursioneDAO =  (TipoEscursioneDAO) HibFactoryDAO.getFactoryDAO(EnumDAO.TipoEscursioneDAO);

	}
	
	/**
	 * Metodo che restituisce tutti i tipi di una escursione
	 * 
	 * @param request: richiesta da inviare
	 * @return response: la risposta in base alla richiesta
	 */
	public OutdoorResponse getAllTipiEscursione(OutdoorRequest request){
		OutdoorResponse response = new OutdoorResponse();
		List<TipoEscursioneTO> list_tipi_escursione = null;
		try {
			list_tipi_escursione = tipoEscursioneDAO.getAll();
			response.setData(list_tipi_escursione);
			response.setResponse(SUCCESS);
		} catch (DatabaseException e) {
			response.setResponse(FAILED);
			e.printStackTrace();
		}
		return response;
	}
}
