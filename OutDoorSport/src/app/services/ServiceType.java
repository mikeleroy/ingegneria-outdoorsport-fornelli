package app.services;

/**
 * Enum che rappresenta i tipi di servizi che possono essere richiesti
 * 
 * @author michele fornelli
 */
public enum ServiceType {
	ApplicationController,
	ApplicationService,
	BusinessDelegate,
	BusinessLookUp,
	FrontController
	
}

