package app.services;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;

/**
 * Classe astratta che rappresenta un servizio base
 * richiamato dal service locator
 * 
 * @author michele fornelli
 *
 */

public abstract class ServiceBase{
	
	/**
	 * Metodo astratto che conterr� l'implementazione della richiesta
	 * da inviare, in base alla classe che lo eredita
	 * 
	 * @param request
	 * @return una risposta in base alla richiesta
	 */
	protected abstract OutdoorResponse eseguiRichiesta(OutdoorRequest request);
	
	
	/**
	 * Riferimento al servizio che si vuole istanziare. Permette di gestire
	 * le richieste e le risposte.
	 */
	protected static ServiceBase service = null;


	/**
	 * @return il tipo del servizio richiesto
	 */
	public abstract ServiceType getType();
	
	
	
	/**
	 * @param request: Richiesta in ingresso
	 * @param service: Servizio richiesto
	 * @return restituisce una risposta gestendo le informazioni in ingresso di servizio e richiesta.
	 */
	protected OutdoorResponse handle(OutdoorRequest request, ServiceBase service){
		return service.eseguiRichiesta(request);
	}
	
	
}


