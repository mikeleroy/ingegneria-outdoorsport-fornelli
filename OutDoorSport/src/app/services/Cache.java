package app.services;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe che rappresenta la classe cache dei servizi collegati al Service Locator
 * 
 * @author michele fornelli
 *
 */

class Cache {
	/**
	 * Lista Servizi
	 */
	private List<ServiceBase> listServices;

	public Cache(){
		listServices = new ArrayList<ServiceBase>();
	}

	/**
	 * @param serviceType: tipo di servizio
	 * @return una nuova istanza del servizio in base al tipo
	 */
	public ServiceBase getService(ServiceType serviceType){

		for (ServiceBase service : listServices) {
			if(service.getType().equals(serviceType)){
				return service;
			}
		}
		return null;
	}

	/**
	 * Metodo che aggiunge un servizio nella cache dei servizi.
	 * Se esiste non lo inserisce.
	 * 
	 * @param newService: servizio da aggiungere
	 */
	public void addService(ServiceBase newService){
		boolean exists = false;

		for (ServiceBase service : listServices) {
			if(service.getType().equals(newService.getType())){
				exists = true;
			}
		}
		
		if(!exists){
			listServices.add(newService);
		}
	}
}
