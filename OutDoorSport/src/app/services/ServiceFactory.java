package app.services;

import app.business.as.ServiceAS;
import app.business.as.ServiceBL;
import app.business.bd.ServiceBD;
import app.presentation.ac.ServiceAC;

/**
 * Classe di supporto al Service Locator che istanzia
 * il servizio, in base al tipo di servizio richiesto (FACTORY)
 * 
 * @author michele fornelli
 *
 */

class ServiceFactory {
	
	/**
	 * @param serviceType: tipo di servizio richiesto
	 * @return una istanza del servizio richiesto
	 * @throws Exception: nel caso il servizio non esiste
	 */
	public Object lookup(ServiceType serviceType) throws Exception{
		switch(serviceType){
		case ApplicationController:
			return new ServiceAC();
			
		case ApplicationService:
			return new ServiceAS();
			
		case BusinessDelegate:
			return new ServiceBD();
			
		case BusinessLookUp:
			return new ServiceBL();
			
		default:
			throw new Exception("Servizio errato");	
		}	
	}
}
