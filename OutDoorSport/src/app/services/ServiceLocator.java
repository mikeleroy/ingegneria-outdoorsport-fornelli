package app.services;

import org.hibernate.query.criteria.internal.predicate.IsEmptyPredicate;

/**
 * Classe che consente di creare il servizio richiesto e usufruirne.
 * Rappresenta il pattern del Service Locator
 * 
 * @author michele fornelli
 *
 */

public class ServiceLocator{

	
	/**
	 * Cache dei servizi
	 */
	private static Cache cache;

	static{
		cache = new Cache();		
	}



     private ServiceLocator() {}

     public static Cache getIstance() {
             if(cache==null)
            	 cache = new Cache();
             return cache;
     }
	
	/** 
	 * @param serviceType: Tipo di servizio richiesto.
	 * @return restituisce il servizio richiesto in base al tipo dello stesso dato in ingresso.
	 * @throws Exception: Lancia l'eccezione nel caso non esista il servizio richiesto.
	 */
	public static ServiceBase getService(ServiceType serviceType){
		
		
		ServiceBase service = cache.getService(serviceType);

		if(service != null)
			return service;

		ServiceFactory context = new ServiceFactory();
		ServiceBase newService = null;
		
		try {
			newService = (ServiceBase)context.lookup(serviceType);
			cache.addService(newService);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newService;
	}
}

