package app;



import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.JDBCConnectionException;

import app.integration.daoUtil.HibernateUtil;
import app.presentation.fc.FrontController;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EnumTO;
import app.to.ManagerDiSistemaTO;
import app.to.impl.FactoryTO;
import app.to.StatoUtenteTO;
import app.to.UtenteTO;
import app.utility.Actions;
import app.utility.Views;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;


public class Main extends Application implements Actions,Views {

	OutdoorResponse response;
	
	private void startSession() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
		} catch (JDBCConnectionException e) {
			tx.rollback();
		}
		
	}
	
	
	@Override
	public void start(Stage primaryStage) {
		
		//if configurazione iniziale TODO else mostra login

	//	ManagerDiSistemaTO mds = (ManagerDiSistemaTO) TOFact.getUtenteTO(UtenteEnum.ManagerDiSistema);
		//avvia la sessione di hibernate
		startSession();

		FrontController fc = FrontController.getInstance();
		response = fc.eseguiRichiesta(new OutdoorRequest(VIEW_LOGIN));


	
		
		/**
		 * Come vedi lo scopo � stato quello di nascondere il tutto
		 * di queste classi pubbliche puoi solo prendere l'istanza perche i metodi
		 * che mandano la richiesta sono tutti protected e non possono essere 
		 * visti fuori dai loro package. Non hanno operazioni distruttive 
		 * o di modifica, quindi va bene cos�. Ovviamente sono migliorabili.
		 * Vai a vedere bene cosa fanno l'interfaccia Generale e la classe
		 * Pattern. Soprattutto la classe Pattern nasconde bene la richiesta.
		 * 
		 */

		
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
