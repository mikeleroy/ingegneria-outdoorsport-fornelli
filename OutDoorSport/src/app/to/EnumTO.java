package app.to;

public enum EnumTO {

	Utente,Partecipante,ManagerDiEscursione,ManagerDiSistema,StatoUtente,TipoUtente,Escursione,
	StatoEscursione,TipoEscursione,Optional,TipoOptional,StatoOptional,Iscrizione,StatoIscrizione,
	OptionalIscrizione,OptionalEscursione,
	Email,EcryptPassword
	
	
}
