package app.to;

/**
 * Interfaccia che rappresenta StatoEscursione. Sono fornite tutte le 
 * dichiarazioni dei metodi per la lettura
 * 
 * @author michele fornelli
 *
 */

public interface StatoEscursioneTO extends BaseTO{
	
	/**
	 * Metodo che setta lo stato dell'escursione
	 * 
	 * @param idStatoEscursione
	 */
	public void setIdStatoEscursione(Integer idStatoEscursione);
	
	/**
	 * @return l'id dello stato escursione
	 */
	public Integer getIdStatoEscursione();
	
	/**
	 * @return il nome dello stato escursione
	 */
	public String getNome();
	
	/**
	 * Metodo che setta il nome dello stato escursione
	 * 
	 * @param nome
	 */
	public void setNome(String nome);
}
