package app.to;

import java.util.Date;

/**
 * Realizzazione della classe UtenteTO che implementa {@link BaseTO}
 * .
 * @author michele fornelli
 *
 */


public interface UtenteTO extends BaseTO{

	/**
	 * @return the idUtente
	 */
	Integer getIdUtente();


	/**
	 * @param idUtente the idUtente to set
	 */
	void setIdUtente(Integer idUtente);


	/**
	 * @return the username
	 */
	String getUsername();


	/**
	 * @param username the username to set
	 */
	void setUsername(String username);



	/**
	 * @return the password
	 */
	String getPassword();


	/**
	 * @param password the password to set
	 */
	void setPassword(String password);


	/**
	 * @return the email
	 */
	String getEmail();



	/**
	 * @param email the email to set
	 */
	void setEmail(String email);





	/**
	 * @return the nome
	 */
	String getNome();




	/**
	 * @param nome the nome to set
	 */
	void setNome(String nome);





	/**
	 * @return the cognome
	 */
	String getCognome();





	/**
	 * @param cognome the cognome to set
	 */
	void setCognome(String cognome);


	/**
	 * @return the cf
	 */
	String getCf() ;



	/**
	 * @param cf the cf to set
	 */
	void setCf(String cf);


	/**
	 * @return the dataNasc
	 */
	Date getDataNasc();




	/**
	 * @param dataNasc the dataNasc to set
	 */
	void setDataNasc(Date dataNasc);



	/**
	 * @return the sesso
	 */
	String getSesso();



	/**
	 * @param sesso the sesso to set
	 */
	void setSesso(String sesso);




	/**
	 * @return the citta
	 */
	String getCitta();






	/**
	 * @param citta the citta to set
	 */
	void setCitta(String citta); 




	/**
	 * @return the indirizzo
	 */
    String getIndirizzo();






	/**
	 * @param indirizzo the indirizzo to set
	 */
	void setIndirizzo(String indirizzo);





	/**
	 * @return lo stato utente relativo a un determinato utente
	 */
	public StatoUtenteTO getStatoUtente();
	
	/**
	 * Metodo che setta lo stato utente relativo a un determinato utente
	 * 
	 * @param tblStatoUtente
	 */
	public void setStatoUtente(StatoUtenteTO statoUtente);





	/**
	 * @return the idTipo
	 */
	public TipoUtenteTO getTipoUtente();



	/**
	 * @param idTipo the idTipo to set
	 */
	public void setTipoUtente(TipoUtenteTO tipoUtente);




}