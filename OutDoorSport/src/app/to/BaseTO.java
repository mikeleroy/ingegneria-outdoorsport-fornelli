package app.to;

import java.io.Serializable;

/**
 * questa interfaccia ha lo scopo di uniformare tutti i transfer-object utilizzati
 * nell'applicazione.
 * 
 * @author michele fornelli
 * 
 */

public interface BaseTO extends Serializable{
	String toString();
}