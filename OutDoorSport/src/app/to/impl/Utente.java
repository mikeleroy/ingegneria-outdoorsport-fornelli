package app.to.impl;

import java.util.Date;

import app.to.BaseTO;
import app.to.StatoUtenteTO;
import app.to.TipoUtenteTO;
import app.to.UtenteTO;

/**
 * Realizzazione della classe UtenteTO che implementa {@link BaseTO}
 * .
 * @author michele fornelli
 *
 */


class Utente implements UtenteTO{

	private static final long serialVersionUID = -5750807819852265909L;
	protected Integer idUtente;
	protected String username;
	protected String password;
	protected String email;
	protected String nome;
	protected String cognome;
	protected String cf;
	protected Date dataNasc;
	protected String sesso;
	protected String citta;
	protected String indirizzo;
	//attributi per chiavi esterne
	private StatoUtenteTO statoUtente;
	private TipoUtenteTO tipoUtente;
	
	
	public Utente(){
	}

	

	public Utente(Integer idUtente, String username, String password, String email, String nome, String cognome,
			String cf, Date dataNasc, String sesso, String citta, String indirizzo, StatoUtenteTO idStato,
			TipoUtenteTO idTipo) {
		this.idUtente = idUtente;
		this.username = username;
		this.password = password;
		this.email = email;
		this.nome = nome;
		this.cognome = cognome;
		this.cf = cf;
		this.dataNasc = dataNasc;
		this.sesso = sesso;
		this.citta = citta;
		this.indirizzo = indirizzo;
		this.statoUtente = statoUtente;
		this.tipoUtente = tipoUtente;
	}



	@Override
	public Integer getIdUtente() {
		return idUtente;
	}



	@Override
	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}






	@Override
	public String getUsername() {
		return username;
	}






	@Override
	public void setUsername(String username) {
		this.username = username;
	}






	@Override
	public String getPassword() {
		return password;
	}






	@Override
	public void setPassword(String password) {
		this.password = password;
	}






	@Override
	public String getEmail() {
		return email;
	}






	@Override
	public void setEmail(String email) {
		this.email = email;
	}






	@Override
	public String getNome() {
		return nome;
	}




	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}




	@Override
	public String getCognome() {
		return cognome;
	}




	@Override
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}




	@Override
	public String getCf() {
		return cf;
	}




	@Override
	public void setCf(String cf) {
		this.cf = cf;
	}





	@Override
	public Date getDataNasc() {
		return dataNasc;
	}




	@Override
	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}





	@Override
	public String getSesso() {
		return sesso;
	}




	@Override
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}





	@Override
	public String getCitta() {
		return citta;
	}





	@Override
	public void setCitta(String citta) {
		this.citta = citta;
	}



	@Override
	public String getIndirizzo() {
		return indirizzo;
	}


	@Override
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}



	@Override
	public StatoUtenteTO getStatoUtente() {
		return statoUtente;
	}


	@Override
	public void setStatoUtente(StatoUtenteTO statoUtente) {
		this.statoUtente = statoUtente;
	}


	@Override
	public TipoUtenteTO getTipoUtente() {
		return tipoUtente;
	}


	@Override
	public void setTipoUtente(TipoUtenteTO tipoUtente) {
		this.tipoUtente = tipoUtente;
	}




}