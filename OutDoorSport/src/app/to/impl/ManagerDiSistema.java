package app.to.impl;

import app.to.ManagerDiSistemaTO;
import app.to.UtenteTO;

/**
 * Classe che implementa il i dati del Manager di Sistema.
 * 
 * @author michele fornelli
 *
 */
class ManagerDiSistema extends Utente implements ManagerDiSistemaTO{

	private static final long serialVersionUID = 5017576654898485317L;
	private int idMds;
	private String telefono;

	ManagerDiSistema() {
		super.setIdUtente(this.getIdMds());
	}

	@Override
	public int getIdMds() {
		return this.idMds;
	}

	@Override
	public void setIdMds(int idMds) {
		this.idMds = idMds;
	}

	@Override
	public String getTelefono() {
		return this.telefono;
	}

	@Override
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
