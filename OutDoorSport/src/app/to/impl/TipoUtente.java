package app.to.impl;

import app.to.TipoUtenteTO;

/**
 * Realizzazione della classe RuoloTO {@link TipoUtenteTO}.
 * 
 * @author michele fornelli
 *
 */
class TipoUtente implements TipoUtenteTO {

	private static final long serialVersionUID = -5565024038693813756L;
	private Integer idTipoUtente;
	private String nome;


	TipoUtente(){}
	
	@Override
	public String getNome() {
		return this.nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	@Override
	public Integer getIdTipoUtente() {
		return idTipoUtente;
	}
	
	
	@Override
	public void setIdTipoUtente(Integer idTipoUtente) {
		this.idTipoUtente = idTipoUtente;
	}



}
