package app.to.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import app.to.EscursioneTO;
import app.to.ManagerDiEscursioneTO;
import app.to.OptionalTO;
import app.to.StatoEscursioneTO;
import app.to.TipoEscursioneTO;
import app.to.UtenteTO;

/**
 * Realizzazione della classe EscursioneTO
 * 
 * @author michele fornelli
 * 
 *
 */
class Escursione implements EscursioneTO
{
	
	private static final long serialVersionUID = 0;
	private Integer idEscursione;
	private String nome;
	private String data;
	private int numMin;
	private int numMax;
	private String descrizione;
	private Double prezzo;
	private Set<OptionalTO> optionals;
	private TipoEscursioneTO tipoEscursione;
	private StatoEscursioneTO statoEscursione;
	private UtenteTO utente;
	private int totIscritti;

	
	Escursione() {
	}

	@Override
	public Integer getIdEscursione() {
		return this.idEscursione;
	}

	@Override
	public void setIdEscursione(Integer idEscursione) {
		this.idEscursione = idEscursione;
	}

	@Override
	public StatoEscursioneTO getStatoEscursione() {
		return this.statoEscursione;
	}

	@Override
	public void setStatoEscursione(StatoEscursioneTO statoEscursione) {
		this.statoEscursione = statoEscursione;
	}

	@Override
	public TipoEscursioneTO getTipoEscursione() {
		return this.tipoEscursione;
	}

	@Override
	public void setTipoEscursione(TipoEscursioneTO tipoEscursione) {
		this.tipoEscursione = tipoEscursione;
	}

	@Override
	public UtenteTO getUtente() {
		return this.utente;
	}

	@Override
	public void setUtente(UtenteTO utente) {
		this.utente = utente;
	}

	@Override
	public String getNome() {
		return this.nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String getData() {
		return this.data;
	}

	@Override
	public void setData(String data) {
		this.data = data;
	}

	
	@Override
	public int getNumMin() {
		return this.numMin;
	}

	@Override
	public void setNumMin(int numMin) {
		this.numMin = numMin;
	}

	@Override
	public int getNumMax() {
		return this.numMax;
	}

	@Override
	public void setNumMax(int numMax) {
		this.numMax = numMax;
	}

	@Override
	public double getPrezzo() {
		return this.prezzo;
	}

	@Override
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	@Override
	public String getDescrizione() {
		return this.descrizione;
	}

	@Override
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	@Override
	public Set<OptionalTO> getOptionals() {
		return optionals;
	}

	@Override
	public void setOptionals(Set<OptionalTO> optionals) {
		this.optionals = optionals;
	}

	@Override
	public int getTotIscritti() {
		return this.totIscritti;
	}

	@Override
	public void setTotIscritti(int totIscritti) {
		this.totIscritti = totIscritti;
	}

}
