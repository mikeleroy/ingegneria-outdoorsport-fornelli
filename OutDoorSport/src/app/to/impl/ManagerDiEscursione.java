package app.to.impl;

import app.to.ManagerDiEscursioneTO;

/**
 * Classe che implementa il i dati del Manager di Escursione.
 * 
 * @author michele fornelli
 *
 */

class ManagerDiEscursione extends Utente implements ManagerDiEscursioneTO{

	private static final long serialVersionUID = -5507782322150099517L;
	private double stipendio;
	private int idMde;


	ManagerDiEscursione() {
		super.setIdUtente(this.getIdUtente());	
		}
	
	

	@Override
	public int getIdMde() {
		return this.idMde;
	}

	@Override
	public void setIdMde(int idMde) {
		this.idMde = idMde;
	}
	
	
	@Override
	public double getStipendio() {
		return this.stipendio;
	}

	@Override
	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}
}