package app.to.impl;

import app.to.EscursioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalTO;
import app.to.StatoOptionalTO;

/**
 * Classe che implementa OptionalEscursione.
 * 
 * @author michele fornelli
 *
 */


class OptionalEscursione implements OptionalEscursioneTO{

	private static final long serialVersionUID = -2717683768500440335L;
	private Integer idOptionalEscursione;
	private EscursioneTO escursione;
	private OptionalTO optional;
	private StatoOptionalTO stato;
	
	OptionalEscursione() {}
	
	OptionalEscursione(Integer id, EscursioneTO escursione, OptionalTO optional, StatoOptionalTO stato) {
		this.idOptionalEscursione = id;
		this.escursione = escursione;
		this.optional = optional;
		this.stato = stato;
	}

	@Override
	public Integer getIdOptionalEscursione() {
		return this.idOptionalEscursione;
	}

	@Override
	public void setIdOptionalEscursione(Integer id) {
		this.idOptionalEscursione = id;
	}
	
	@Override
	public EscursioneTO getEscursione() {
		return this.escursione;
	}

	@Override
	public void setEscursione(EscursioneTO escursione) {
		this.escursione = escursione;
	}

	@Override
	public OptionalTO getOptional() {
		return this.optional;
	}

	@Override
	public void setOptional(OptionalTO optional) {
		this.optional = optional;
	}

	@Override
	public StatoOptionalTO getStatoOptional() {
		return this.stato;
	}

	@Override
	public void setStatoOptional(StatoOptionalTO stato) {
		this.stato = stato;
	}
}
