package app.to.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalTO;
import app.to.PartecipanteTO;
import app.to.StatoIscrizioneTO;
import app.to.UtenteTO;

/**
 * Classe che implementa lo stato dell'Iscrizione.
 * 
 * @author michele fornelli
 *
 */

class Iscrizione implements IscrizioneTO{

	private static final long serialVersionUID = 1L;
	private Integer idIscrizione;
	private EscursioneTO escursione;
	private StatoIscrizioneTO statoIscrizione;
	private UtenteTO utente;
	private String data;
	private String orario;
	private Set<OptionalEscursioneTO> optionals;

	 Iscrizione() {
	}
	
	@Override
	public Integer getIdIscrizione() {
		return this.idIscrizione;
	}

	@Override
	public void setIdIscrizione(Integer idIscrizione) {
		this.idIscrizione = idIscrizione;
	}

	@Override
	public EscursioneTO getEscursione() {
		return this.escursione;
	}

	@Override
	public void setEscursione(EscursioneTO escursione) {
		this.escursione = escursione;
	}

	@Override
	public StatoIscrizioneTO getStatoIscrizione() {
		return this.statoIscrizione;
	}

	@Override
	public void setStatoIscrizione(StatoIscrizioneTO statoIscrizione) {
		this.statoIscrizione = statoIscrizione;
	}

	@Override
	public UtenteTO getUtente() {
		return this.utente;
	}

	@Override
	public void setUtente(UtenteTO utente) {
		this.utente = utente;
	}

	@Override
	public String getData() {
		return this.data;
	}

	@Override
	public void setData(String data) {
		this.data = data;
	}
	
	@Override
	public Set<OptionalEscursioneTO> getOptionals() {
		return optionals;
	}

	@Override
	public void setOptionals(Set<OptionalEscursioneTO> optionals) {
		this.optionals = optionals;
	}

	@Override
	public String getOrario() {
		return orario;
	}

	@Override
	public void setOrario(String orario) {
		this.orario = orario;
	}
}

