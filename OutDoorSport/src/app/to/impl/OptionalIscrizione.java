package app.to.impl;

import app.to.OptionalIscrizioneTO;

/**
 * Classe che implementa lo stato di OptionalIscrizione.
 * 
 * @author michele fornelli
 *
 */
class OptionalIscrizione implements OptionalIscrizioneTO{

	private static final long serialVersionUID = -5812339531551921552L;
	private Integer idIscrizione;
	private Integer idOptionalEscursione;
	
	public OptionalIscrizione(){}

	public OptionalIscrizione(Integer idIscrizione, Integer idOptionalEscursione) {
		this.idIscrizione = idIscrizione;
		this.idOptionalEscursione = idOptionalEscursione;
	}

	@Override
	public Integer getIdIscrizione() {
		return this.idIscrizione;
	}

	@Override
	public void setIdIscrizione(Integer idIscrizione) {
		this.idIscrizione = idIscrizione;
	}

	@Override
	public Integer getIdOptionalEscursione() {
		return this.idOptionalEscursione;
	}

	@Override
	public void setIdOptionalEscursione(Integer idOptionalEscursione) {
		this.idOptionalEscursione = idOptionalEscursione;
	}
	
	 public boolean equals(Object o) {
	        if (this== o) return true;
	        if (o ==null|| getClass() != o.getClass()) return false;

	        return true;
	    }

	    public int hashCode() {
	        return 0;
	    }   
	    
}
