package app.to.impl;

import java.util.ArrayList;
import java.util.List;

import app.to.EmailTO;
import app.to.UtenteTO;

/**
 * Implementazione dell'interfaccia {@link EmailTO}.
 * 
 * @author michele fornelli
 *
 */
class Email implements EmailTO{
	private static final long serialVersionUID = -7116471049863934907L;
	private String messaggio;
	private String oggetto;
	private List<UtenteTO> destinatari;

	Email(){
		this.destinatari = new ArrayList<UtenteTO>();
	}

	@Override
	public void setMessaggio(String messaggio) {
		this.messaggio = messaggio;	
	}

	@Override
	public List<UtenteTO> getDestinatari() {
		return this.destinatari;
	}

	@Override
	public String getMessaggio() {
		return this.messaggio;
	}

	@Override
	public void setDestinatari(List<UtenteTO> destinatari) {
		this.destinatari = destinatari;
	}

	@Override
	public String getOggetto() {
		return this.oggetto;
	}

	@Override
	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}
}