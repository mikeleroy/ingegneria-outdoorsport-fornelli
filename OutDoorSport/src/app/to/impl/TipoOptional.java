package app.to.impl;

import app.to.TipoOptionalTO;

/**
 * Classe che implementa lo stato del TipoOptional.
 * 
 * @author michele fornelli
 *
 */

class TipoOptional implements TipoOptionalTO{

	private static final long serialVersionUID = -3551877113422783655L;
	private Integer idTipoOptional;
	private String nome;
	private double prezzo;


	TipoOptional(){}

	@Override
	public Integer getIdTipoOptional() {
		return this.idTipoOptional;
	}

	@Override
	public void setIdTipoOptional(Integer idTipoOptional) {
		this.idTipoOptional = idTipoOptional;
	}

	@Override
	public String getNome() {
		return this.nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public double getPrezzo() {
		return this.prezzo;
	}

	@Override
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
}
