package app.to.impl;

import app.to.BaseTO;
import app.to.EnumTO;

public class FactoryTO {

	
	public static BaseTO getFactoryTO(EnumTO type) {
		switch (type) {
		case Utente:
			return new Utente();
		case Partecipante:
			return new Partecipante();
		case ManagerDiSistema:
			return new ManagerDiSistema();
		case ManagerDiEscursione:
			return new ManagerDiEscursione();
		case StatoUtente:
			return new StatoUtente();
		case TipoUtente:
			return new TipoUtente();
		case Escursione:
			return new Escursione();
		case TipoEscursione:
			return new TipoEscursione();
		case StatoEscursione:
			return new StatoEscursione();
		case Iscrizione:
			return new Iscrizione();
		case StatoIscrizione:
			return new StatoIscrizione();
		case Optional:
			return new Optional();
		case TipoOptional:
			return new TipoOptional();
		case StatoOptional:
			return new StatoOptional();
		case OptionalEscursione:
			return new OptionalEscursione();
		case OptionalIscrizione:
			return new OptionalIscrizione();
		case Email:
			return new Email();
		case EcryptPassword:
			return new EncryptPassword();
		default:
			return new Utente();
		}
	}

}
