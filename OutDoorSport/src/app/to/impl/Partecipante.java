package app.to.impl;

import java.util.Date;

import app.to.PartecipanteTO;

/**
 * Realizzazione della interfaccia PartecipanteTO che estende {@link Utente}
 * .
 * @author michele fornelli
 *
 */
class Partecipante extends Utente implements PartecipanteTO {

	private static final long serialVersionUID = -8781718535238096085L;
	private int idPartecipante;
	private String numTessera;
	private Date dataSrc;
	private String fileSrc;

	
	public Partecipante() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	@Override
	public int getIdPartecipante() {
		return idPartecipante;
	}



	@Override
	public void setIdPartecipante(int idPartecipante) {
		this.idPartecipante = idPartecipante;
	}



	@Override
	public String getNumTessera() {
		return numTessera;
	}


	@Override
	public void setNumTessera(String numTessera) {
		this.numTessera = numTessera;
	}


	@Override
	public Date getDataSrc() {
		return dataSrc;
	}



	@Override
	public void setDataSrc(Date dataSrc) {
		this.dataSrc = dataSrc;
	}



	@Override
	public String getFileSrc() {
		return fileSrc;
	}




	@Override
	public void setFileSrc(String fileSrc) {
		this.fileSrc = fileSrc;
	}


	
}