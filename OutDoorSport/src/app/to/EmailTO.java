package app.to;


import java.util.List;

/**
 * L'interfaccia rappresenta un oggetto Email . <br>
 * Sono quindi forniti tutti i metodi per gestire l'invio di una email.
 * 
 * @author michele fornelli
 * 
 */
public interface EmailTO extends BaseTO{

	/**
	 * @return la lista dei destinatari
	 */
	public List<UtenteTO> getDestinatari();
	
	/**
	 * Metodo che setta la lista dei destinatari
	 * 
	 * @param Destinatari
	 */
	public void setDestinatari(List<UtenteTO> destinatari);
	
	/**
	 * @return il corpo del messaggio
	 */
	public String getMessaggio();
	
	/**
	 * Metodo che setta il corpo del messaggio
	 * 
	 * @param messaggio
	 */
	public void setMessaggio(String messaggio);
	
	/**
	 * @return restituisce l'oggetto della mail
	 */
	public String getOggetto();
	
	/**
	 * Metodo che setta l'oggetto della mail
	 * 
	 * @param oggetto
	 */
	public void setOggetto(String oggetto);
}