package app.to;


/**
 * Realizzazione della interfaccia TipoUtenteTO {@link TipoUtenteTO}.
 * 
 * @author michele fornelli
 *
 */
public interface TipoUtenteTO extends BaseTO{

	
	/**
	 * @return the nome
	 */
	 String getNome();
	 
	 
	/**
	 * @param nome the nome to set
	 */
	 void setNome(String nome);
	 
	 
	/**
	 * @return the idTipo
	 */
	 Integer getIdTipoUtente();
	 
	 
	/**
	 * @param idTipoUtente the idTipoUtente to set
	 */
	void setIdTipoUtente(Integer idTipoUtente);




}
