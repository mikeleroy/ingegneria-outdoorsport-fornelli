package app.to;


/**
 * Interfaccia che rappresenta il i dati del Manager di Escursione. Sono fornite tutte le 
 * dichiarazioni dei metodi per la lettura e la scrittura dei dati
 * 
 * @author michele fornelli
 *
 */

public interface ManagerDiEscursioneTO extends UtenteTO{
	
	
	/**
	 * Metodo che setta il manager di sistema
	 * 
	 * @param idMde
	 */
	public void setIdMde(int idMde);
	
	/**
	 * @return l'id del manager di escursione
	 */
	public int getIdMde();
	
	
	/**
	 * @return lo stipendio del manager di escursione
	 */
	public double getStipendio();
	
	/**
	 * Metodo che setta lo stipendio del manager di escursione
	 * 
	 * @param stipendio
	 */
	public void setStipendio(double stipendio);
}