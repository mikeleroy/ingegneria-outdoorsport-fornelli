package app.to;

/**
 * Interfaccia che rappresenta lo stato del TipoOptional. Sono fornite tutte le 
 * dichiarazioni dei metodi per la lettura dei dati
 * 
 * @author michele fornelli
 *
 */

public interface TipoOptionalTO extends BaseTO{
	
	/**
	 * Metodo che setta l'id del tipo optional
	 * 
	 * @param idTipoOptional
	 */
	public void setIdTipoOptional(Integer idTipoOptional);
	
	/**
	 * @return l'id del tipo optional
	 */
	public Integer getIdTipoOptional();
	
	/**
	 * @return il nome del tipo optional
	 */
	public String getNome();
	
	/**
	 * Metodo che setta il nome del tipo optional
	 * 
	 * @param nome
	 */

	public void setNome(String nome);
	
	/**
	 * @return il prezzo di un optional
	 */
	public double getPrezzo();
	
	/**
	 * Metodo che setta il prezzo di un optional
	 * 
	 * @param prezzo
	 */
	public void setPrezzo(double prezzo);
}
