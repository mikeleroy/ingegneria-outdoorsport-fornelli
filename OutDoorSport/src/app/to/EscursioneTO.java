package app.to;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Realizzazione della classe EscursioneTO
 * 
 * @author michele fornelli
 * 
 *
 */
public interface EscursioneTO extends BaseTO
{
	
	/**
	 * Metodo che setta l'id dell'escursione
	 * 
	 * @param idEscursione
	 */
	public void setIdEscursione(Integer idEscursione);
	
	/**
	 * @return l'id dell'Escursione
	 */
	public Integer getIdEscursione();
	
	/**
	 * @return lo stato dell'Escursione
	 */
	public StatoEscursioneTO getStatoEscursione();
	
	/**
	 * Metodo che setta lo stato dell'Escursione
	 * 
	 * @param tblStatoEscursione
	 */
	public void setStatoEscursione(StatoEscursioneTO statoEscursione);
	
	/**
	 * @return il tipo dell'escursione
	 */
	public TipoEscursioneTO getTipoEscursione();
	
	/**
	 * Metodo che setta il Tipo dell'Escursione
	 * 
	 * @param tblTipoEscursione
	 */
	public void setTipoEscursione(TipoEscursioneTO tipoEscursione);
	
	/**
	 * @return l'utente associato a una determinata Escursione
	 */
	public UtenteTO getUtente();
	
	/**
	 * Metodo che setta l'utente associato all'escursione
	 * 
	 * @param tblUtente
	 */
	public void setUtente(UtenteTO utente);
	
	/**
	 * @return il nome dell'escursione
	 */
	public String getNome();
	

	/**
	 * Metodo che setta il nome dell'escursione
	 * 
	 * @param nome
	 */
	public void setNome(String nome);
	
	/**
	 * @return la data dell'escursione
	 */
	public String getData();
	
	/**
	 * Metodo che setta la data dell'escursione
	 * 
	 * @param data
	 */
	public void setData(String data);
	
	/**
	 * @return numero minimo di partecipanti della escursione
	 */
	public int getNumMin();
	
	/**
	 * Metodo che setta il numero minimo dei partecipanti della escursione
	 * 
	 * @param numberMin
	 */
	public void setNumMin(int numMin);
	
	/**
	 * @return numero massimo di partecipanti della escursione
	 */
	public int getNumMax();
	
	/**
	 * Metodo che setta il numero massimo dei partecipanti alla escursione
	 * 
	 * @param numberMax
	 */
	public void setNumMax(int numMax);
	
	/**
	 * @return numero degli iscritti all'escursione
	 */
	public int getTotIscritti();
	
	/**
	 * Metodo che setta il numero degli iscritti all'escursione
	 * 
	 * @param iscritti
	 */
	public void setTotIscritti(int totIscritti);
	
	/**
	 * @return il prezzo della escursione
	 */
	public double getPrezzo();
	
	/**
	 * Metodo che setta il prezzo della escursione
	 * 
	 * @param costo
	 */
	public void setPrezzo(double prezzo);
	
	/**
	 * @return la descrizione della escursione
	 */
	public String getDescrizione();
	
	/**
	 * Metodo che setta la descrizione della escursione
	 * 
	 * @param descrizione
	 */
	public void setDescrizione(String descrizione);
	
	/**
	 * @return un set di optional
	 */
	public Set<OptionalTO> getOptionals();
	
	/**
	 * Metodo che setta gli optional per una determinata escursione
	 * 
	 * @param optionals
	 */
	public void setOptionals(Set<OptionalTO> optionals);
}
