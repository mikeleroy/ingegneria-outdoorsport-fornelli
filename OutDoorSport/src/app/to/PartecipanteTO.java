package app.to;

import java.util.Date;

/**
 * Realizzazione della classe PartecipanteTO che estende {@link UtenteTO}
 * .
 * @author michele fornelli
 *
 */
public interface PartecipanteTO extends UtenteTO {

	
	
	/**
	 * @return the idPartecipante
	 */
	int getIdPartecipante();


	/**
	 * @param idPartecipante the idPartecipante to set
	 */
	void setIdPartecipante(int idPartecipante);



	/**
	 * @return the numTessera
	 */
	String getNumTessera();




	/**
	 * @param numTessera the numTessera to set
	 */
	void setNumTessera(String numTessera);



	/**
	 * @return the dataSrc
	 */
	Date getDataSrc();



	/**
	 * @param dataSrc the dataSrc to set
	 */
	void setDataSrc(Date dataSrc);






	/**
	 * @return the fileSrc
	 */
	String getFileSrc();





	/**
	 * @param fileSrc the fileSrc to set
	 */
	void setFileSrc(String fileSrc);


	
}