package app.to;

import java.util.HashSet;
import java.util.Set;

/**
 * Classe che rappresenta lo stato di OptionalEscursione. Sono fornite tutte le 
 * dichiarazioni dei metodi per la lettura dei dati
 * 
 * @author michele fornelli
 * 
 *
 */
public interface OptionalEscursioneTO extends BaseTO{

	/**
	 * @return l'id di OptionalEscursione
	 */
	public Integer getIdOptionalEscursione();

	/**
	 * Metodo che setta l'id di OptionalEscursione
	 * 
	 * @param id
	 */
	public void setIdOptionalEscursione(Integer idOptionalEscursione);

	/**
	 * @return l'escursione collegata all'optional
	 */
	public EscursioneTO getEscursione();
	/**
	 * Metodo che setta l'escursione collegata all'optional
	 * 
	 * @param tblEscursione
	 */
	public void setEscursione(EscursioneTO escursione);

	/**
	 * @return l'optional collegato all'escursione
	 */
	public OptionalTO getOptional();
	/**
	 * Metodo che setta l'optional collegato all'escursione
	 * 
	 * @param tblOptional
	 */
	public void setOptional(OptionalTO optional);

	/**
	 * @return lo stato dell'optional
	 */
	public StatoOptionalTO getStatoOptional();
	
	/**
	 * Metodo che setta lo stato dell'optional
	 * 
	 * @param tblStatoOptional
	 */
	public void setStatoOptional(StatoOptionalTO stato);

}