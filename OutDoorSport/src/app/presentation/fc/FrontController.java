package app.presentation.fc;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.services.ServiceBase;
import app.services.ServiceLocator;
import app.services.ServiceType;

/**
 * <strong>Presentation Tier</strong><br />
 * La classe rappresenta un'implementazione del Front Controller (basandosi 
 * su quanto specificato nell'interfaccia).<br />
 * 
 * Classe che implementa il Front Controller. Grazie a questa classe � possibile centralizzare
 * le richieste che provengono dal livello di presentation. Se non ci fosse un punto di
 * centralizzazione, i livelli sottostanti diventerebbero accessibili da pi� parti, rendendo
 * l'applicazione meno modulare e coesa. Inoltre, avere codice duplicato in diversi punti, significa avere un'enorme
 * difficolt� in termini di manutenzione (un singolo cambiamento potrebbe richiedere un numero elevato di
 * modifiche al codice).
 * 
 * @author michele fornelli
 */

public class FrontController extends ServiceBase{

	
	/**
	 * Riferimento all'istanza di FrontController
	 */
	private static FrontController fc = new FrontController();

	/**
	 * Costruttore della classe FrontController privato
	 */
	private FrontController(){}

	/**
	 * Metodo che restituisce l'istanza del FrontController (Singleton)
	 * @return fc: istanza del FrontController
	 */
	public static FrontController getInstance(){
		if(service == null){
			try {
				service = ServiceLocator.getService(ServiceType.ApplicationController);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return fc;
	}

	/**
	 * Metodo che gestisce la richiesta specificata. Il compito di inviare i dati per ottenere
	 * la risposta � delegato all'application controller, che libera il front controller della richiesta.
	 * 
	 * @param richiesta che viene passata al front controller
	 * @return la risposta in base alla richiesta
	 */



	@Override
	public ServiceType getType() {
		return ServiceType.FrontController;
	}

	@Override
	public OutdoorResponse eseguiRichiesta(OutdoorRequest request) {
		return this.handle(request, service);

	}
}
