package app.presentation.request;

import app.to.BaseTO;
import javafx.scene.layout.Pane;

/**
 * Classe che gestisce le richieste da mandare nei vari livelli dell'architettura
 * 
 * @author michele fornelli
 */



public class OutdoorRequest {

	private Object data;
	private String request;
	private Pane view;
	
	public OutdoorRequest(){}

	public OutdoorRequest(String request,Pane view ,Object data  ) {
		this.data = data;
		this.request = request;
		this.view = view;
	}

	public OutdoorRequest(String request,Pane view ) {
		this.view = view;	
		this.request = request;
	}
	
	public OutdoorRequest( String request ) {
		this.request = request;
	}
	
	
	public OutdoorRequest(String request,Object data ) {
		this.data = data;
		this.request = request;
	}

	
	
	/**
	 * @return the view
	 */
	public Pane getView() {
		return view;
	}

	/**
	 * @param view the view to set
	 */
	public void setView(Pane view) {
		this.view = view;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}

	
	/**
	 * @return the data
	 */
	public Object getData() {
		return this.data;
	}


	/**
	 * Override del metodo toString() della classe Object che restituisce la richiesta effettiva
	 */
	@Override
	public String toString() {
		return this.request;
	}
	
	
	

	
}