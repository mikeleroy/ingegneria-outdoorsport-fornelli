package app.presentation.ac;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import app.presentation.response.OutdoorResponse;
/**
 * Views � una classe realizzata allo scopo associare a delle costanti chiavi-valore il reale percorso
 * di dove � posiziona la view da caricare quando il dispatcher la richiede
 * 
 * @author michele fornelli
 *
 */

class Views {
	//Utente
	private final String VIEW_LOGIN = "../../../resources/views/utente/login";
	private final String VIEW_REGISTRA_UTENTE = "../../../resources/views/utente/registraPartecipante";
	private final String VIEW_RECUPERA_PASSWORD = "../../../resources/views/utente/passwordDimenticata";
	private final String VIEW_MODIFICA_ACCESSO = "../../../resources/views/utente/datiAccesso";
	private final String VIEW_NUOVA_PASSWORD = "../../../resources/views/utente/nuovaPassword";
	
	//Partecipante
	private final String VIEW_DASHBOARD_PARTECIPANTE = "../../../resources/views/partecipante/dashboardPAR";
	private final String VIEW_TROVA_ESCURSIONI_PAR = "../../../resources/views/partecipante/trovaEscursioniPAR";
//	private final String VIEW_ISCRIZIONI_ESCURSIONI_PAR = "../../../resources/views/partecipante/escursioniPAR";
	private final String VIEW_ESCURSIONI_PAR = "../../../resources/views/partecipante/escursioniPAR";
	private final String VIEW_PROFILO_PAR = "../../../resources/views/partecipante/profiloPAR";
	private final String VIEW_MODIFICA_PAR = "../../../resources/views/partecipante/modificaPartecipante";
	private final String VIEW_ISCRIZIONE_ESCURSIONE = "../../../resources/views/partecipante/iscrizioneEscursione";
	private final String VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE = "../../../resources/views/partecipante/modificaIscrizioneEscursione";
	private final String VIEW_SELEZIONA_OPTIONAL_ISCRIZIONE = "../../../resources/views/partecipante/sceltaOptional";
	private final String VIEW_MODIFICA_OPTIONAL_ISCRIZIONE = "../../../resources/views/partecipante/modificaSceltaOptional";


	
	//Manager Escursione
	private final String VIEW_DASHBOARD_MAN_ESCURSIONE = "../../../resources/views/managerEscursione/dashboardManagerESC";
	private final String VIEW_PROFILO_MANAGER_ESC = "../../../resources/views/managerEscursione/profiloESC";
	private final String VIEW_LISTA_ESCURSIONI = "../../../resources/views/managerEscursione/listaEscursioni";
	private final String VIEW_NUOVA_ESCURSIONE = "../../../resources/views/managerEscursione/nuovaEscursione";
	private final String VIEW_LISTA_OPTIONAL = "../../../resources/views/managerEscursione/listaOptional";
	private final String VIEW_LISTA_PARTECIPANTI = "../../../resources/views/managerEscursione/listaPartecipanti";
	private final String VIEW_DETTAGLI_ESCURSIONE_MDE = "../../../resources/views/managerEscursione/dettagliEscursione";
	private final String VIEW_MODIFICA_ESCURSIONE = "../../../resources/views/managerEscursione/modificaEscursione";
	private final String VIEW_ISCRITTI_ESCURSIONE = "../../../resources/views/managerEscursione/listaPartecipanti";
	//private final String VIEW_MODIFICA_MANAGER_ESC = "VIEW_MODIFICA_MANAGER_ESC";

	
	
	//Manager Sistema
	private final String VIEW_DASHBOARD_MAN_SISTEMA ="../../../resources/views/managerSistema/dashboardManagerSIS";
	private final String VIEW_PROFILO_MANAGER_SIS ="../../../resources/views/managerSistema/profiloSIS";
	private final String VIEW_ESCURSIONI_SIS ="../../../resources/views/managerSistema/escursioniSIS";
	private final String VIEW_LISTA_MANAGER_SIS ="../../../resources/views/managerSistema/listaManager";
	private final String VIEW_AGGIUNGI_MANAGER_ESC ="../../../resources/views/managerSistema/aggiungiManager";
	private final String VIEW_DETTAGLI_ESCURSIONE_SIS ="../../../resources/views/managerSistema/dettagliEscursione";
	private final String VIEW_ISCRITTI_ESCURSIONE_SIS ="../../../resources/views/managerSistema/listaPartecipantiEscursione";

	

	
	
	public String getView(String view) {
		Field v;
		try {
			v = getClass().getDeclaredField(view);
			v.setAccessible(true);
			return v.get(this).toString();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			return e.toString();
		} catch (SecurityException e) {
			e.printStackTrace();
			return e.toString();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return e.toString();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return e.toString();
		}

	}

}
