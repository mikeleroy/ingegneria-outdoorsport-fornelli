package app.presentation.ac;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.services.ServiceBase;
import app.services.ServiceLocator;
import app.services.ServiceType;

/**
 * Classe che rappresenta un'implementazione dell'Application Controller
 * Nel livello di presentazione Ŕ importante risolvere la richiesta di arrivo con una azione
 * che la porti a termine, e capire quale view visualizzare in base alla risposta.
 * Tale strategia migliora la modularitÓ, l'estendibilitÓ e la riusabilitÓ del codice.
 * 
 * 
 * @author michele fornelli
 *
 */

class ApplicationController extends ServiceBase{
	private static ServiceBase service = null;
	
	private static ApplicationController applicationController = new ApplicationController();
	
	private static Dispatcher dispatcher = Dispatcher.getInstance();
	
	/**
	 * Costruttore privato
	 */
	private ApplicationController() {}
	
	/**
	 * 
	 * @return restituisce l'istanza dell'applicationController
	 * pattern singleton
	 */
	static ApplicationController getInstance(){
		if(service == null){
			try {
				service = ServiceLocator.getService(ServiceType.BusinessDelegate);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return applicationController;
	}
	
	/**
	 * Metodo che invia la richiesta al servizio del business delegate e ottiene come risposta
	 * una azione da mandare al front controller.
	 * Nel caso in cui ricevesse una richiesta di visualizzazione di una view, la manda al dispatcher.
	 * 
	 * @param richiesta che viene passata all'Application Controller
	 * @return la risposta in base alla richiesta
	 */
	OutdoorResponse getAction(OutdoorRequest request){
		return this.eseguiRichiesta(request);
	}

	@Override
	public ServiceType getType() {
		return null;
	}


	@Override
	protected OutdoorResponse eseguiRichiesta(OutdoorRequest request) {
		if(request.toString().contains("VIEW_")){
			return dispatcher.dispatch(request);
		}else
			return this.handle(request, service);
	}
}
