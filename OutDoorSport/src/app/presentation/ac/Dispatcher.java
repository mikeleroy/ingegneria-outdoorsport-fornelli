package app.presentation.ac;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import javafx.scene.layout.AnchorPane;

/**
 * La classe Dispatcher si occupa di gestire le richieste di visualizzazione che arrivano in ingresso
 * e poi mostrarle, grazie anche all'accesso della cache delle Views (ViewsCache)
 * 
 * @author michele fornelli
 *
 */
class Dispatcher {

	private static ViewsCache vc = null;
	private static Dispatcher dispatcher = new Dispatcher();;
	
	/**
	 * Costruttore privato
	 */
	private Dispatcher(){
	}
	
	/**
	 * 
	 * @return restituisce l'istanza del Dispatcher
	 */
	public static Dispatcher getInstance(){
		if(vc == null){
			try {
				vc = ViewsCache.getInstance();
				vc.initialize();				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return dispatcher;
	}
	
	/**
	 * Metodo privato di dispatcher che consente di settare a video una schermata avendo accesso
	 * alla cache delle view.
	 * @param request: richiesta in ingresso
	 */
	private void setView(OutdoorRequest request){
		vc.setView(request);
	}
	
	/**
	 * Metodo privato di dispatcher che consente di settare a video una schermata figlia avendo accesso
	 * alla cache delle view.
	 * @param request: richiesta in ingresso
	 */
	private void setNestedView(OutdoorRequest request, AnchorPane pane){
		vc.setNestedView(request, pane);
	}

	/**
	 * Esegue l'operazione di visualizzazione della schermata a seconda
	 * della richiesta in ingresso.
	 * @param request: richiesta della vista in ingresso
	 * @return null
	 */
	OutdoorResponse dispatch(OutdoorRequest request){
		if(request.getView() == null){
			setView(request);
		}else if(request.getView() instanceof AnchorPane){
			setNestedView(request, (AnchorPane)request.getView());
		}
		return null;
	}
}
