package app.presentation.ac;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.services.ServiceBase;
import app.services.ServiceType;

/**
 * Classe di servizio, invocata dal Service Locator, per collegare il 
 * FrontController all'Application Controller. In questo modo si nascondono 
 * i vari passaggi della richiesta senza che ci sia possibilitÓ di essere richiamati da altre
 * parti del programma.
 * 
 * @author michele fornelli
 */

public class ServiceAC extends ServiceBase{

	/**
	 * Chiama l'istanza di ApplicationController
	 */
	private static ApplicationController applicationController = ApplicationController.getInstance();
	
	public ServiceAC(){}
	
	@Override
	public ServiceType getType() {
		return ServiceType.ApplicationController;
	}

	@Override
	protected OutdoorResponse eseguiRichiesta(OutdoorRequest request) {
		return applicationController.getAction(request);
	}

}
