package app.presentation.ac;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Queue;

import app.presentation.request.OutdoorRequest;
import app.utility.SessionData;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * ViewsCache � una classe che ha lo scopo di precaricare le schermate di 
 * visualizzazione richieste dal frontcontroller e gestite dal dispatcher in una 
 * cache temporeanea, in modo da non rallentare le operazioni del programma
 * 
 * @author michele fornelli 
 *
 */
public class ViewsCache extends SessionData{
	
	private static HashMap<String, Pane> mapViews = new HashMap<>();
	private static Queue<Stage> stageQueue = new ArrayDeque<>();
	private static AnchorPane nestedAnchorPane;
	private Views view;
	
	private static ViewsCache viewCache = new ViewsCache();

	/**
	 * Costruttore privato - Singleton Class
	 */
	private ViewsCache(){
		view = new Views();
	}

	/**
	 * 
	 * @return viewCache: restituisce l'istanza ViewCache
	 */
	public static ViewsCache getInstance(){
		return viewCache;
	}

	/**
	 * Metodo di creazione cache in memoria di tutte le schermate 
	 * @throws Exception
	 */
	void initialize() throws Exception{
		loadForm("VIEW_LOGIN",view.getView("VIEW_LOGIN"), true);
		loadForm("VIEW_REGISTRA_UTENTE",view.getView("VIEW_REGISTRA_UTENTE"), true);
		loadForm("VIEW_RECUPERA_PASSWORD",view.getView("VIEW_RECUPERA_PASSWORD"), true);
		loadForm("VIEW_NUOVA_PASSWORD",view.getView("VIEW_NUOVA_PASSWORD"), true);
		loadForm("VIEW_MODIFICA_ACCESSO",view.getView("VIEW_MODIFICA_ACCESSO"), false);
		
		
		//Partecipante
		loadForm("VIEW_DASHBOARD_PARTECIPANTE",view.getView("VIEW_DASHBOARD_PARTECIPANTE"), true);
		loadForm("VIEW_TROVA_ESCURSIONI_PAR",view.getView("VIEW_TROVA_ESCURSIONI_PAR"), false);
//		loadForm("VIEW_ISCRIZIONI_ESCURSIONI_PAR",view.getView("VIEW_ISCRIZIONI_ESCURSIONI_PAR"), false);
		loadForm("VIEW_ESCURSIONI_PAR",view.getView("VIEW_ESCURSIONI_PAR"), false);
		loadForm("VIEW_PROFILO_PAR",view.getView("VIEW_PROFILO_PAR"), false);
		loadForm("VIEW_MODIFICA_PAR",view.getView("VIEW_MODIFICA_PAR"), false);
		loadForm("VIEW_ISCRIZIONE_ESCURSIONE",view.getView("VIEW_ISCRIZIONE_ESCURSIONE"), false);
		loadForm("VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE",view.getView("VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE"), false);
		loadForm("VIEW_SELEZIONA_OPTIONAL_ISCRIZIONE",view.getView("VIEW_SELEZIONA_OPTIONAL_ISCRIZIONE"), false);
		loadForm("VIEW_MODIFICA_OPTIONAL_ISCRIZIONE",view.getView("VIEW_MODIFICA_OPTIONAL_ISCRIZIONE"), false);


		
		//Manager Escursione
		loadForm("VIEW_DASHBOARD_MAN_ESCURSIONE",view.getView("VIEW_DASHBOARD_MAN_ESCURSIONE"), true);
		loadForm("VIEW_PROFILO_MANAGER_ESC",view.getView("VIEW_PROFILO_MANAGER_ESC"), false);
		loadForm("VIEW_NUOVA_ESCURSIONE",view.getView("VIEW_NUOVA_ESCURSIONE"), false);
		loadForm("VIEW_LISTA_ESCURSIONI",view.getView("VIEW_LISTA_ESCURSIONI"), false);
		loadForm("VIEW_LISTA_OPTIONAL",view.getView("VIEW_LISTA_OPTIONAL"), false);
		loadForm("VIEW_LISTA_PARTECIPANTI",view.getView("VIEW_LISTA_PARTECIPANTI"), false);
		loadForm("VIEW_DETTAGLI_ESCURSIONE_MDE",view.getView("VIEW_DETTAGLI_ESCURSIONE_MDE"), false);
		loadForm("VIEW_ISCRITTI_ESCURSIONE",view.getView("VIEW_ISCRITTI_ESCURSIONE"), false);
		loadForm("VIEW_MODIFICA_ESCURSIONE",view.getView("VIEW_MODIFICA_ESCURSIONE"), false);
		
		

		//Manager Sistema
		loadForm("VIEW_DASHBOARD_MAN_SISTEMA",view.getView("VIEW_DASHBOARD_MAN_SISTEMA"), true);
		loadForm("VIEW_PROFILO_MANAGER_SIS",view.getView("VIEW_PROFILO_MANAGER_SIS"), false);
		loadForm("VIEW_ESCURSIONI_SIS",view.getView("VIEW_ESCURSIONI_SIS"), false);
		loadForm("VIEW_LISTA_MANAGER_SIS",view.getView("VIEW_LISTA_MANAGER_SIS"), false);
		loadForm("VIEW_AGGIUNGI_MANAGER_ESC",view.getView("VIEW_AGGIUNGI_MANAGER_ESC"), false);
		loadForm("VIEW_DETTAGLI_ESCURSIONE_SIS",view.getView("VIEW_DETTAGLI_ESCURSIONE_SIS"), false);
		loadForm("VIEW_ISCRITTI_ESCURSIONE_SIS",view.getView("VIEW_ISCRITTI_ESCURSIONE_SIS"), false);

	}
	

	
	/**
	 * Setta la vista richiesta dall'utente e chiude quella precedente
	 * @param key: chiave della schermata
	 */
	void setView(OutdoorRequest request){
		Stage currentStage = null;
		Parent root = mapViews.get(request.toString());
		Scene myScene = null;
		
		if(!stageQueue.isEmpty()){
			currentStage = stageQueue.poll();
			myScene = currentStage.getScene();
			currentStage.close();			
		}else
			currentStage = new Stage();
			
		
		this.setSessionData(request);

		
		currentStage.setTitle("OutDoorSports 1.0");
		currentStage.setResizable(false);
		
		if(myScene == null){
			myScene = new Scene(root);
			currentStage.setScene(myScene);
		}else
			currentStage.getScene().setRoot(root);
		
		currentStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent event) {
				Platform.exit();
				System.exit(0);				
			}
		});
		
		currentStage.show();
		
		stageQueue.add(currentStage);
		
		
		//reset cache
		if(request.toString().equals("VIEW_LOGIN")){
			this.resetSessionData();
		}
		
		
	}

	
	/**
	 * Metodo che consente di settare la vista di una sotto-schermata richiesta se quest'ultima si trova in una schermata principale
	 * @param key: chiave sotto-finestra richiesta
	 * @param parent: finestra genitore
	 */
	void setNestedView(OutdoorRequest request, AnchorPane parent){
		StackPane panel = (StackPane)mapViews.get(request.toString());
		this.setSessionData(request);
//		
	setNestedAnchorPane(parent);
		AnchorPane.setLeftAnchor(panel, 0.0);
		AnchorPane.setRightAnchor(panel, 0.0);
		AnchorPane.setTopAnchor(panel, 0.0);
		AnchorPane.setBottomAnchor(panel, 0.0);
		if(!parent.getChildren().isEmpty())
			parent.getChildren().get(0).setVisible(false);
		parent.getChildren().clear();
		if(!panel.isVisible())
			panel.setVisible(true);
		parent.getChildren().add(panel);
	}
	
	
	
	
	
	
	
	/**
	 * Metodo di caricamento nella cache della singola schermata
	 * @param key: chiave schermata
	 * @param resource: percorso risorsa da caricare
	 * @param visibility: imposta quale view deve essere subito visibile e quale no
	 */
	private void loadForm(String key, String resource, boolean visibility){
		try {
			Pane newPane = FXMLLoader.load(getClass().getResource(resource + ".fxml"));
			newPane.setVisible(visibility);
			mapViews.put(key, newPane);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Metodo che setta la view innestata corrente
	 * 
	 * @param anchorPane: la view genitore che deve contenere la nuova view
	 */
	private void setNestedAnchorPane(AnchorPane anchorPane){
		nestedAnchorPane = anchorPane;
	}
	
	/**
	 * 
	 * @return stageQueue.peak(): Restituisce lo stage della schermata corrente
	 */
	public static Stage getCurrentView(){
		return stageQueue.peek();
	}
	
	/**
	 * @return la view innestata corrente
	 */
	public static AnchorPane getNestedAnchorPane(){
		return nestedAnchorPane;
	}
	
	

}
