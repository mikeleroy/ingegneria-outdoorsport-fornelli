package app.presentation.response;


import app.to.BaseTO;
import app.to.UtenteTO;
import javafx.scene.layout.Pane;

/**
 * Classe che gestisce le risposte da ricevere dai vari livelli dell'architettura
 * 
 * @author michele fornelli
 */


public class OutdoorResponse {

	private Object data;
	private String response;
	private String view;

	
	
	public OutdoorResponse(){}



	/**
	 * @return the view
	 */
	public String getView() {
		return view;
	}



	/**
	 * @param view the view to set
	 */
	public void setView(String view) {
		this.view = view;
	}



	/**
	 * @return the data
	 */
	public Object getData() {
		return this.data;
	}


	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}


	/**
	 * Override del metodo toString() della classe Object che restituisce la risposta effettiva
	 */
	@Override
	public String toString() {
		return this.response;
	}
	
	
}