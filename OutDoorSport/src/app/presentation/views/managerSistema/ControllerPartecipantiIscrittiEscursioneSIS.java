package app.presentation.views.managerSistema;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.TableViewController;
import app.presentation.views.tableModels.TableModelPAR;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.PartecipanteTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class ControllerPartecipantiIscrittiEscursioneSIS extends TableViewController{

	@FXML private TextField txtRicerca;
	@FXML private Button btnRicercaPAR;
	@FXML private TableView<TableModelPAR> tblListaPartecipanti;
	@FXML private TableColumn<TableModelPAR, String> columnNome;
	@FXML private TableColumn<TableModelPAR, String> columnCognome;
	@FXML private TableColumn<TableModelPAR, String> columnEmail;
	@FXML private TableColumn<TableModelPAR, String> columnCF;
	@FXML private TableColumn<TableModelPAR, String> columnDataSRC;
	@FXML private Label lblNomeEscursione;
	@FXML private Button btnIndietro;
	@FXML private Button btnModificaDatiEscursione;
	@FXML private Button btnEliminaIscrizione;
	@FXML private StackPane stackListaIscritti;
	private PartecipanteTO partecipante = null;
	private List<PartecipanteTO> list_partecipanti = new ArrayList<>();
	private List<IscrizioneTO> list_iscrizioni = null;
	private IscrizioneTO iscrizione = null;
	private TableModelPAR partecipante_model = null;
	private EscursioneTO escursione = null;
	
	public ControllerPartecipantiIscrittiEscursioneSIS() {
		if(partecipante == null){
			partecipante = (PartecipanteTO) FactoryTO.getFactoryTO(EnumTO.Partecipante);
		}
		if(iscrizione == null){
			iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
	}

	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					escursione = (EscursioneTO) SessionData.getSessionData(escursione.getClass().getSimpleName());
					iscrizione.setEscursione(escursione);
					lblNomeEscursione.setText(escursione.getNome());
					allPartecipantiIscritti();
				}
			}
		};

		stackListaIscritti.visibleProperty().addListener(visibilityListener);
	}
	
	@FXML protected void btnRicercaPAR(){
		cercaPartecipante();
	}
	
	/**
	 * Metodo che carica tutti i Partecipanti Iscritti all'Escursione
	 * all'Escursione
	 */
	@SuppressWarnings("unchecked")
	private void allPartecipantiIscritti(){
		OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_ISCRITTI_FROM_ESCURSIONE,iscrizione));
		list_iscrizioni = (ArrayList<IscrizioneTO>) response.getData();
		
		list_partecipanti.clear();
		for(IscrizioneTO i : list_iscrizioni){
			list_partecipanti.add((PartecipanteTO) i.getUtente());
		}

		ObservableList<TableModelPAR> data = FXCollections.observableArrayList(getListTabellaPartecipanti(list_partecipanti));

		this.initColumn(columnNome, "nome");
		this.initColumn(columnCognome, "cognome");
		this.initColumn(columnEmail, "email");
		this.initColumn(columnCF, "codiceFiscale");
	//	this.initColumn(columnDataSRC, "dataSRC");

		tblListaPartecipanti.setItems(data);
		
		tblListaPartecipanti.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				tblListaPartecipanti = (TableView<TableModelPAR>) event.getSource();
				partecipante_model = tblListaPartecipanti.getSelectionModel().getSelectedItem();
				if(partecipante_model != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
						aggiornaDatiIscrizione();
		            }
				}
			}
		});
	}
	
	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati nella tabella
	 * 
	 * @param lista di Partecipanti iscritti
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelPAR> getListTabellaPartecipanti(List<PartecipanteTO> list_partecipanti){
		ObservableList<TableModelPAR> res = FXCollections.observableArrayList();

		for(PartecipanteTO iscritto : list_partecipanti){
			partecipante_model = new TableModelPAR(iscritto);
			res.add(partecipante_model);
		}
		
		return res;
	}
	
	/**
	 * Evento associato al Button per la ricerca di un Partecipante iscritto. 
	 * La tabella presente nella View verr� ricaricata in base 
	 * ai parametri inseriti nella casella di testo.
	 */
	@FXML protected void cercaPartecipante(){
		String param = txtRicerca.getText();
		List<PartecipanteTO> list_partecipante = new ArrayList<>();
		
		for(PartecipanteTO partecipante : this.list_partecipanti){
			if(partecipante.getNome().contains(param) ||
			   partecipante.getCognome().contains(param) ||
			   partecipante.getEmail().contains(param) ||
			   partecipante.getCf().contains(param) ||
			   partecipante.getFileSrc().contains(param)
			   ){
			   list_partecipante.add(partecipante);
			}
		}
		
		ObservableList<TableModelPAR> data = FXCollections.observableArrayList(getListTabellaPartecipanti(list_partecipante));

		tblListaPartecipanti.setItems(data);
	}
	
	/**
	 * Metodo che cancella l'iscrizione di un determinato Partecipante,
	 * e gli viene inviata una mail di conferma.
	 */
	private void deleteIscrizione(){
		partecipante_model = tblListaPartecipanti.getSelectionModel().getSelectedItem();
		for(IscrizioneTO iscrizione : list_iscrizioni){
			if(iscrizione.getUtente().equals(partecipante_model.getPartecipante())){
				this.iscrizione = iscrizione;
			}
		}
		
		if(!(partecipante_model == null)){
			OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_ANNULLA_ISCRIZIONE,this.iscrizione));
			allPartecipantiIscritti();
			if(response.getData() != null){
				Alert alertConfirm = new Alert(AlertType.CONFIRMATION, "Sei sicuro di voler annullare l'iscrizione?");
				Optional<ButtonType> result = alertConfirm.showAndWait();
				if (result.get() == ButtonType.OK){
				    Alert alert = new Alert(AlertType.INFORMATION, "Iscrizione annullata con successo!", ButtonType.OK);
				    alert.setTitle("OutDoorSport1.0");
				    alert.showAndWait();
				} else {
					alertConfirm.close();
					Alert alert = new Alert(AlertType.INFORMATION, "Non � stata apportata nessuna modifica all'iscrizione.", ButtonType.OK);
					alert.setTitle("OutDoorSport1.0");
					alert.showAndWait();
				}
			}else{
				Alert alert = new Alert(AlertType.INFORMATION, "Non � stata apportata nessuna modifica all'iscrizione.", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");
				alert.showAndWait();
			}

		}else{
			Alert alert = new Alert(AlertType.ERROR, "Nessun Partecipante Selezionato", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}
	
	/**
	 * Evento associato all'eliminazione di una iscrizione in una escursione
	 * da parte del Manager di Escursione
	 */
	@FXML protected void eliminaIscrizione(){
		deleteIscrizione();
	}
	
	/**
	 * Ritorna alla schermata precedente
	 */
	@FXML protected void indietro(){
		eseguiRichiesta(new OutdoorRequest(VIEW_ESCURSIONI_SIS,ViewsCache.getNestedAnchorPane()));
	}
	
	
	/**
	 * Visualizza i dettagli dell'iscrizione di un iscritto cliccando
	 * direttamente sulla tabella. Pu� modificare gli optional
	 * e, di conseguenza, il costo risultante.
	 */
	private void aggiornaDatiIscrizione(){
		partecipante_model = tblListaPartecipanti.getSelectionModel().getSelectedItem();
		for(IscrizioneTO iscrizione : list_iscrizioni){
			if(iscrizione.getUtente().equals(partecipante_model.getPartecipante())){
				this.iscrizione = iscrizione;
			}
		}
		eseguiRichiesta(new OutdoorRequest(VIEW_DETTAGLI_ESCURSIONE_SIS, ViewsCache.getNestedAnchorPane(), iscrizione));
	}
}
