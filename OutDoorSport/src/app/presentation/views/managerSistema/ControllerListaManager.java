package app.presentation.views.managerSistema;

import java.util.ArrayList;
import java.util.List;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.TableViewController;
import app.presentation.views.tableModels.TableModelMDE;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.impl.FactoryTO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;



public class ControllerListaManager extends TableViewController{

	
	@FXML private TextField txtCercaManager;
	@FXML private Button btnCercaManager;
	@FXML private StackPane stackListaManager;
	@FXML private TableView<TableModelMDE> tblListaManager;
	@FXML private TableColumn<TableModelMDE, String> columnNomeMDE;
	@FXML private TableColumn<TableModelMDE, String> columnCognomeMDE;
	@FXML private TableColumn<TableModelMDE, String> columnEmailMDE;
	@FXML private Button btnModificaMDE;
	@FXML private Button btnDettagliMDE;
	private ManagerDiEscursioneTO mde = null;
	private List<ManagerDiEscursioneTO> listMDE = null;
	private TableModelMDE modelMDE = null;
	
	/**
	 * Costruttore che avvalora il partecipante in memoria
	 */
	public ControllerListaManager() {
		if(mde == null){
			mde = (ManagerDiEscursioneTO) FactoryTO.getFactoryTO(EnumTO.ManagerDiEscursione);

		}
	}
	
	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue)
					listAllMDE();
			}
		};

		stackListaManager.visibleProperty().addListener(visibilityListener);
		
	}

	
	
	@FXML protected void btnAggiungiManager(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_AGGIUNGI_MANAGER_ESC,ViewsCache.getNestedAnchorPane()));
	}
	
	
	/**
	 * Metodo che carica tutti i Manager di Escursione presente nel database
	 */
	@SuppressWarnings("unchecked")
	private void listAllMDE(){
		OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_LISTA_MDE,mde));
		listMDE = (ArrayList<ManagerDiEscursioneTO>) response.getData();

		ObservableList<TableModelMDE> data = FXCollections.observableArrayList(getListaTabellaMDE(listMDE));

		this.initColumn(columnNomeMDE, "nome");
		this.initColumn(columnCognomeMDE, "cognome");
		this.initColumn(columnEmailMDE, "email");

		tblListaManager.setItems(data);
		
		tblListaManager.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				TableView<TableModelMDE> table = (TableView<TableModelMDE>) event.getSource();
				modelMDE = table.getSelectionModel().getSelectedItem();
				if(modelMDE != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
		                eseguiRichiesta(new OutdoorRequest( VIEW_AGGIUNGI_MANAGER_ESC,ViewsCache.getNestedAnchorPane(), mde));
		            }
				}
			}
		});
	}
	
	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati nella tabella
	 * 
	 * @param lista di Manager di Escursione
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelMDE> getListaTabellaMDE(List<ManagerDiEscursioneTO> listMDE){
		ObservableList<TableModelMDE> res = FXCollections.observableArrayList();

		for(ManagerDiEscursioneTO mde : listMDE){
			modelMDE = new TableModelMDE(mde);
			res.add(modelMDE);
		}

		return res;
	}
	
	
}
