package app.presentation.views.managerSistema;


import java.io.File;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import org.omg.CORBA.Request;

import app.integration.daoUtil.DatabaseException;
import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.PartecipanteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Rules;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

public class ControllerAggiungiManager extends BaseController implements Rules{

	@FXML private Button btnCancel;
	@FXML private Button btnCaricaSRC;
	@FXML private AnchorPane OutDoorSportPane;
	@FXML private Label lblTitolo;
	@FXML private Label lblPassLost;
	@FXML private Label lblMessage;
	@FXML private Label btnPswDimenticata;
	@FXML private Label lblPathSRC;
	

	@FXML private TextField txtNome;
	@FXML private TextField txtCognome;
	@FXML private TextField txtCF;
	@FXML private TextField txtUsername;
	@FXML private TextField txtStipendio;
	@FXML private PasswordField txtPassword;
	@FXML private TextField txtShowPassword;
	@FXML private CheckBox checkShowPassword;
	@FXML private TextField txtIndirizzo;
	@FXML private TextField txtCitta;
	@FXML private RadioButton radioM;
	@FXML private RadioButton radioF;
	@FXML private TextField txtEmail;
	@FXML private DatePicker txtDataNasc;
	@FXML private Button btnRegistrati;
	@FXML private Button btnReset;
	@FXML private Label lblErrore;
	@FXML private Button btnIndietro;

	private Calendar c =  Calendar.getInstance();
	private Date dateNasc;
	private ManagerDiEscursioneTO mde = null;
	private OutdoorResponse risposta;


	public ControllerAggiungiManager() {
		if(mde == null){
			mde = (ManagerDiEscursioneTO) FactoryTO.getFactoryTO(EnumTO.ManagerDiEscursione);
		}
	}
	
	@FXML protected void txtKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER){
			lblMessage.setVisible(true);
			lblMessage.setText("Premere il tasto LOGIN");
			lblMessage.setTextFill(Color.BLUE);
//			txtEmail.setText("");
			txtPassword.setText("");
		}
		
	}

	
	@FXML protected void btnCancel(MouseEvent event) throws DatabaseException {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_MANAGER_SIS));

	}
	

	@FXML protected void btnSaveMDE(MouseEvent event) {	
		saveMDE();
	}
	
	
	
	private void saveMDE(){
		mde.setNome(txtNome.getText());
		mde.setCognome(txtCognome.getText());
		mde.setCf(txtCF.getText());
		mde.setUsername(txtUsername.getText());
		mde.setPassword(txtPassword.getText());
		mde.setIndirizzo(txtIndirizzo.getText());
		mde.setCitta(txtCitta.getText());
		mde.setEmail(txtEmail.getText());
		
		double stipendio = Double.parseDouble(txtStipendio.getText());
		mde.setStipendio(stipendio);
		
		if(radioM.isSelected())
			mde.setSesso(MALE);
		else if(radioF.isSelected())
			mde.setSesso(FEMALE);
		
		LocalDate dataNasc = txtDataNasc.getValue();
		
		c.set(dataNasc.getYear(), dataNasc.getMonthValue(), dataNasc.getDayOfMonth());
		 dateNasc = c.getTime();
		
		 mde.setDataNasc(dateNasc);
		
		
		String result = checkErrors(mde);
		if(result.equals(""))
		{
			 risposta = this.eseguiRichiesta(new OutdoorRequest(ACTION_NUOVO_MDE,mde));
			 if(risposta.getResponse().equals(SUCCESS))
				this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_MANAGER_SIS,ViewsCache.getNestedAnchorPane()));
			 else
				lblErrore.setText("Errore! Email o Username gi� presenti!");
		}
		else
		{
			lblErrore.setText(result);	
		}
		
		
}

	
	@FXML protected void btnReset(MouseEvent event) {	
		resetField();
	}
	
	
	
	private void resetField() {
		txtNome.setText("");
		txtCognome.setText("");
		txtUsername.setText("");
		txtEmail.setText("");
		txtPassword.setText("");
		txtShowPassword.setText("");
		txtIndirizzo.setText("");
		txtStipendio.setText("");
		txtCitta.setText("");
		radioF.setSelected(false);
		radioM.setSelected(false);
		
		
	}

	@Override
	protected void initialize() {

	//	lblErrore.setText("");

		final ToggleGroup group = new ToggleGroup();
		radioM.setToggleGroup(group);
		radioF.setToggleGroup(group);
		
		txtCF.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
	            if (txtCF.getText().length() > 16) {
	                String s = txtCF.getText().substring(0, 16);
	                txtCF.setText(s);
	            }
	        }
	    });
		
		
		//controllo nome, cognome e citta niente numeri
		
		checkShowPassword.selectedProperty().addListener(new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) {
				if(newVal){
					txtPassword.setVisible(false);
					txtShowPassword.setVisible(true);
					txtShowPassword.setText(txtPassword.getText());
				} else {
					txtPassword.setVisible(true);
					txtPassword.setText(txtShowPassword.getText());
					txtShowPassword.setVisible(false);
				}
			}
		});
		
		group.selectToggle(radioM);
		
		
	}
}