package app.presentation.views.managerSistema;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseEscursioneController;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.OptionalTO;
import app.to.StatoEscursioneTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

/**
 * Visualizza i dettagli di una determinata Escursione. Il Manager
 * di Escursione pu� visualizzare tutti i Partecipanti iscritti
 * all'escursione, modificare l'escursione o annullarla
 * 
 * @author michele fornelli
 *
 */

public class ControllerDettagliEscursione extends BaseEscursioneController{

	@FXML private StackPane stackDettagliEscursione;
	@FXML private Label lblNomeEscursione;
	@FXML private Label lblStatoEscursione;
	@FXML private Label lblTipoEscursione;
	@FXML private Label lblDataEscursione;
	@FXML private Label lblMDE;
	@FXML private Label lblNumMin;
	@FXML private Label lblNumMax;
	@FXML private Label lblPrezzoEscursione;
	@FXML private Label lblOptionalEscursione;
	@FXML private Label lblDescrizioneEscursione;
	@FXML private Button btnModificaEscursione;
	@FXML private Button btnAnnullaEscursione;
	@FXML private Button btnVisualizzaIscritti;
	@FXML private Button btnIndietro;
	private EscursioneTO escursione = null;
	private List<StatoEscursioneTO> list_stato_escursione = new ArrayList<>();
	private StatoEscursioneTO stato_escursione = null;
	
	
	public ControllerDettagliEscursione() {
		if(stato_escursione == null){
			stato_escursione = (StatoEscursioneTO)FactoryTO.getFactoryTO(EnumTO.StatoEscursione);
		}
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
	}

	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					escursione = (EscursioneTO) SessionData.getSessionData(escursione.getClass().getSimpleName());
					lblNomeEscursione.setText(escursione.getNome());
					lblMDE.setText("Manager: " + escursione.getUtente().getNome() + " " + escursione.getUtente().getCognome());
					lblStatoEscursione.setText( escursione.getStatoEscursione().getNome());
					lblTipoEscursione.setText(escursione.getTipoEscursione().getNome());
					lblDataEscursione.setText(escursione.getData());
					lblNumMin.setText( escursione.getNumMin() + " Partecipanti");
					lblNumMax.setText(escursione.getNumMax() + " Partecipanti");
					lblPrezzoEscursione.setText("�" + escursione.getPrezzo());
						
					lblDescrizioneEscursione.setText("Descrizione: " + escursione.getDescrizione());
					if(list_stato_escursione.isEmpty()){
						OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_STATO_ESCURSIONE,stato_escursione));
						list_stato_escursione = (List<StatoEscursioneTO>) response.getData();
					}
					
					String optionals = "";
					for(OptionalTO op : escursione.getOptionals()){
						optionals += op.getNome() + "\n";
					}
					lblOptionalEscursione.setText("Optional: " + optionals);

				}
			}
		};
		
		stackDettagliEscursione.visibleProperty().addListener(visibilityListener);
	}
	
	/**
	 * Evento associato alla view. Torna alla schermata precedente
	 */
	@FXML protected void tornaIndietro(){
		this.eseguiRichiesta(new OutdoorRequest(VIEW_ESCURSIONI_SIS, ViewsCache.getNestedAnchorPane() ));
	}
	
	/**
	 * Evento associato alla view. Visualizza gli iscritti dell'escursione
	 */
	@FXML protected void visualizzaIscritti(){
		this.eseguiRichiesta(new OutdoorRequest(VIEW_ISCRITTI_ESCURSIONE_SIS, ViewsCache.getNestedAnchorPane(),escursione ));
	}
	

	
	

	
	@Override
	protected void controlloDataEscursione(EscursioneTO escursione) {
		// TODO Auto-generated method stub
		
	}	
}
