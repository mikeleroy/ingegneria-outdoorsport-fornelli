package app.presentation.views.managerSistema;


import java.io.IOException;

import app.presentation.fc.FrontController;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;



public class ControllerDashboardSIS extends BaseController{

	private OutdoorRequest richiesta;
	private OutdoorResponse risposta;
	@FXML private AnchorPane anchorContentSIS;

	


	
	
	@Override
	protected void initialize() {
		
	}	

	@FXML protected void lblLogout(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_LOGIN));
	}
	
	@FXML protected void btnListaEscursioni(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_ESCURSIONI_SIS,anchorContentSIS));
		 }
	
	@FXML protected void btnListaManager(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_MANAGER_SIS,anchorContentSIS));
	}
	
	@FXML protected void btnProfilo(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_MANAGER_SIS,anchorContentSIS));
	}
	
	
	

	
}
