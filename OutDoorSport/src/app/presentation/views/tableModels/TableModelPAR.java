package app.presentation.views.tableModels;


import app.to.PartecipanteTO;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Classe che implementa il modello che servir� per la rappresentazione
 * dei dati nella tabella di una View. In questa classe � presentato il
 * Partecipante. La classe estende il modello dell'Utente perch�
 * avr� dei dati in comune.
 * 
 * @author michele fornelli
 */

public class TableModelPAR extends TableModelUtente{

	private SimpleStringProperty numTesseraSan;
	private SimpleStringProperty certificatoSRC;
	private SimpleStringProperty dataSRC;
	private PartecipanteTO partecipante;
	
	public TableModelPAR(){}
	
	public TableModelPAR(PartecipanteTO partecipante) {
		this.id = new SimpleIntegerProperty(partecipante.getIdUtente());
		this.nome = new SimpleStringProperty(partecipante.getNome());
		this.cognome = new SimpleStringProperty(partecipante.getCognome());
		this.email = new SimpleStringProperty(partecipante.getEmail());
		this.username = new SimpleStringProperty(partecipante.getUsername());
		this.password = new SimpleStringProperty(partecipante.getPassword());
		this.dataNascita = new SimpleStringProperty("par.getDataNasc()");
		this.codiceFiscale = new SimpleStringProperty(partecipante.getCf());
		this.sesso = new SimpleStringProperty(partecipante.getSesso());
		this.indirizzo = new SimpleStringProperty(partecipante.getIndirizzo());
		this.citta = new SimpleStringProperty(partecipante.getCitta());
		this.numTesseraSan = new SimpleStringProperty(partecipante.getNumTessera());
		this.certificatoSRC = new SimpleStringProperty(partecipante.getFileSrc());
		this.dataSRC = new SimpleStringProperty("DATA");
		this.partecipante = partecipante;
	}

	/**
	 * @return il numero di tessera sanitaria del partecipante
	 */
	public String getnumTesseraSan() {
		return numTesseraSan.get();
	}

	/**
	 * @return il percorso del file del certificato SRC
	 */
	public String getCertificatoSRC() {
		return certificatoSRC.get();
	}

	/**
	 * @return la data di validit� del certificato src
	 */
	public String getDataSRC() {
		return dataSRC.get();
	}
	
	/**
	 * @return il partecipante corrente
	 */
	public PartecipanteTO getPartecipante(){
		return this.partecipante;
	}
}
