package app.presentation.views.tableModels;

import java.text.SimpleDateFormat;

import app.to.ManagerDiEscursioneTO;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Classe che implementa il modello per la rappresentazione
 * dei dati nella tabella di una View.
 * In questa classe � presentato il Manager di Escursione. 
 * La classe estende il modello dell'Utente base perch�
 * possiede dei campi in comune.
 * 
 * @author michele fornelli
 */

public class TableModelMDE extends TableModelUtente{

	private SimpleDoubleProperty stipendio;
	private ManagerDiEscursioneTO mde;
	
	public TableModelMDE(){}
	
	public TableModelMDE(ManagerDiEscursioneTO mde) {
		this.id = new SimpleIntegerProperty(mde.getIdUtente());
		this.nome = new SimpleStringProperty(mde.getNome());
		this.cognome = new SimpleStringProperty(mde.getCognome());
		this.email = new SimpleStringProperty(mde.getEmail());
		this.username = new SimpleStringProperty(mde.getUsername());
		this.password = new SimpleStringProperty(mde.getPassword());
		this.codiceFiscale = new SimpleStringProperty(mde.getCf());
		this.sesso = new SimpleStringProperty(mde.getSesso());
		this.indirizzo = new SimpleStringProperty(mde.getIndirizzo());
		this.citta = new SimpleStringProperty(mde.getCitta());
		this.stipendio = new SimpleDoubleProperty(mde.getStipendio());
		this.mde = mde;
		
		 String DATE_FORMAT_NOW = "yyyy-MM-dd";
		 SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		 String stringDate = sdf.format(mde.getDataNasc() );
		this.dataNascita = new SimpleStringProperty(stringDate);
	}

	/**
	 * @return stipendio Manager di Escursione
	 */
	public Double getStipendio() {
		return stipendio.get();
	}
	
	/**
	 * @return manager di Escursione
	 */
	public ManagerDiEscursioneTO getManagerDiEscursione(){
		return this.mde;
	}
}
