package app.presentation.views.tableModels;

import app.to.OptionalTO;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Classe che implementa il modello che servir� per la rappresentazione
 * dei dati nella tabella di una View. In questa classe sono presentati
 * gli Optional
 * 
 * @author michele fornelli
 * 
 */

public class TableModelOptional {

	private SimpleIntegerProperty idOptional;
	private SimpleStringProperty nome;
	private SimpleStringProperty descrizione;
	private SimpleStringProperty nomeTipoOptional;
	private SimpleStringProperty prezzo;
	private OptionalTO optional;
	
	public TableModelOptional(OptionalTO optional) {
		idOptional = new SimpleIntegerProperty(optional.getIdOptional());
		nome = new SimpleStringProperty(optional.getNome());
		descrizione = new SimpleStringProperty(optional.getDescrizione());
		nomeTipoOptional = new SimpleStringProperty(optional.getTipoOptional().getNome());
		prezzo = new SimpleStringProperty(Double.toString(optional.getTipoOptional().getPrezzo()));
		this.optional = optional;
	}

	/**
	 * @return l'id dell'optional
	 */
	public Integer getIdOptional() {
		return idOptional.get();
	}

	/**
	 * @return il nome dell'optional
	 */
	public String getNome() {
		return nome.get();
	}

	/**
	 * @return la descrizione dell'optional
	 */
	public String getDescrizione() {
		return descrizione.get();
	}

	/**
	 * @return il nome del tipo dell'optional
	 */
	public String getNomeTipoOptional() {
		return nomeTipoOptional.get();
	}

	/**
	 * @return il costo dell'optional
	 */
	public String getPrezzo() {
		return prezzo.get();
	}

	/**
	 * @return l'optional
	 */
	public OptionalTO getOptional() {
		return optional;
	}

	
}
