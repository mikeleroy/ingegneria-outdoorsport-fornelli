package app.presentation.views.tableModels;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Classe che implementa il modello per la rappresentazione
 * dei dati nella tabella di una View. 
 * In questa classe � presentato l'Utente base. 
 * La classe � astratta perch� l'Utente loggato non potr� mai essere
 * un Utente base, ma almeno un Manager di Sistema, oppure un Manager di Escursione,
 * oppure un Partecipante
 * 
 * @author michele fornelli
 */

abstract class TableModelUtente {

	protected SimpleIntegerProperty id;
	protected SimpleStringProperty nome;
	protected SimpleStringProperty cognome;
	protected SimpleStringProperty username;
	protected SimpleStringProperty email;
	protected SimpleStringProperty password;
	protected SimpleStringProperty sesso;
	protected SimpleStringProperty codiceFiscale;
	protected SimpleStringProperty citta;
	protected SimpleStringProperty indirizzo;
	protected SimpleStringProperty dataNascita;
	
	/**
	 * @return id dell'Utente
	 */
	public Integer getId() {
		return id.get();
	}
	
	/**
	 * @return nome dell'Utente
	 */
	public String getNome() {
		return nome.get();
	}
	
	/**
	 * @return cognome dell'Utente
	 */
	public String getCognome() {
		return cognome.get();
	}
	
	/**
	 * @return email dell'Utente
	 */
	public String getEmail() {
		return email.get();
	}
	
	/**
	 * @return username dell'Utente
	 */
	public String getUsername() {
		return username.get();
	}
	
	/**
	 * @return codice fiscale dell'Utente
	 */
	public String getCodiceFiscale() {
		return codiceFiscale.get();
	}
	
	/**
	 * @return data nascita dell'Utente
	 */
	public String getDataNascita() {
		return dataNascita.get();
	}
	
	/**
	 * @return sesso dell'Utente
	 */
	public String getSesso() {
		return sesso.get();
	}
	
	/**
	 * @return citt� dell'Utente
	 */
	public String getCitta() {
		return citta.get();
	}
	
	/**
	 * @return indirizzo dell'Utente
	 */
	public String getIndirizzo() {
		return indirizzo.get();
	}
	

	
}
