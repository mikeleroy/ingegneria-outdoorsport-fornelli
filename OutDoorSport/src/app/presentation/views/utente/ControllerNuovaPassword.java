package app.presentation.views.utente;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.ManagerDiSistemaTO;
import app.to.PartecipanteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * Gestisce il recupero della password
 * 
 * @author michele fornelli
 *
 */

public class ControllerNuovaPassword extends BaseController{

	@FXML private Button btnRichiediNuovaPassword;
	@FXML private TextField txtShowPassword;
	@FXML private PasswordField txtPassword;
	@FXML private Button btnCancel;
	@FXML private CheckBox checkShowPassword;
	@FXML private AnchorPane anchorNuovaPassword;
	
	private UtenteTO user = null;
	
	/**
	 * Costruttore della classe ControllerLostPassword
	 */
	public ControllerNuovaPassword() {
		if(user == null){
			user = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente); 
		}
		
	}
	
	
	@FXML protected void txtKeyPressed(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER){
//			lblMessage.setVisible(true);
//			lblMessage.setText("Premere il tasto RECUPERA");
//			lblMessage.setTextFill(Color.BLUE);
////			txtEmail.setText("");
//			txtPassword.setText("");
//		}
		
	}
	
	
	
	/**
	 * Metodo che inizializza tutti i campi della finestra
	 */
	@Override
	protected void initialize() {
		
		anchorNuovaPassword.sceneProperty().addListener((obs, oldScene, newScene) -> {
		    if (newScene == null) {
		    	//TODO
			} else {
				if(SessionData.getSessionData("Partecipante") != null)
				{
					user = (PartecipanteTO) SessionData.getSessionData("Partecipante");				
				}
				else if(SessionData.getSessionData("ManagerDiEscursione") !=null)
				{
					user = (ManagerDiEscursioneTO) SessionData.getSessionData("ManagerDiEscursione");				
				}
				else{
					user = (ManagerDiSistemaTO) SessionData.getSessionData("ManagerDiSistema");				
				}
				 
		    }
		});
		
	
		
		
		checkShowPassword.selectedProperty().addListener(new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> ov, Boolean oldVal, Boolean newVal) {
				if(newVal){
					txtPassword.setVisible(false);
					txtShowPassword.setVisible(true);
					txtShowPassword.setText(txtPassword.getText());
				} else {
					txtPassword.setVisible(true);
					txtPassword.setText(txtShowPassword.getText());
					txtShowPassword.setVisible(false);
				}
			}
		});
	}
	
	/**
	 * Evento associato alla richiesta di una nuova password. Verr� inviata all'utente
	 * una nuova password all'indirizzo email a cui si � registrato inizalmente.
	 */
	@FXML protected void btnNuovaPassword(){
		nuovaPassword();
	}

	/**
	 * Metodo di supporto a execRichiediNuovaPassword(). Viene catturata l'email e viene
	 * effettuata la richiesta di invio di una nuova password.
	 */
	private void nuovaPassword() {
		user.setPassword(txtShowPassword.getText());
		OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_NUOVA_PASSWORD,user));
		if(response.toString().equals(SUCCESS)){
			Alert alert = new Alert(AlertType.INFORMATION, "La nuova password � stata salvata e inviata al tuo indirizzo email!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
			if(alert.getResult() == ButtonType.OK)
				resetField();
				this.eseguiRichiesta(new OutdoorRequest(VIEW_LOGIN));
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Errore!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}		
	}
	
	/**
	 * Evento associato al al tasto indietro per tornare alla schermata di login
	 */
	@FXML protected void btnCancel(){
		resetField();
		eseguiRichiesta(new OutdoorRequest(VIEW_LOGIN));
	}
	
	
	private void resetField()
	{
		 txtPassword.setText("");
		 txtShowPassword.setText("");
	}
	
}
