package app.presentation.views.utente;

import java.util.Optional;

import app.integration.dao.PartecipanteDAO;
import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.ManagerDiSistemaTO;
import app.to.PartecipanteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Actions;
import app.utility.Rules;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class ControllerModificaAccesso extends BaseController implements Rules,Actions{

	
	private OutdoorResponse risposta;
	@FXML private TextField txtEmail;
	@FXML private TextField txtUsername;
	@FXML private CheckBox chekcShowPassword;
	@FXML private TextField txtShowPassword;
	@FXML private PasswordField txtPassword;
	@FXML private Label lblErrore;	
	@FXML private StackPane stackModificaAccesso;

	
	private PartecipanteTO partecipante = null;
	private ManagerDiEscursioneTO managerEscursione = null;
	private ManagerDiSistemaTO managerSistema = null;

	private UtenteTO user = null;
	
	/**
	 * Costruttore che avvalora il partecipante in memoria
	 */
	public ControllerModificaAccesso() {
		if(user == null){
			user = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente);
		}
	}
	
	
	
	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					if(SessionData.getSessionData("Partecipante") != null)
					{
						user = (PartecipanteTO) SessionData.getSessionData("Partecipante");				
					}
					else if(SessionData.getSessionData("ManagerDiEscursione") !=null)
					{
						user = (ManagerDiEscursioneTO) SessionData.getSessionData("ManagerDiEscursione");				
					}
					else{
						user = (ManagerDiSistemaTO) SessionData.getSessionData("ManagerDiSistema");				
					}
					
					txtUsername.setText(user.getUsername());
					txtEmail.setText(user.getEmail());
					txtShowPassword.setPromptText("inserire una nuova password");
					txtPassword.setVisible(false);

				}
			}
		};
		
		stackModificaAccesso.visibleProperty().addListener(visibilityListener);
		


	}
	
	
	
	
	@FXML protected void btnSave(MouseEvent event) {	
		 modificaDati();
	}
	
	
	
	
	private void modificaDati() {
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("OutDoorSports 1.0");
		alert.setHeaderText("Modifica dati di accesso");
		alert.setContentText("Sei sicuro di apportare queste modifiche?");
		Optional<ButtonType> res = alert.showAndWait();
		
		if(res.get() == ButtonType.OK)
		{
		
		user.setEmail(txtEmail.getText());
		user.setUsername(txtUsername.getText());
		user.setPassword(txtShowPassword.getText());
		 
		

		
			String result = checkErrors(user);
			if(result.equals(""))
			{
				OutdoorResponse risposta = this.eseguiRichiesta(new OutdoorRequest(ACTION_MODIFICA_ACCESSO_UTENTE,user ));

				if(risposta.getResponse().equals(SUCCESS)){
						tornaIndietro();
				}
				else{
					lblErrore.setText("Errore nella modifica dell'utente.");
				}
			}else{
				lblErrore.setText(result);
			}
		}
	}

	
	


	private void tornaIndietro() {
		
		txtShowPassword.setText("");
		txtPassword.setText("");
		txtEmail.setText("");
		txtUsername.setText("");
		
		if(SessionData.getSessionData("Partecipante") != null)
		{
			 this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_PAR,ViewsCache.getNestedAnchorPane()));
		}
		else if(SessionData.getSessionData("ManagerDiEscursione") !=null)
		{
			 this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_MANAGER_ESC,ViewsCache.getNestedAnchorPane()));
		}
		else{
			 this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_MANAGER_SIS,ViewsCache.getNestedAnchorPane()));
		}		
	}



	@FXML protected void btnCancel(MouseEvent event) {	
	
		tornaIndietro();
	}
	

}





