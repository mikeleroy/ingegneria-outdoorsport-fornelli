package app.presentation.views.utente;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

/**
 * Gestisce il recupero della password
 * 
 * @author michele fornelli
 *
 */

public class ControllerRecuperaPassword extends BaseController{

	@FXML private Button btnRichiediNuovaPassword;
	@FXML private TextField txtEmail;
	@FXML private Button btnCancel;
	
	private UtenteTO utente = null;
	
	/**
	 * Costruttore della classe ControllerLostPassword
	 */
	public ControllerRecuperaPassword() {
		if(utente == null){
			utente = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente); 
		}
	}
	
	
	@FXML protected void txtKeyPressed(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER){
//			lblMessage.setVisible(true);
//			lblMessage.setText("Premere il tasto RECUPERA");
//			lblMessage.setTextFill(Color.BLUE);
////			txtEmail.setText("");
//			txtPassword.setText("");
//		}
		
	}
	
	
	
	/**
	 * Metodo che inizializza tutti i campi della finestra
	 */
	@Override
	protected void initialize() {}
	
	/**
	 * Evento associato alla richiesta di una nuova password. Verr� inviata all'utente
	 * una nuova password all'indirizzo email a cui si � registrato inizalmente.
	 */
	@FXML protected void btnRecuperaPassword(){
		recuperaPassword();
	}

	/**
	 * Metodo di supporto a execRichiediNuovaPassword(). Viene catturata l'email e viene
	 * effettuata la richiesta di invio di una nuova password.
	 */
	private void recuperaPassword() {
		utente.setEmail(txtEmail.getText());
		OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_RECUPERA_PASSWORD,utente));
		if(response.toString().equals(SUCCESS)){
				utente = (UtenteTO)response.getData();
				resetField();
				this.eseguiRichiesta(new OutdoorRequest(VIEW_NUOVA_PASSWORD,utente));
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Errore! L'email inserita non esiste!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}		
	}
	
	/**
	 * Evento associato al al tasto indietro per tornare alla schermata di login
	 */
	@FXML protected void btnCancel(){
		resetField();
		eseguiRichiesta(new OutdoorRequest(VIEW_LOGIN));
	}
	
	
	private void resetField()
	{
		 txtEmail.setText("");
	}
	
}
