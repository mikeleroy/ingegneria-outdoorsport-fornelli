package app.presentation.views.utente;


import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.integration.dao.StatoUtenteDAO;
import app.integration.dao.UtenteDAO;
import app.integration.daoUtil.DatabaseException;
import app.presentation.fc.FrontController;
import app.presentation.fc.FrontController;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.PartecipanteTO;
import app.to.TipoUtenteTO;
import app.to.StatoUtenteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Rules;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ControllerLogin extends BaseController implements Rules{

	@FXML private Button btnLogin;
	@FXML private Button btnRegistrati;
	@FXML private AnchorPane OutDoorSportPane;
	@FXML private TextField txtEmail;
	@FXML private PasswordField txtPassword;
	@FXML private Label lblTitolo;
	@FXML private Label lblPassLost;
	@FXML private Label lblMessage;
	@FXML private Label btnPswDimenticata;
	
	private String user;
	private String pass;
	private String checkUser, checkPass;
	
	private OutdoorRequest richiesta;
	private OutdoorResponse risposta;

	public ControllerLogin() {

	}
	
	@FXML protected void txtKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER){
			lblMessage.setVisible(true);
			lblMessage.setText("Premere il tasto LOGIN");
			lblMessage.setTextFill(Color.BLUE);
//			txtEmail.setText("");
			txtPassword.setText("");
		}
		
	}

	
	@FXML protected void btnLogin(MouseEvent event) throws DatabaseException {	
		//eseguiLogin();
		

		
		UtenteTO user = (UtenteTO) FactoryTO.getFactoryTO(EnumTO.Utente);
		user.setEmail(txtEmail.getText());
		user.setPassword(txtPassword.getText());
		 risposta = this.eseguiRichiesta(new OutdoorRequest(ACTION_LOGIN,user));
		 
		 if(risposta.getResponse().equals(SUCCESS)){
			 this.eseguiRichiesta(new OutdoorRequest(risposta.getView(),risposta.getData()));
			 resetField();
		 }
		 else{
			 Alert alert = new Alert(AlertType.ERROR, "Errore! Email e/o password errata!", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");
				alert.showAndWait();
		 }
	}
	

		

	@FXML protected void btnRegistrati(MouseEvent event) {	
		risposta = this.eseguiRichiesta(new OutdoorRequest(VIEW_REGISTRA_UTENTE));
		resetField();
	}
	

	@FXML protected void recuperaPassword(MouseEvent event) {	 
		risposta = this.eseguiRichiesta(new OutdoorRequest(VIEW_RECUPERA_PASSWORD));
		resetField();
	}
	
	
	
	
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	private void resetField()
	{
		 txtEmail.setText("");
		 txtPassword.setText("");
	}
	
}