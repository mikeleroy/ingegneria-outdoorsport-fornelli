package app.presentation.views;

import java.lang.reflect.Field;

import app.to.EscursioneTO;

/**
 * 
 * Classe astratta che incorpora tutti i metodi che servono 
 * nella fase di creazione di una escursione
 * 
 * @author michele fornelli
 *
 */

public abstract class BaseEscursioneController extends BaseController{


	/**
	 * Funzione che restituisce la stringa degli errori rispetto alle informazioni inserite in maniera non corretta 
	 * per inserire una nuova escursione
	 * 
	 * @param escursione: escursione da controllare
	 * @return result: stringa errori
	 */
	protected String checkErrorsEscursione(EscursioneTO escursione){
		String result = "";

		controlloDataEscursione(escursione);

		int i = 0;
		
		for (Field f : escursione.getClass().getDeclaredFields()) {
			f.setAccessible(true);
			try {
				if (f.get(escursione) == null || f.get(escursione).equals("")) {
					if(!(f.getName().equals("idEscursione") || 
						 f.getName().equals("optionals") ||
						 f.getName().equals("utente") ||
						 f.getName().equals("statoEscursione"))){
					result += f.getName() + ", ";
					i++;
					if(i == 2){
						result += "\n";
						i = 0;
					}
						}
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		if(!result.equals(""))
			result += " non corretti!" + "\n";

		if((escursione.getNumMin() == 0) || (escursione.getNumMax() == 0))
			result += "Zero non � un numero valido";
		
		if(escursione.getNumMax() < escursione.getNumMin())
			result += "Il numero massimo dei partecipanti non pu� essere minore del numero minimo" + "\n";
			
		return result;
	}
	
	
	
	
	/**
	 * Metodo che permette di controllare il datepicker delle schermate
	 * di inserimento escursione. Il metodo verr� poi riscritto ed utilizzato
	 * nel controller specifico per il controllo della data.
	 * @param escursione: Data escursione in fase di creazione di una nuova escursione
	 */
	protected abstract void controlloDataEscursione(EscursioneTO escursione);

	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		
	}
	
	
}
