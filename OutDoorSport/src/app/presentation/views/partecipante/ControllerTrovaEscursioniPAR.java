package app.presentation.views.partecipante;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.presentation.views.TableViewController;
import app.presentation.views.tableModels.TableModelEscursione;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.PartecipanteTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

/**
 * Classe controller che gestisce e visualizza le escursione aperte al partecipante
 * 
 * @author michele fornelli
 *
 */
public class ControllerTrovaEscursioniPAR extends TableViewController{

	@FXML private StackPane stackEscursioniAperte;
	@FXML private TextField txtRicerca;
	@FXML private Button btnRicerca;
	@FXML private TableView<TableModelEscursione> tblEscursioniAperte;
	@FXML private TableColumn<TableModelEscursione, String> columnNomeEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnTipoEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnDataEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnNumMinEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnNumMaxEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnPrezzoEscursione;
	
	private List<EscursioneTO> listaEscursioni = new ArrayList<>();
	private TableModelEscursione escursione_model = null;
	private EscursioneTO escursione = null;
	private IscrizioneTO iscrizione = null;
	
	/**
	 * Costruttore
	 */
	public ControllerTrovaEscursioniPAR() {
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
		if(iscrizione == null){
			iscrizione = (IscrizioneTO)FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
	}

	
	/**
	 * Metodo di inizializzazione della schermata a video
	 */
	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					allEscursioniAperte();
				}
			}
		};
		stackEscursioniAperte.visibleProperty().addListener(visibilityListener);
	}
	
	
	/**
	 * Metodo di supporto che fornisce tutte le escursioni a cui l'utente si pu� iscrivere
	 */
	@SuppressWarnings("unchecked")
	private void allEscursioniAperte(){
		
		OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_ESCURSIONI_APERTE, escursione));		
		listaEscursioni = (List<EscursioneTO>) response.getData();

		ObservableList<TableModelEscursione> data = FXCollections.observableArrayList(getListTabellaEscursioni(listaEscursioni));

		this.initColumn(columnNomeEscursione, "nome");
		this.initColumn(columnTipoEscursione, "nomeTipoEscursione");
		this.initColumn(columnDataEscursione, "data");
		this.initColumn(columnNumMinEscursione, "numMin");
		this.initColumn(columnNumMaxEscursione, "numMax");
		this.initColumn(columnPrezzoEscursione, "prezzo");

		tblEscursioniAperte.setItems(data);

		tblEscursioniAperte.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				TableView<TableModelEscursione> table_escursioni = (TableView<TableModelEscursione>) event.getSource();
				escursione_model = table_escursioni.getSelectionModel().getSelectedItem();
				if(escursione_model != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
						if(escursione_model.getEscursione().getTotIscritti() < escursione_model.getEscursione().getNumMax()){
							iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
							iscrizione.setEscursione(escursione_model.getEscursione());
							iscrizione.setUtente((PartecipanteTO) SessionData.getSessionData("Partecipante"));

							OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_CHECK_ISCRIZIONE,iscrizione));
							if(response.toString().equals(SUCCESS))
								eseguiRichiesta(new OutdoorRequest(VIEW_ISCRIZIONE_ESCURSIONE, ViewsCache.getNestedAnchorPane(),iscrizione));
							else{
								Alert alert1 = new Alert(AlertType.ERROR, "Sei gi� iscritto a questa escursione", ButtonType.OK);
								alert1.setTitle("OutDoorSport1.0");
								alert1.showAndWait();
							}
						}else{
							Alert alert1 = new Alert(AlertType.ERROR, "Numero massimo di partecipanti raggiunto", ButtonType.OK);
							alert1.setTitle("OutDoorSport1.0");
							alert1.showAndWait();
						}
					}
				}
			}
		});
	}
	
	/**
	 * Metodo associato all'evento del click del bottone Iscriviti ad una Escursione
	 */
	@FXML
	protected void iscrizioneEscursione(){
		escursione_model = tblEscursioniAperte.getSelectionModel().getSelectedItem();
		if(escursione_model != null){
			iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
			iscrizione.setEscursione(escursione_model.getEscursione());
			iscrizione.setUtente((PartecipanteTO) SessionData.getSessionData("Partecipante"));
			
			OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_CHECK_ISCRIZIONE,iscrizione));
			if(response.toString().equals(SUCCESS))
				eseguiRichiesta(new OutdoorRequest(VIEW_ISCRIZIONE_ESCURSIONE, ViewsCache.getNestedAnchorPane(),iscrizione));
			else{
				Alert alert = new Alert(AlertType.ERROR, "Sei gi� iscritto a questa escursione", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");
				alert.showAndWait();
			}
		}
		else{
			Alert alert = new Alert(AlertType.ERROR, "Nessuna Escursione Selezionata", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}	
	}
	
	/**
	 * Metodo associato all'evento del click del bottone Cerca Escursioni.
	 */
	@FXML 
	protected void ricercaEscursione(){
		String param = txtRicerca.getText();
		List<EscursioneTO> listaEscursioni = new ArrayList<>();
		
		
		for(EscursioneTO escursione : this.listaEscursioni){
			if(escursione.getNome().contains(param) ||
			   escursione.getData().contains(param) ||
			   escursione.getDescrizione().contains(param) ||
			   escursione.getTipoEscursione().getNome().contains(param.toUpperCase()))
				listaEscursioni.add(escursione);
			else
				try{
					if(escursione.getNumMax() == Integer.parseInt(param) || 
						escursione.getNumMin() == Integer.parseInt(param) ||
						escursione.getPrezzo() == Double.parseDouble(param))
						listaEscursioni.add(escursione);
				}catch(Exception e){
				}
		}
		
		ObservableList<TableModelEscursione> data = FXCollections.observableArrayList(getListTabellaEscursioni(listaEscursioni));

		tblEscursioniAperte.setItems(data);
	}
	
	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati nella tabella
	 * 
	 * @param lista delle escursione
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelEscursione> getListTabellaEscursioni(List<EscursioneTO> listaEscursioni){
		ObservableList<TableModelEscursione> res = FXCollections.observableArrayList();

		for(EscursioneTO escursione : listaEscursioni){
			escursione_model = new TableModelEscursione(escursione);
			res.add(escursione_model);
		}
		return res;
	}
}
