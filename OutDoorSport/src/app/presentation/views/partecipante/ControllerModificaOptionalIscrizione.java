package app.presentation.views.partecipante;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.TableViewController;
import app.presentation.views.tableModels.TableModelOptional;
import app.to.EnumTO;
import app.to.IscrizioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalTO;
import app.to.StatoOptionalTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class ControllerModificaOptionalIscrizione  extends TableViewController{

	@FXML private StackPane stackModificaOptional;
	@FXML private TableView<TableModelOptional> tblOptionalScelti;
	@FXML private TableView<TableModelOptional> tblOptionalDisponibili;
	@FXML private TableColumn<TableModelOptional, String> columnOptionalScelti;
	@FXML private TableColumn<TableModelOptional, String> columnPrezzoScelti;
	@FXML private TableColumn<TableModelOptional, String> columnTipoOptionalScelti;
	@FXML private TableColumn<TableModelOptional, String> columnOptionalDisponibili;
	@FXML private TableColumn<TableModelOptional, String> columnPrezzoDisponibili;
	@FXML private TableColumn<TableModelOptional, String> columnTipoOptionalDisponibili;
	@FXML private Button btnInserisciOptional;
	@FXML private Button btnRimuoviOptional;
	@FXML private Label lblPrezzoTotaleOptional;
	@FXML private Label lblPrezzoTotale;
	@FXML private Button btnIndietro;
	@FXML private Button btnConferma;
	private TableModelOptional optional_disponibili_model = null;
	private TableModelOptional optional_scelti_model = null;
	private OptionalEscursioneTO optional_escursione;
	private OptionalTO optional = null;
	private IscrizioneTO iscrizione = null;
	private IscrizioneTO iscrizioneOld = null;
	private Set<OptionalEscursioneTO> all_optional_scelti = null;
	private Set<OptionalEscursioneTO> all_optional_disponibili = null;
	private Set<OptionalEscursioneTO> all_current_optional_disponibili = null;
	private StatoOptionalTO stato_optional = null;
	@SuppressWarnings("unused")
	private List<StatoOptionalTO> list_stato_optional = new ArrayList<>();


	public ControllerModificaOptionalIscrizione() {
		if(optional == null){
			optional = (OptionalTO) FactoryTO.getFactoryTO(EnumTO.Optional);
		}
		if(stato_optional == null){
			stato_optional = (StatoOptionalTO)  FactoryTO.getFactoryTO(EnumTO.StatoOptional);
		}
		if(iscrizione == null){
			iscrizione = (IscrizioneTO)  FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
		if(iscrizioneOld == null){
			iscrizioneOld = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
		if(optional_escursione == null){
			optional_escursione = (OptionalEscursioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);
		}
	}

	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					iscrizione = (IscrizioneTO) SessionData.getSessionData(iscrizione.getClass().getSimpleName());
					iscrizioneOld = (IscrizioneTO) SessionData.getSessionData(iscrizione.getClass().getSimpleName());
					OutdoorResponse response = eseguiRichiesta(new OutdoorRequest( ACTION_GET_ALL_STATO_OPTIONAL,stato_optional));
					list_stato_optional = (List<StatoOptionalTO>) response.getData();
					lblPrezzoTotale.setText("0 �");
					lblPrezzoTotaleOptional.setText("0 �");

					optional_escursione.setEscursione(iscrizione.getEscursione());
					OutdoorResponse resp = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_OPTIONAL_ESCURSIONE,optional_escursione));
					Set<OptionalEscursioneTO> temp = new HashSet<>();
					temp.addAll((Collection<? extends OptionalEscursioneTO>) resp.getData());
					all_optional_disponibili = new HashSet<>();
					all_optional_disponibili.addAll(temp);
					all_optional_scelti = new HashSet<>();
					if(iscrizione.getOptionals() != null)
						all_optional_scelti.addAll(iscrizione.getOptionals());
					all_current_optional_disponibili = new HashSet<>();
					all_current_optional_disponibili.addAll(all_optional_disponibili);

					for(OptionalEscursioneTO optional : all_optional_disponibili){
						for(OptionalEscursioneTO oe : all_optional_scelti){
							if(optional.getOptional().getNome().equals(oe.getOptional().getNome())){
								all_current_optional_disponibili.remove(optional);
							}
						}
					}
					setTables();
				}
			}
		};

		stackModificaOptional.visibleProperty().addListener(visibilityListener);
	}

	/**
	 * Metodo che carica gli optional disponibili per quella escursione nella tabella in alto, 
	 * mentre carica gli optional scelti nella tabella in basso, in base agli optional
	 * disponibili
	 */
	@SuppressWarnings("unchecked")
	private void setTables(){

		ObservableList<TableModelOptional> dataDisponibili = FXCollections.observableArrayList(getListTabellaOptionalDisponibili(all_current_optional_disponibili));

		this.initColumn(columnOptionalDisponibili, "nome");
		this.initColumn(columnPrezzoDisponibili, "prezzo");
		this.initColumn(columnTipoOptionalDisponibili, "nomeTipoOptional");

		tblOptionalDisponibili.setItems(dataDisponibili);

		tblOptionalDisponibili.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				tblOptionalDisponibili = (TableView<TableModelOptional>) event.getSource();
				optional_disponibili_model = tblOptionalDisponibili.getSelectionModel().getSelectedItem();
				if(optional_disponibili_model != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
						inserisciOptional();
					}
				}
			}
		});

		ObservableList<TableModelOptional> dataScelti = FXCollections.observableArrayList(getListTabellaOptionalScelti(all_optional_scelti));

		this.initColumn(columnOptionalScelti, "nome");
		this.initColumn(columnPrezzoScelti, "prezzo");
		this.initColumn(columnTipoOptionalScelti, "nomeTipoOptional");

		tblOptionalScelti.setItems(dataScelti);

		tblOptionalScelti.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				tblOptionalScelti = (TableView<TableModelOptional>) event.getSource();
				optional_scelti_model = tblOptionalScelti.getSelectionModel().getSelectedItem();
				if(optional_scelti_model != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
						rimuoviOptional();
					}
				}
			}
		});

		double prezzoTotale = iscrizione.getEscursione().getPrezzo();
		double prezzoTotaleOptional = 0;
		for(OptionalEscursioneTO oe : all_optional_scelti){
			if(oe.getStatoOptional().getIdStatoOptional() == 2){
				prezzoTotale = prezzoTotale + oe.getOptional().getTipoOptional().getPrezzo();
				prezzoTotaleOptional = prezzoTotaleOptional + oe.getOptional().getTipoOptional().getPrezzo();
			}
		}
		lblPrezzoTotale.setText(Double.toString(prezzoTotale) + " �");
		lblPrezzoTotaleOptional.setText(Double.toString(prezzoTotaleOptional) + " �");
	}

	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati
	 * nella tabella degli optional disponibili
	 * 
	 * @param lista Optional
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelOptional> getListTabellaOptionalDisponibili(Set<OptionalEscursioneTO> param){
		ObservableList<TableModelOptional> res = FXCollections.observableArrayList();

		for(OptionalEscursioneTO optional : param){
			optional_scelti_model = new TableModelOptional(optional.getOptional());
			if(optional.getStatoOptional().getIdStatoOptional() == 2)
				res.add(optional_scelti_model);
		}

		return res;
	}

	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati
	 * nella tabella degli optional scelti dal partecipante iscritto
	 * 
	 * @param lista Optional
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelOptional> getListTabellaOptionalScelti(Set<OptionalEscursioneTO> param){
		ObservableList<TableModelOptional> res = FXCollections.observableArrayList();

		for(OptionalEscursioneTO optional : param){
			optional_scelti_model = new TableModelOptional(optional.getOptional());
			if(optional.getStatoOptional().getIdStatoOptional() == 2)
				res.add(optional_scelti_model);
		}

		return res;
	}

	/**
	 * Evento associato all'inserimento di un optional nella lista degli optional
	 * scelti tra quelli disponibili per quella escursione
	 */
	@FXML protected void inserisciOptional(){
		optional_disponibili_model = tblOptionalDisponibili.getSelectionModel().getSelectedItem();
		if(optional_disponibili_model != null){
			for(OptionalEscursioneTO o : all_current_optional_disponibili){
				if(o.getOptional().getNome().equals(optional_disponibili_model.getNome())){
					optional_escursione = (OptionalEscursioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);
					optional_escursione = o;
					break;
				}
			}
			all_current_optional_disponibili.remove(optional_escursione);
			all_optional_scelti.add(optional_escursione);
			setTables();
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Nessun Optional Selezionato", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}

	/**
	 * Evento associato alla rimozione di un optional dalla lista degli optional
	 * scelti tra quelli disponibili per quella escursione
	 */
	@FXML protected void rimuoviOptional(){
		optional_scelti_model = tblOptionalScelti.getSelectionModel().getSelectedItem();
		if(optional_scelti_model != null){
			for(OptionalEscursioneTO oe : all_optional_scelti){
				if(oe.getOptional().getNome().equals(optional_scelti_model.getNome())){
					optional_escursione = (OptionalEscursioneTO)FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);
					optional_escursione = oe;
					break;
				}
			}
			all_optional_scelti.remove(optional_escursione);
			all_current_optional_disponibili.add(optional_escursione);
			setTables();
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Nessun Optional Selezionato", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}

	/**
	 * Evento associato alla conferma gli optional scelti. 
	 * Torna alla view precedente
	 */
	@FXML protected void btnConferma(){
		iscrizione.setOptionals(all_optional_scelti);
		eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE, ViewsCache.getNestedAnchorPane(),iscrizione));
	}

	/**
	 * Evento associato all'annullamento delle
	 * modifiche. Torna alla view precedente
	 */
	@FXML protected void btnCancel(){
		Alert alert = new Alert(AlertType.CONFIRMATION, "Sei sicuro di voler annullare le modifiche?");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE , ViewsCache.getNestedAnchorPane(),iscrizioneOld));
		} else {
			alert.close();
		}
	}
}
