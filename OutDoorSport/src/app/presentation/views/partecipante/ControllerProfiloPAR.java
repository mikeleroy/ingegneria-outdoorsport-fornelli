package app.presentation.views.partecipante;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.PartecipanteTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;


public class ControllerProfiloPAR extends BaseController{
	
	@FXML private StackPane stackProfiloPAR;
	@FXML private Label lblNome;
	@FXML private Label lblUsername;
	@FXML private Label lblEmail;
	@FXML private Label lblCitta;
	@FXML private Label lblIndirizzo;
	@FXML private Label lblCF;
	@FXML private Label lblDataN;
	@FXML ControllerDashboardPAR dashboardPAR;
	@FXML AnchorPane pane;
	
	private PartecipanteTO partecipante = null;
	
	/**
	 * Costruttore che avvalora il partecipante in memoria
	 */
	public ControllerProfiloPAR() {
		if(partecipante == null){
			partecipante = (PartecipanteTO) FactoryTO.getFactoryTO(EnumTO.Partecipante);

		}
	}
	
	@Override
	protected void initialize() {
		
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					
					partecipante = (PartecipanteTO) SessionData.getSessionData(partecipante.getClass().getSimpleName());
					lblNome.setText(partecipante.getNome() + " " + partecipante.getCognome());
					lblUsername.setText(partecipante.getUsername());
					lblEmail.setText(partecipante.getEmail());
				//	lblDataN.setText(partecipante.getDataNasc());
					lblCF.setText(partecipante.getCf());
					lblIndirizzo.setText(partecipante.getIndirizzo());
					lblCitta.setText(partecipante.getCitta());
					
				}
			}
		};
		
		stackProfiloPAR.visibleProperty().addListener(visibilityListener);
		
		
	}
	
	
	
	@FXML protected void btnModificaPartecipante(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_PAR,ViewsCache.getNestedAnchorPane()));
	}
	
	@FXML protected void btnModificaAccesso(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ACCESSO,ViewsCache.getNestedAnchorPane()));
	}
	
	@FXML protected void btnModificaSRC(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_SRC,ViewsCache.getNestedAnchorPane()));
	}
	

}
