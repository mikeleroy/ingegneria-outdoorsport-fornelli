package app.presentation.views.partecipante;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.TableViewController;
import app.presentation.views.tableModels.TableModelEscursione;
import app.to.EmailTO;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.ManagerDiEscursioneTO;
import app.to.PartecipanteTO;
import app.to.StatoIscrizioneTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.EmailSettings;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

/**
 * Classe controller che gestisce la visualizzazione delle escursioni
 * a cui il partecipante � iscritto.
 * 
 * @author michele fornelli
 *
 */

public class ControllerEscursioniPAR extends TableViewController{

	@FXML private StackPane stackEscursioniPAR;
	@FXML private TextField txtRicerca;
	@FXML private Button btnRicerca;
	@FXML private TableView<TableModelEscursione> tblEscursioniPAR;
	@FXML private TableColumn<TableModelEscursione, String> columnNomeEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnTipoEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnDataEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnNumMinEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnNumMaxEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnPrezzoEscursione;
	@FXML private Button btnCancelEscursione, btnModificaIscrizione;

	private List<EscursioneTO> listaEscursioni = new ArrayList<>();
	private TableModelEscursione escursione_model = null;
	private EscursioneTO escursione = null;
	private IscrizioneTO iscrizione = null;
	private List<StatoIscrizioneTO> list_stato_iscrizione = new ArrayList<>();

	public ControllerEscursioniPAR() {
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
		if(iscrizione == null){
			iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
	}

	/**
	 * Metodo di inizializzazione dell'interfaccia
	 */
	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(newValue){
					allEscursioniPAR();
				}
			}
		};

		stackEscursioniPAR.visibleProperty().addListener(visibilityListener);
	}

	/**
	 * Metodo di supporto che fornisce tutte le escursioni a cui l'utente � iscritto
	 */
	@SuppressWarnings("unchecked")
	private void allEscursioniPAR(){
		PartecipanteTO partecipante = (PartecipanteTO) SessionData.getSessionData("Partecipante");
		OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_ESCURSIONI_PAR,partecipante));
		
		ArrayList<Object[]> objList = (ArrayList<Object[]>) response.getData();
		listaEscursioni.clear();
		
		for(Object obj[] : objList){
			listaEscursioni.add((EscursioneTO) obj[0]);
		}
		//list_escursioni = (List<EscursioneTO>) response.getData();

		ObservableList<TableModelEscursione> data = FXCollections.observableArrayList(getListTabellaEscursioni(listaEscursioni));

		this.initColumn(columnNomeEscursione, "nome");
		this.initColumn(columnTipoEscursione, "nomeTipoEscursione");
		this.initColumn(columnDataEscursione, "data");
		this.initColumn(columnNumMinEscursione, "numMin");
		this.initColumn(columnNumMaxEscursione, "numMax");
		this.initColumn(columnPrezzoEscursione, "prezzo");

		tblEscursioniPAR.setItems(data);

		tblEscursioniPAR.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				TableView<TableModelEscursione> table_escursioni = (TableView<TableModelEscursione>) event.getSource();
				escursione_model = table_escursioni.getSelectionModel().getSelectedItem();
				if(escursione_model != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
						iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
						iscrizione.setEscursione(escursione_model.getEscursione());
						iscrizione.setUtente((PartecipanteTO) SessionData.getSessionData("Partecipante"));
						OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ISCRIZIONE_FROM_ESCURSIONE,iscrizione));
						if(response.toString().equals(SUCCESS)){
							iscrizione = (IscrizioneTO) response.getData();
							eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE, ViewsCache.getNestedAnchorPane(),iscrizione ));
						}
					}
				}
			}
		});
	}

	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati nella tabella
	 * 
	 * @param lista delle escursione
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelEscursione> getListTabellaEscursioni(List<EscursioneTO> list_escursioni){
		ObservableList<TableModelEscursione> res = FXCollections.observableArrayList();

		for(EscursioneTO escursione : list_escursioni){
			escursione_model = new TableModelEscursione(escursione);
			res.add(escursione_model);
		}

		return res;
	}

	/**
	 * Metodo associato all'evento del click del bottone Cerca Escursioni.
	 */
	@FXML protected void ricercaEscursione(){
		String param = txtRicerca.getText();
		List<EscursioneTO> list_escursione = new ArrayList<>();


		for(EscursioneTO escursione : this.listaEscursioni){
			if(escursione.getNome().contains(param) ||
					escursione.getData().contains(param) ||
					escursione.getDescrizione().contains(param) ||
					escursione.getTipoEscursione().getNome().contains(param.toUpperCase()))
				list_escursione.add(escursione);
			else
				try{
					if(escursione.getNumMax() == Integer.parseInt(param) || 
							escursione.getNumMin() == Integer.parseInt(param) ||
							escursione.getPrezzo() == Double.parseDouble(param))
						list_escursione.add(escursione);
				}catch(Exception e){
				}
		}

		ObservableList<TableModelEscursione> data = FXCollections.observableArrayList(getListTabellaEscursioni(list_escursione));

		tblEscursioniPAR.setItems(data);
	}

	@SuppressWarnings("unchecked")
	@FXML
	protected void btnEliminaIscrizione(){

		escursione_model = tblEscursioniPAR.getSelectionModel().getSelectedItem();
		if(escursione_model != null){
			Alert alert = new Alert(AlertType.CONFIRMATION, "Sei sicuro di voler cancellare l'iscrizione selezionata?");
			alert.setTitle("OutDoorSport1.0");
			Optional<ButtonType> res = alert.showAndWait();

			if(res.get() == ButtonType.OK){
				PartecipanteTO partecipante = (PartecipanteTO) SessionData.getSessionData("Partecipante");
				iscrizione.setUtente(partecipante);
				iscrizione.setEscursione(escursione_model.getEscursione());
				OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ISCRIZIONE_FROM_ESCURSIONE,iscrizione ));
				if(response.toString().equals(SUCCESS)){
					iscrizione = (IscrizioneTO) response.getData();
					OutdoorResponse rsp = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_STATO_ESCURSIONE, iscrizione ));
					list_stato_iscrizione = (List<StatoIscrizioneTO>) rsp.getData();
					iscrizione.setStatoIscrizione(list_stato_iscrizione.get(0));
					OutdoorResponse resp = this.eseguiRichiesta(new OutdoorRequest(ACTION_DELETE_ISCRIZIONE_FROM_ESCURSIONE_PAR,iscrizione));
					if(resp.toString().equals(SUCCESS)){
						EmailSettings emailConfig = new EmailSettings();
						EmailTO email = (EmailTO)FactoryTO.getFactoryTO(EnumTO.Email);
						ArrayList<UtenteTO> dest = new ArrayList<>();
						ManagerDiEscursioneTO mde = (ManagerDiEscursioneTO)iscrizione.getEscursione().getUtente();
						dest.add(mde);
						String mailOggetto = "OutDoorSports | Disiscrizione Partecipante";
						String mailMessaggio = "Gentile ";
						mailMessaggio += mde.getNome() + " " + mde.getCognome() + ", \n";
						mailMessaggio += "Il partecipante " + partecipante.getNome() + " " + partecipante.getCognome();
						mailMessaggio += "con codice fiscale " + partecipante.getCf();
						mailMessaggio += "ha scelto di disiscriversi dall'escursione " + iscrizione.getEscursione().getNome();
						email.setOggetto(mailOggetto);
						email.setMessaggio(mailMessaggio);
						email.setDestinatari(dest);
						emailConfig.sendEmail(email);
						allEscursioniPAR();
					}
				}
			}
		}else{
			Alert alert1 = new Alert(AlertType.ERROR, "Nessuna Escursione Selezionata", ButtonType.OK);
			alert1.setTitle("OutDoorSport1.0");
			alert1.showAndWait();
		}		
	}

	@FXML
	protected void btnModificaIscrizione(){
		escursione_model = tblEscursioniPAR.getSelectionModel().getSelectedItem();
		if(escursione_model != null){
			iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
			iscrizione.setEscursione(escursione_model.getEscursione());
			iscrizione.setUtente((PartecipanteTO) SessionData.getSessionData("Partecipante"));
			OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ISCRIZIONE_FROM_ESCURSIONE,iscrizione));
			if(response.toString().equals(SUCCESS)){
				iscrizione = (IscrizioneTO) response.getData();
				eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE, ViewsCache.getNestedAnchorPane(),iscrizione ));
			}
		}
		else{
			Alert alert = new Alert(AlertType.ERROR, "Nessuna Escursione Selezionata", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}	
	}
}
