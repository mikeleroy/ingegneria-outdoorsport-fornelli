package app.presentation.views.partecipante;

import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.PartecipanteTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;



public class ControllerDashboardPAR extends BaseController{
	

	@FXML protected AnchorPane anchorContentPAR;
	@FXML protected AnchorPane anchorPartecipante;
	@FXML protected GridPane menuHeader;
	@FXML protected Label lblUtente;
	
	Scene scene = null;
	
	
	private PartecipanteTO partecipante = null;
	/**
	 * Costruttore che avvalora il partecipante in memoria
	 */
	public ControllerDashboardPAR() {
		if(partecipante == null){
			partecipante = (PartecipanteTO) FactoryTO.getFactoryTO(EnumTO.Partecipante);
		}
	}
	
	
	@Override
	protected void initialize() {
	

		anchorPartecipante.sceneProperty().addListener((obs, oldScene, newScene) -> {
		    if (newScene == null) {
		    	//TODO
			} else {
				partecipante = (PartecipanteTO) SessionData.getSessionData(partecipante.getClass().getSimpleName());
				stage = (Stage) anchorPartecipante.getScene().getWindow();
				stage.setTitle("OutDoor Sport v.1.0");
				this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_PAR,anchorContentPAR));
				 
		    }
		});
		
		

	
	}	
	



	@FXML protected void btnTrovaEscursioni(MouseEvent event) {	
		
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_TROVA_ESCURSIONI_PAR,anchorContentPAR));
		 
//		 menuHeader.getStyleClass().clear();
//		 menuHeader.getStyleClass().add("menu-header-active");		 

		 }
	
	@FXML protected void btnIscrizioniEscursioni(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_ESCURSIONI_PAR,anchorContentPAR));
	}
	
	@FXML protected void btnProfilo(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_PAR,anchorContentPAR));
	}
	

	@FXML protected void lblLogout(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_LOGIN));
	}




	

}
