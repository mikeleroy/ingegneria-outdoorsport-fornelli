package app.presentation.views.partecipante;

import java.util.Optional;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.ManagerDiSistemaTO;
import app.to.PartecipanteTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;
import app.utility.Rules;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class ControllerModificaPartecipante extends BaseController implements Rules{

	private OutdoorResponse risposta;
	@FXML private TextField txtNome;
	@FXML private TextField txtCognome;
	@FXML private TextField txtCF;
	@FXML private TextField txtIndirizzo;
	@FXML private TextField txtCitta;
	@FXML private Label lblErrore;	
	@FXML private StackPane stackDatiAnagrafici;

	
	private PartecipanteTO partecipante = null;
	private ManagerDiEscursioneTO managerEscursione = null;
	private ManagerDiSistemaTO managerSistema = null;

	private UtenteTO user = null;
	
	public ControllerModificaPartecipante() {
		if(partecipante == null){
			partecipante = (PartecipanteTO) FactoryTO.getFactoryTO(EnumTO.Partecipante);

		}
	}
	
	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					partecipante = (PartecipanteTO) SessionData.getSessionData(partecipante.getClass().getSimpleName());

					txtNome.setText(partecipante.getNome());
					txtCognome.setText(partecipante.getCognome());
					txtCF.setText(partecipante.getCf());
					txtCitta.setText(partecipante.getCitta());
					txtIndirizzo.setText(partecipante.getIndirizzo());
				
				}
			}
		};
		
		stackDatiAnagrafici.visibleProperty().addListener(visibilityListener);		
	}

	
	
	
	@FXML protected void btnSave(MouseEvent event) {	
		 modificaDati();
	}
	
	
	
	private void modificaDati() {
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("OutDoorSports 1.0");
		alert.setHeaderText("Modifica dati anagrafici");
		alert.setContentText("Sei sicuro di apportare queste modifiche?");
		Optional<ButtonType> res = alert.showAndWait();
		
		if(res.get() == ButtonType.OK)
		{
		
		partecipante.setNome(txtNome.getText());
		partecipante.setCognome(txtCognome.getText());
		partecipante.setCf(txtCF.getText());
		partecipante.setCitta(txtCitta.getText());
		partecipante.setIndirizzo(txtIndirizzo.getText());
		 
		

		
//			String result = checkErrors(user);
			String result ="";
			if(result.equals(""))
			{
				OutdoorResponse risposta = this.eseguiRichiesta(new OutdoorRequest(ACTION_PAR_MODIFICA,partecipante ));

				if(risposta.getResponse().equals(SUCCESS)){
					 this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_PAR,ViewsCache.getNestedAnchorPane()));
				}
				else{
					lblErrore.setText("Errore nella modifica del partecipante.");
				}
			}else{
				lblErrore.setText(result);
			}
		}
	}
	
	
	
	

	@FXML protected void btnCancel(MouseEvent event) {	
		this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_PAR,ViewsCache.getNestedAnchorPane()));
	}
	
	
	
}
