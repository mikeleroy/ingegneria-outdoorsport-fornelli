package app.presentation.views.partecipante;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.OptionalEscursioneTO;
import app.to.PartecipanteTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.StackPane;

/**
 * Classe Controller che gestisce l'iscrizione del partecipante
 * all'escursione precedentemente scelta.
 * Il Partecipante pu� scegliere gli optional, e verr�
 * calcolato il costo risultante in base agli optional scelti.
 * 
 * @author michele fornelli
 *
 */
public class ControllerIscrizioneEscursionePAR extends BaseController{

	@FXML private StackPane stackIscrizioneEscursione;
	@FXML private Label lblNomeEscursione;
	@FXML private Label lblPrezzo;
	@FXML private Label lblData;
	@FXML private Label lblNumMax;
	@FXML private Label lblNumMin;
	@FXML private Label lblPrezzoTotale; 
	@FXML private Label lblTipoEscursione;
	@FXML private Label lblOptionalScelti;
	@FXML private Button btnSelezionaOptional;
	@FXML private Button btnConfermaIscrizione;
	@FXML private Button btnBack;
	private IscrizioneTO iscrizione = null;
	private EscursioneTO escursione = null;
	private PartecipanteTO partecipante = null;

	/**
	 * Costruttore
	 */
	public ControllerIscrizioneEscursionePAR() {
		if(iscrizione == null){
			iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
		if(partecipante == null){
			partecipante = (PartecipanteTO) FactoryTO.getFactoryTO(EnumTO.Partecipante);
		}
	}

	/**
	 * Metodo che inizializza la schermata dell'iscrizione all'escursione
	 */
	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					partecipante = (PartecipanteTO) SessionData.getSessionData(partecipante.getClass().getSimpleName());
					iscrizione = (IscrizioneTO) SessionData.getSessionData("Iscrizione");
					lblNomeEscursione.setText(iscrizione.getEscursione().getNome());
					lblTipoEscursione.setText(iscrizione.getEscursione().getTipoEscursione().getNome());
					lblData.setText(iscrizione.getEscursione().getData());
					lblNumMin.setText(Integer.toString(iscrizione.getEscursione().getNumMin()));
					lblNumMax.setText(Integer.toString(iscrizione.getEscursione().getNumMax()));
					lblPrezzo.setText(Double.toString(iscrizione.getEscursione().getPrezzo()) + " �");
					String string = "";
					double prezzoTotale = iscrizione.getEscursione().getPrezzo();
					Set<OptionalEscursioneTO> optionals = new HashSet<>();
					optionals = (Set<OptionalEscursioneTO>) iscrizione.getOptionals();
					if(optionals == null)
						string = "Nessun Optional Scelto";
					else{
						for(OptionalEscursioneTO optional : optionals){
							if(optional.getStatoOptional().getIdStatoOptional() == 2){
								string += optional.getOptional().getNome() + " | ";
								prezzoTotale += optional.getOptional().getTipoOptional().getPrezzo();
							}
						}
					}
					lblOptionalScelti.setText(string);
					lblPrezzoTotale.setText(Double.toString(prezzoTotale) + " �");
				}
			}
		};

		stackIscrizioneEscursione.visibleProperty().addListener(visibilityListener);
	}

	/**
	 * Evento associato alla gestione degli optional per 
	 * quella escursione, di un determinato partecipante
	 */
	@FXML protected void btnSelezionaOptional(){
		this.eseguiRichiesta(new OutdoorRequest(VIEW_SELEZIONA_OPTIONAL_ISCRIZIONE , ViewsCache.getNestedAnchorPane(),iscrizione ));
	}

	/**
	 * Metodo associato all'evento del click del bottone Conferma Iscrizione
	 */
	@FXML
	protected void btnIscrizione(){
		OutdoorResponse response;
		Alert alertConfirm = new Alert(AlertType.CONFIRMATION, "Vuoi confermare questa iscrizione?");
		Optional<ButtonType> result = alertConfirm.showAndWait();
		if (result.get() == ButtonType.OK){
			response = this.eseguiRichiesta(new OutdoorRequest(ACTION_CREATE_OPTIONAL_FROM_ISCRIZIONE , iscrizione ));
			if(response.toString().equals(SUCCESS)){
				iscrizione = null;
				this.eseguiRichiesta(new OutdoorRequest(VIEW_TROVA_ESCURSIONI_PAR , ViewsCache.getNestedAnchorPane(),iscrizione ));
			}else{
				Alert alert = new Alert(AlertType.ERROR, "Errore! Qualcosa � andata storta durante l'iscrizione!", ButtonType.OK);
				alert.setTitle("OutDoorSport1.0");
				alert.showAndWait();
			}
		} else {
			alertConfirm.close();
		}
	}

	/**
	 * Metodo associato all'evento del click del bottone Indietro
	 */
	@FXML
	protected void btnCancel(){
		Alert alertConfirm = new Alert(AlertType.CONFIRMATION, "Attenzione! Perderai tutte le modifiche non confermate");
		Optional<ButtonType> result = alertConfirm.showAndWait();
		if (result.get() == ButtonType.OK){
			iscrizione = null;
			this.eseguiRichiesta(new OutdoorRequest(VIEW_TROVA_ESCURSIONI_PAR , ViewsCache.getNestedAnchorPane(),iscrizione ));
		} else {
			alertConfirm.close();
		}	
	}
}

