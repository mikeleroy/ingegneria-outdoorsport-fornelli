package app.presentation.views.partecipante;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.TableViewController;
import app.presentation.views.tableModels.TableModelOptional;
import app.to.EnumTO;
import app.to.IscrizioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalTO;
import app.to.StatoOptionalTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;


/**
 * Classe Controller che gestisce la scelta degli
 * optional da parte di una Partecipante.
 * Il Partecipante pu� scegliere gli optional, e verr�
 * calcolato il costo risultante in base agli optional scelti.
 * 
 * @author michele fornelli
 *
 */

public class ControllerIscrizioneSceltaOptional extends TableViewController{

	@FXML private StackPane stackSelezionaOptional;
	@FXML private TableView<TableModelOptional> tblOptionalScelti;
	@FXML private TableView<TableModelOptional> tblOptionalDisponibili;
	@FXML private TableColumn<TableModelOptional, String> columnOptionalScelti;
	@FXML private TableColumn<TableModelOptional, String> columnPrezzoScelti;
	@FXML private TableColumn<TableModelOptional, String> columnTipoOptionalScelti;
	@FXML private TableColumn<TableModelOptional, String> columnOptionalDisponibili;
	@FXML private TableColumn<TableModelOptional, String> columnPrezzoDisponibili;
	@FXML private TableColumn<TableModelOptional, String> columnTipoOptionalDisponibili;
	@FXML private Button btnInserisciOptional;
	@FXML private Button btnRimuoviOptional;
	@FXML private Label lblPrezzoTotaleOptional;
	@FXML private Label lblPrezzoTotale;
	@FXML private Button btnIndietro;
	@FXML private Button btnConferma;
	private IscrizioneTO iscrizione = null;
	private IscrizioneTO iscrizioneOld = null;
	private TableModelOptional optionalDisponibiliModel = null;
	private TableModelOptional optionalSceltiModel = null;
	private StatoOptionalTO statoOptional = null;
	@SuppressWarnings("unused")
	private List<StatoOptionalTO> listaStatoOptional = new ArrayList<>();
	private OptionalEscursioneTO optionalEscursione = null;
	private Set<OptionalEscursioneTO> allOptionalScelti = null;
	private Set<OptionalEscursioneTO> allOptionalDisponibili = null;
	private Set<OptionalEscursioneTO> allCurrentOptionalDisponibili = null;

	public ControllerIscrizioneSceltaOptional() {
		if(iscrizione == null){
			iscrizione = (IscrizioneTO) FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
		if(iscrizioneOld == null){
			iscrizioneOld = (IscrizioneTO)  FactoryTO.getFactoryTO(EnumTO.Iscrizione);
		}
		if(optionalEscursione == null){
			optionalEscursione = (OptionalEscursioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);
		}
		if(statoOptional == null){
			statoOptional = (StatoOptionalTO) FactoryTO.getFactoryTO(EnumTO.StatoOptional);
		}
	}

	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					iscrizione = (IscrizioneTO) SessionData.getSessionData(iscrizione.getClass().getSimpleName());
					iscrizioneOld = (IscrizioneTO) SessionData.getSessionData(iscrizione.getClass().getSimpleName());
					OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_STATO_OPTIONAL,statoOptional));
					listaStatoOptional = (List<StatoOptionalTO>) response.getData();
					lblPrezzoTotale.setText("0 �");
					lblPrezzoTotaleOptional.setText("0 �");

					optionalEscursione.setEscursione(iscrizione.getEscursione());
					OutdoorResponse resp = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_OPTIONAL_ESCURSIONE,optionalEscursione));
					Set<OptionalEscursioneTO> temp = new HashSet<>();
					temp.addAll((Collection<? extends OptionalEscursioneTO>) resp.getData());
					allOptionalDisponibili = new HashSet<>();
					allOptionalDisponibili.addAll(temp);
					allOptionalScelti = new HashSet<>();
					if(iscrizione.getOptionals() != null)
						allOptionalScelti.addAll(iscrizione.getOptionals());
					allCurrentOptionalDisponibili = new HashSet<>();
					allCurrentOptionalDisponibili.addAll(allOptionalDisponibili);

					for(OptionalEscursioneTO optional : allOptionalDisponibili){
						for(OptionalEscursioneTO oe : allOptionalScelti){
							if(optional.getOptional().getNome().equals(oe.getOptional().getNome())){
								allCurrentOptionalDisponibili.remove(optional);
							}
						}
					}
					setTables();
				}
			}
		};

		stackSelezionaOptional.visibleProperty().addListener(visibilityListener);
	}

	/**
	 * Metodo che carica gli optional disponibili per quella escursione nella tabella in alto, 
	 * mentre carica gli optional scelti nella tabella in basso, in base agli optional
	 * disponibili
	 */
	@SuppressWarnings("unchecked")
	private void setTables(){

		ObservableList<TableModelOptional> dataDisponibili = FXCollections.observableArrayList(getListTabellaOptionalDisponibili(allCurrentOptionalDisponibili));

		this.initColumn(columnOptionalDisponibili, "nome");
		this.initColumn(columnPrezzoDisponibili, "prezzo");
		this.initColumn(columnTipoOptionalDisponibili, "nomeTipoOptional");

		tblOptionalDisponibili.setItems(dataDisponibili);

		tblOptionalDisponibili.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				tblOptionalDisponibili = (TableView<TableModelOptional>) event.getSource();
				optionalDisponibiliModel = tblOptionalDisponibili.getSelectionModel().getSelectedItem();
				if(optionalDisponibiliModel != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
						inserisciOptional();
					}
				}
			}
		});

		ObservableList<TableModelOptional> dataScelti = FXCollections.observableArrayList(getListTabellaOptionalScelti(allOptionalScelti));

		this.initColumn(columnOptionalScelti, "nome");
		this.initColumn(columnPrezzoScelti, "prezzo");
		this.initColumn(columnTipoOptionalScelti, "nomeTipoOptional");

		tblOptionalScelti.setItems(dataScelti);

		tblOptionalScelti.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				tblOptionalScelti = (TableView<TableModelOptional>) event.getSource();
				optionalSceltiModel = tblOptionalScelti.getSelectionModel().getSelectedItem();
				if(optionalSceltiModel != null){
					if(event.getClickCount() == 2){
						rimuoviOptional();
					}
				}
			}
		});

		double prezzoTotale = iscrizione.getEscursione().getPrezzo();
		double prezzoTotaleOptional = 0;
		for(OptionalEscursioneTO oe : allOptionalScelti){
			if(oe.getStatoOptional().getIdStatoOptional() == 2){
				prezzoTotale = prezzoTotale + oe.getOptional().getTipoOptional().getPrezzo();
				prezzoTotaleOptional = prezzoTotaleOptional + oe.getOptional().getTipoOptional().getPrezzo();
			}
		}
		lblPrezzoTotale.setText(Double.toString(prezzoTotale) + " �");
		lblPrezzoTotaleOptional.setText(Double.toString(prezzoTotaleOptional) + " �");
	}

	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati
	 * nella tabella degli optional disponibili
	 * 
	 * @param lista Optional
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelOptional> getListTabellaOptionalDisponibili(Set<OptionalEscursioneTO> param){
		ObservableList<TableModelOptional> res = FXCollections.observableArrayList();

		for(OptionalEscursioneTO optional : param){
			optionalSceltiModel = new TableModelOptional(optional.getOptional());
			if(optional.getStatoOptional().getIdStatoOptional() == 2)
				res.add(optionalSceltiModel);
		}

		return res;
	}

	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati
	 * nella tabella degli optional scelti dal partecipante iscritto
	 * 
	 * @param lista Optional
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelOptional> getListTabellaOptionalScelti(Set<OptionalEscursioneTO> param){
		ObservableList<TableModelOptional> res = FXCollections.observableArrayList();

		for(OptionalEscursioneTO optional : param){
			optionalSceltiModel = new TableModelOptional(optional.getOptional());
			if(optional.getStatoOptional().getIdStatoOptional() == 2)
				res.add(optionalSceltiModel);
		}

		return res;
	}

	/**
	 * Evento associato all'inserimento di un optional nella lista degli optional
	 * scelti tra quelli disponibili per quella escursione
	 */
	@FXML protected void inserisciOptional(){
		optionalDisponibiliModel = tblOptionalDisponibili.getSelectionModel().getSelectedItem();
		if(optionalDisponibiliModel != null){
			for(OptionalEscursioneTO o : allCurrentOptionalDisponibili){
				if(o.getOptional().getNome().equals(optionalDisponibiliModel.getNome())){
					optionalEscursione = (OptionalEscursioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);
					optionalEscursione = o;
					break;
				}
			}
			allCurrentOptionalDisponibili.remove(optionalEscursione);
			allOptionalScelti.add(optionalEscursione);
			setTables();
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Nessun Optional Selezionato", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}

	/**
	 * Evento associato alla rimozione di un optional dalla lista degli optional
	 * scelti tra quelli disponibili per quella escursione
	 */
	@FXML protected void rimuoviOptional(){
		optionalSceltiModel = tblOptionalScelti.getSelectionModel().getSelectedItem();
		if(optionalSceltiModel != null){
			for(OptionalEscursioneTO oe : allOptionalScelti){
				if(oe.getOptional().getNome().equals(optionalSceltiModel.getNome())){
					optionalEscursione = (OptionalEscursioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);
					optionalEscursione = oe;
					break;
				}
			}
			allOptionalScelti.remove(optionalEscursione);
			allCurrentOptionalDisponibili.add(optionalEscursione);
			setTables();
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Nessun Optional Selezionato", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}

	/**
	 * Torna ai dettagli delle iscrizioni e lascia invariata la
	 * modifica degli optional scelti
	 */
	@FXML protected void btnCancel(){
		Alert alert = new Alert(AlertType.CONFIRMATION, "Sei sicuro di voler annullare le modifiche?");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE, ViewsCache.getNestedAnchorPane(),iscrizioneOld ));
		} else {
			alert.close();
		}
	}

	/**
	 * Conferma le modifiche della scelta degli optional, e li aggiorna
	 * nella schermata dei dettagli dell'iscrizione. Le modifiche non vengono
	 * ancora effettuate nel database
	 */
	@FXML protected void btnConferma(){
		iscrizione.setOptionals(allOptionalScelti);
		eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE , ViewsCache.getNestedAnchorPane(), iscrizione));
	}
}
