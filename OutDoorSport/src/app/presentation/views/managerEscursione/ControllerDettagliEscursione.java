package app.presentation.views.managerEscursione;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseEscursioneController;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.StatoEscursioneTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

/**
 * Visualizza i dettagli di una determinata Escursione. Il Manager
 * di Escursione pu� visualizzare tutti i Partecipanti iscritti
 * all'escursione, modificare l'escursione o annullarla
 * 
 * @author michele fornelli
 *
 */

public class ControllerDettagliEscursione extends BaseEscursioneController{

	@FXML private StackPane stackDettagliEscursione;
	@FXML private Label lblNomeEscursione;
	@FXML private Label lblStatoEscursione;
	@FXML private Label lblTipoEscursione;
	@FXML private Label lblDataEscursione;
	@FXML private Label lblNumMin;
	@FXML private Label lblNumMax;
	@FXML private Label lblCostoEscursione;
	@FXML private Label lblDescrizioneEscursione;
	@FXML private Button btnModificaEscursione;
	@FXML private Button btnAnnullaEscursione;
	@FXML private Button btnVisualizzaIscritti;
	@FXML private Button btnIndietro;
	private EscursioneTO escursione = null;
	private List<StatoEscursioneTO> list_stato_escursione = new ArrayList<>();
	private StatoEscursioneTO stato_escursione = null;
	
	
	public ControllerDettagliEscursione() {
		if(stato_escursione == null){
			stato_escursione = (StatoEscursioneTO)FactoryTO.getFactoryTO(EnumTO.StatoEscursione);
		}
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
	}

	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					escursione = (EscursioneTO) SessionData.getSessionData(escursione.getClass().getSimpleName());
					lblNomeEscursione.setText(escursione.getNome());
					lblStatoEscursione.setText("Stato: " + escursione.getStatoEscursione().getNome());
					lblTipoEscursione.setText("Tipo: " + escursione.getTipoEscursione().getNome());
					lblDataEscursione.setText("Data: " + escursione.getData());
					lblNumMin.setText("Minimo " + escursione.getNumMin() + " Partecipanti");
					lblNumMax.setText("Massimo " + escursione.getNumMax() + " Partecipanti");
					lblCostoEscursione.setText("Costo: " + escursione.getPrezzo());
						
					lblDescrizioneEscursione.setText("Descrizione: " + escursione.getDescrizione());
					if(list_stato_escursione.isEmpty()){
						OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_STATO_ESCURSIONE,stato_escursione));
						list_stato_escursione = (List<StatoEscursioneTO>) response.getData();
					}
				}
			}
		};
		
		stackDettagliEscursione.visibleProperty().addListener(visibilityListener);
	}
	
	/**
	 * Evento associato alla view. Torna alla schermata precedente
	 */
	@FXML protected void visualizzaEscursioni(){
		this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_ESCURSIONI, ViewsCache.getNestedAnchorPane() ));
	}
	
	/**
	 * Evento associato alla view. Visualizza gli iscritti dell'escursione
	 */
	@FXML protected void visualizzaIscritti(){
		this.eseguiRichiesta(new OutdoorRequest(VIEW_ISCRITTI_ESCURSIONE, ViewsCache.getNestedAnchorPane(),escursione ));
	}
	
	/**
	 * Evento associato alla modifica di una escursione
	 */
	@FXML protected void modificaEscursione(){
		if(escursione.getStatoEscursione().getNome().equals(list_stato_escursione.get(3).getNome()))
			this.eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ESCURSIONE,ViewsCache.getNestedAnchorPane()));
		else if(escursione.getStatoEscursione().getNome().equals(list_stato_escursione.get(2).getNome())){
			Alert alert = new Alert(AlertType.ERROR, "Non � possibile modificare una escursione in corso!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}else if(escursione.getStatoEscursione().getNome().equals(list_stato_escursione.get(0).getNome())){
			Alert alert = new Alert(AlertType.ERROR, "Non � possibile modificare una escursione chiusa!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}else if(escursione.getStatoEscursione().getNome().equals(list_stato_escursione.get(1).getNome())){
			Alert alert = new Alert(AlertType.ERROR, "Non � possibile modificare una escursione annullata!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}		
	}
	
	/**
	 * Evento associato all'annullamento di una escursione
	 */
	@FXML protected void annullaEscursione(){
		LocalDate escursione_date = LocalDate.parse(escursione.getData());
		LocalDate date_now = LocalDate.now();
		if(escursione.getStatoEscursione().getNome().equals(list_stato_escursione.get(3).getNome()) && (ChronoUnit.DAYS.between(date_now, escursione_date) > 2)){
			Alert alert = new Alert(AlertType.CONFIRMATION, "Sei Sicuro di voler annullare questa escursione?");
			alert.setTitle("OutDoorSport1.0");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK){
				OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_ANNULLA_ESCURSIONE,escursione));
				if(response.toString().equals(SUCCESS)){
					Alert alert1 = new Alert(AlertType.INFORMATION, "Escursione annullata con successo!", ButtonType.OK);
					alert1.setTitle("OutDoorSport1.0");
					alert1.showAndWait();
					this.eseguiRichiesta(new OutdoorRequest( VIEW_LISTA_ESCURSIONI,ViewsCache.getNestedAnchorPane()));
				}
			} else
				alert.close();
		}else if(!escursione.getStatoEscursione().getNome().equals(list_stato_escursione.get(1).getNome())){
			Alert alert = new Alert(AlertType.ERROR, "Non � possibile annullare l'escursione", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Non � possibile annullare una escursione a meno di due giorni prima del suo inizio!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}

	
	@Override
	protected void controlloDataEscursione(EscursioneTO escursione) {
		// TODO Auto-generated method stub
		
	}	
}
