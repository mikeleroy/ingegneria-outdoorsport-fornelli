package app.presentation.views.managerEscursione;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseEscursioneController;
import app.presentation.views.tableModels.TableModelEscursione;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalTO;
import app.to.StatoOptionalTO;
import app.to.TipoEscursioneTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;

/**
 * Gestisce la modifica di una escursione da parte del
 * Manager di Escursione, solo se lo stato dell'escursione
 * � APERTA.
 * 
 * @author michele fornelli
 *
 */

public class ControllerModificaEscursione extends BaseEscursioneController{

	@FXML private StackPane stackModificaEscursione;
	@FXML private TextField txtNomeEscursione;
	@FXML private TextField txtMin;
	@FXML private TextField txtMax;
	@FXML private TextField txtCostoBase;
	@FXML private DatePicker dataEscursione;
	@FXML private TextArea txtDescrizione;
	@FXML private ChoiceBox<String> chbTipoEscursione;
	@FXML private ChoiceBox<String> chbSelezionaOptional;
	@FXML private ListView<String> listOptionalScelti;
	@FXML private Button btnRimuoviOptional;
	@FXML private Button btnInviaDati;
	@FXML private Label lblErrore;
	@FXML private ListView<String> listOptionalPresenti;
	@FXML private Button btnDisabilita;
	
	private EscursioneTO escursione = null;
	private TableModelEscursione escursioneModel = new TableModelEscursione();
	private TipoEscursioneTO tipoEscursione = null;
	private OptionalTO optional = null;
	private List<TipoEscursioneTO> listaTipoEscursione = new ArrayList<>();
	private List<OptionalTO> listaOptional = new ArrayList<>();
	private ObservableList<String> data = null;
	private ArrayList<String> strings = null;
	private OptionalEscursioneTO optionalEscursione = null;
	private List<StatoOptionalTO> listaStatoOptional = null;
	private StatoOptionalTO statoOptional = null;
	
	/**
	 * Costruttore della classe ControllerModificaEscursione
	 */
	public ControllerModificaEscursione() {
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
		if(tipoEscursione == null){
			tipoEscursione = (TipoEscursioneTO) FactoryTO.getFactoryTO(EnumTO.TipoEscursione);
		}
		if(optionalEscursione == null){
			optionalEscursione = (OptionalEscursioneTO) FactoryTO.getFactoryTO(EnumTO.OptionalEscursione);
		}
		if(statoOptional == null){
			statoOptional = (StatoOptionalTO) FactoryTO.getFactoryTO(EnumTO.StatoOptional);
		}
		if(optional == null){
			optional = (OptionalTO) FactoryTO.getFactoryTO(EnumTO.Optional);
		}
	}

	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					lblErrore.setText("");
					listOptionalPresenti.getItems().clear();
					listOptionalScelti.getItems().clear();
					
					escursione = (EscursioneTO) SessionData.getSessionData(escursione.getClass().getSimpleName());
					txtNomeEscursione.setText(escursione.getNome());
					txtMin.setText(Integer.toString(escursione.getNumMin()));
					txtMax.setText(Integer.toString(escursione.getNumMax()));
					txtCostoBase.setText(Double.toString(escursione.getPrezzo()));
					dataEscursione.setValue(LocalDate.parse(escursione.getData()));
					txtDescrizione.setText(escursione.getDescrizione());
					if(listaTipoEscursione.isEmpty()){
						OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_TIPI_ESCURSIONE,tipoEscursione ));
						listaTipoEscursione = (List<TipoEscursioneTO>) response.getData();
						strings = new ArrayList<>();
						for(TipoEscursioneTO tipoescursione: listaTipoEscursione){
							strings.add(tipoescursione.getNome());
						}
						data = FXCollections.observableArrayList(strings);
						chbTipoEscursione.setItems(data);
						chbTipoEscursione.getSelectionModel().select(escursione.getTipoEscursione().getNome());
					}
					if(listaOptional.isEmpty()){
						OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_OPTIONALS,optional));
						listaOptional = (List<OptionalTO>) response.getData();
						strings = new ArrayList<>();
						for(OptionalTO optional: listaOptional){
							strings.add(optional.getNome());
						}
						data = FXCollections.observableArrayList(strings);
						chbSelezionaOptional.setItems(data);
					}
					
					optionalEscursione.setEscursione(escursione);
					OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_OPTIONAL_ESCURSIONE,optionalEscursione));
					List<OptionalEscursioneTO> listaOptionalEscursione = (List<OptionalEscursioneTO>) response.getData();
					List<String> strings = new ArrayList<>();
					for(OptionalEscursioneTO op : listaOptionalEscursione)
						strings.add(op.getOptional().getNome() + " | " + op.getStatoOptional().getNome());
					listOptionalPresenti.getItems().addAll(strings);
				}
			}
		};

		stackModificaEscursione.visibleProperty().addListener(visibilityListener);
		
		chbSelezionaOptional.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				String choice = chbSelezionaOptional.getItems().get(newValue.intValue());
				if(!listOptionalScelti.getItems().contains(choice)){
					boolean flag = false;
					List<String> strings = listOptionalPresenti.getItems();
					for(String s : strings)
						if(s.contains(choice)){
							flag = true;
							break;
						}
					if(!flag){
						listOptionalScelti.getItems().add(listOptionalScelti.getItems().size(), choice);
						listOptionalScelti.scrollTo(choice);
						listOptionalScelti.edit(listOptionalScelti.getItems().size() - 1);
					}
				}
			}
		});
	}
	
	@FXML protected void modificaEscursione(){
		Alert alert = new Alert(AlertType.CONFIRMATION, "Confermare le modifiche?");
		alert.setTitle("OutDoorSport1.0");
		
		Optional<ButtonType> result = alert.showAndWait();
		
		if (result.get() == ButtonType.OK)
			execModificaEscursione();
	}
	
	@FXML protected void indietro(){
		Alert alert = new Alert(AlertType.CONFIRMATION, "Attenzione! Perderai tutte le modifiche effettuate! Sei Sicuro?");
		alert.setTitle("OutDoorSport1.0");
		
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
		    eseguiRichiesta(new OutdoorRequest(VIEW_DETTAGLI_ESCURSIONE_MDE, ViewsCache.getNestedAnchorPane()));
		} else {
		    alert.close();
		}
	}
	
	
	/**
	 * Evento associato alla disabilitazione di un optional dalla lista,
	 * prima della modifica dell'escursione
	 */
	@SuppressWarnings("unchecked")
	@FXML protected void disabilitaOptional(){
		if(listaStatoOptional == null){
			OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_STATO_OPTIONAL,statoOptional));
			listaStatoOptional = (List<StatoOptionalTO>) response.getData();
		}
		String choice = listOptionalPresenti.getSelectionModel().getSelectedItem();
		if(choice != null){
		if(choice.contains(listaStatoOptional.get(0).getNome())){
			String newChoice = choice.replace(listaStatoOptional.get(0).getNome(), listaStatoOptional.get(1).getNome());
			listOptionalPresenti.getItems().add(listOptionalPresenti.getItems().size(), newChoice);
			listOptionalPresenti.scrollTo(choice);
			listOptionalPresenti.edit(listOptionalPresenti.getItems().size() - 1);
			listOptionalPresenti.getItems().remove(choice);
		}else{
			String newChoice = choice.replace(listaStatoOptional.get(1).getNome(), listaStatoOptional.get(0).getNome());
			listOptionalPresenti.getItems().add(listOptionalPresenti.getItems().size(), newChoice);
			listOptionalPresenti.scrollTo(choice);
			listOptionalPresenti.edit(listOptionalPresenti.getItems().size() - 1);
			listOptionalPresenti.getItems().remove(choice);
		}
		}else{
			Alert alert = new Alert(AlertType.ERROR, "Attenzione! Nessun Optional Selezionato", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}
	
	/**
	 * Evento associato alla rimozione di un optional dalla lista, prima
	 * della modifica dell'escursione
	 */
	@FXML protected void rimuoviSelezione(){
		String choice = listOptionalScelti.getSelectionModel().getSelectedItem();
		listOptionalScelti.getItems().remove(choice);
	}
	
	/**
	 * Metodo che implementa la logica di modifica di una escursione
	 * esistente. Vengono effettuati tutti i controlli prima di 
	 * mandare i dati al database
	 */
	private void execModificaEscursione() {
		escursione.setNome(txtNomeEscursione.getText());
		escursione.setNumMin(Integer.parseInt(txtMin.getText()));
		escursione.setNumMax(Integer.parseInt(txtMax.getText()));
		escursione.setPrezzo(Double.parseDouble(txtCostoBase.getText()));
		escursione.setDescrizione(txtDescrizione.getText());
		for(TipoEscursioneTO te : listaTipoEscursione){
			if(te.getNome().equals(chbTipoEscursione.getValue())){
				escursione.setTipoEscursione(te);
				break;
			}
		}

		Set<OptionalTO> temp = new HashSet<>();
		for(OptionalTO op : listaOptional){
			for(int i=0; i<listOptionalScelti.getItems().size(); i++){
				if(listOptionalScelti.getItems().get(i).equals(op.getNome()))
					temp.add(op);
			}
		}
		
		for(OptionalTO op : listaOptional){
			for(int i=0; i<listOptionalPresenti.getItems().size(); i++){
				if(listOptionalPresenti.getItems().get(i).contains(op.getNome())){
					OptionalTO newOptional = (OptionalTO) FactoryTO.getFactoryTO(EnumTO.Optional);
					newOptional.setIdOptional(op.getIdOptional());
					newOptional.setNome(listOptionalPresenti.getItems().get(i));
					newOptional.setDescrizione(op.getDescrizione());
					newOptional.setTipoOptional(op.getTipoOptional());
					temp.add(newOptional);
				}
			}
		}
		escursione.setOptionals(temp);
		
		String result = checkErrorsEscursione(escursione);
		if(result.equals("")){
			OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_UPDATE_ESCURSIONE,escursione));
			if(response.toString().equals(SUCCESS))
				this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_ESCURSIONI,ViewsCache.getNestedAnchorPane()));
			else
				lblErrore.setText("Errore! L'Escursione � gi� presente nel Sistema!");
		}else
			lblErrore.setText(result);
	}
	
	
	@Override
	protected void controlloDataEscursione(EscursioneTO escursione) {
		if(dataEscursione.getValue() == null)
			escursione.setData("");
		else{
			if(dataEscursione.getValue().getYear() > LocalDate.now().getYear())
				escursione.setData(dataEscursione.getValue().toString());
		}
	}
}
