package app.presentation.views.managerEscursione;

import java.util.ArrayList;
import java.util.List;

import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.presentation.views.TableViewController;
import app.presentation.views.tableModels.TableModelEscursione;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.impl.FactoryTO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class ControllerEscursioni extends TableViewController{

	private OutdoorRequest richiesta;
	private OutdoorResponse risposta;
	@FXML private TextField txtSearchEscursione;
	@FXML private Button btnSearchEscursione;
	@FXML private Button btnDettagliEscursione;
	@FXML private TableView<TableModelEscursione> tblListaEscursioni;
	@FXML private TableColumn<TableModelEscursione, String> columnNome;
	@FXML private TableColumn<TableModelEscursione, String> columnTipoEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnDataEscursione;
	@FXML private TableColumn<TableModelEscursione, String> columnMin;
	@FXML private TableColumn<TableModelEscursione, String> columnMax;
	@FXML private TableColumn<TableModelEscursione, String> columnPrezzo;
	//@FXML private TableColumn<EscursioneModel, String> columnStato;
	
	@FXML private StackPane stackEscursioni;
	
	private EscursioneTO escursione = null;
	private List<EscursioneTO> listaEscursioni = null;
	private TableModelEscursione modelEscursione = null;

	public ControllerEscursioni() {
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
	}

	@Override
	protected void initialize() {
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue)
					allEscursioniFromMDE();
			}
		};

		stackEscursioni.visibleProperty().addListener(visibilityListener);
	}
	
	
	@FXML protected void btnAggiungiESC(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_NUOVA_ESCURSIONE,ViewsCache.getNestedAnchorPane()));
	}
	

	/**
	 * Metodo che carica tutte le escursioni presenti nel sistema di un determinato Manager di Escursione
	 */
	@SuppressWarnings("unchecked")
	private void allEscursioniFromMDE(){
		OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_ESCURSIONI_MDE,escursione));
		listaEscursioni = (ArrayList<EscursioneTO>) response.getData();

		ObservableList<TableModelEscursione> data = FXCollections.observableArrayList(getListTabellaEscursioni(listaEscursioni));

		this.initColumn(columnNome, "nome");
		this.initColumn(columnTipoEscursione, "tipoEscursione");
		this.initColumn(columnDataEscursione, "data");
		this.initColumn(columnMin, "numMin");
		this.initColumn(columnMax, "numMax");
		this.initColumn(columnPrezzo, "prezzo");
		//this.initColumn(columnStato, "nomeStatoEscursione");

		tblListaEscursioni.setItems(data);

		tblListaEscursioni.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				TableView<TableModelEscursione> table_escursioni = (TableView<TableModelEscursione>) event.getSource();
				modelEscursione = table_escursioni.getSelectionModel().getSelectedItem();
				if(modelEscursione != null){
					if(event.getClickCount() == 2  && !event.isConsumed()){
						event.consume();
						eseguiRichiesta(new OutdoorRequest(VIEW_DETTAGLI_ESCURSIONE_MDE, ViewsCache.getNestedAnchorPane(),modelEscursione.getEscursione()));
					}
				}
			}
		});
	}

	/**
	 * Evento associato al Button per la ricerca di una Escursione. La tabella presente nella View
	 * verr� ricaricata in base ai parametri inseriti nella casella di testo.
	 */
	@FXML protected void cercaEscursione(){		
		String param = txtSearchEscursione.getText();
		List<EscursioneTO> list_escursione = new ArrayList<>();
		
		
		for(EscursioneTO escursione : this.listaEscursioni){
			if(escursione.getNome().contains(param) ||
					escursione.getData().contains(param) ||
					escursione.getDescrizione().contains(param) ||
					escursione.getTipoEscursione().getNome().contains(param.toUpperCase()))
				list_escursione.add(escursione);
			else
				try{
					if(escursione.getNumMax() == Integer.parseInt(param) || 
							escursione.getNumMin() == Integer.parseInt(param) ||
							escursione.getPrezzo() == Double.parseDouble(param))
						list_escursione.add(escursione);
				}catch(Exception e){
				}
		}
		
		ObservableList<TableModelEscursione> data = FXCollections.observableArrayList(getListTabellaEscursioni(list_escursione));

		tblListaEscursioni.setItems(data);
	}

	/**
	 * Metodo che inizializza il modello che servir� per visualizzare i dati nella tabella
	 * 
	 * @param lista di Escursioni
	 * @return res: il modello per la tabella
	 */
	private ObservableList<TableModelEscursione> getListTabellaEscursioni(List<EscursioneTO> listaEscursione){
		ObservableList<TableModelEscursione> res = FXCollections.observableArrayList();

		for(EscursioneTO escursione : listaEscursione){
			modelEscursione = new TableModelEscursione(escursione);
			res.add(modelEscursione);
		}

		return res;
	}

	/**
	 * Evento associato alla visualizzazione dei dettagli di una escursione. Una volta selezionata
	 * l'escursione desiderata, il Manager di Escursione, premendo il tasto, potr� visualizzare i 
	 * dettagli dell'Escursione
	 */
	@FXML protected void dettagliEscursione(){
		modelEscursione = tblListaEscursioni.getSelectionModel().getSelectedItem();
		if(modelEscursione != null)
			eseguiRichiesta(new OutdoorRequest(VIEW_DETTAGLI_ESCURSIONE_MDE, ViewsCache.getNestedAnchorPane(),modelEscursione.getEscursione()));
		else{
			Alert alert = new Alert(AlertType.ERROR, "Nessuna Escursione Selezionata", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}


}
