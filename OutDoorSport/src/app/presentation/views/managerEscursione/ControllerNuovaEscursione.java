package app.presentation.views.managerEscursione;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import app.integration.daoUtil.DatabaseException;
import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import app.presentation.views.BaseEscursioneController;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.OptionalTO;
import app.to.TipoEscursioneTO;
import app.to.impl.FactoryTO;
import app.utility.Actions;
import app.utility.Rules;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;

public class ControllerNuovaEscursione  extends BaseEscursioneController implements Rules,Actions{

	@FXML private StackPane stackNuovaEscursione;
	@FXML private TextField txtNomeEscursione;
	@FXML private TextField txtMin;
	@FXML private TextField txtMax;
	@FXML private TextField txtCostoBase;
	@FXML private DatePicker dataEscursione;
	@FXML private TextArea txtDescrizione;
	@FXML private ChoiceBox<String> chbSelezionaOptional;
	@FXML private ChoiceBox<String> chbTipoEscursione;
	@FXML private ListView<String> listAreaOptionalScelti;
	@FXML private Button btnRimuoviOptional;
	@FXML private Button btnInviaDati;
	@FXML private Label lblErrore;
	
	private EscursioneTO escursione = null;
	private TipoEscursioneTO tipoEscursione = null;
	private List<TipoEscursioneTO> listTipoEscursione = new ArrayList<>();
	private OptionalTO optional = null;
	private List<OptionalTO> listOptional = new ArrayList<>();
	private ObservableList<String> data = null;
	private ArrayList<String> strings = null;
	
	/**
	 * Costruttore della classe ControllerInserimentoEscursione
	 */
	public ControllerNuovaEscursione() {
		if(escursione == null){
			escursione = (EscursioneTO) FactoryTO.getFactoryTO(EnumTO.Escursione);
		}
		if(tipoEscursione == null){
			tipoEscursione = (TipoEscursioneTO) FactoryTO.getFactoryTO(EnumTO.TipoEscursione);
		}
		if(optional == null){
			optional = (OptionalTO) FactoryTO.getFactoryTO(EnumTO.Optional);
		}
	}

	@Override
	protected void initialize() {
		lblErrore.setText("");
		
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {

			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					if(listTipoEscursione.isEmpty()){
						OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_TIPI_ESCURSIONE,tipoEscursione));
						listTipoEscursione = (List<TipoEscursioneTO>) response.getData();
						strings = new ArrayList<>();
						for(TipoEscursioneTO tipoescursione: listTipoEscursione){
							strings.add(tipoescursione.getNome());
						}
						data = FXCollections.observableArrayList(strings);
						chbTipoEscursione.setItems(data);
					}
					if(listOptional.isEmpty()){
						OutdoorResponse response = eseguiRichiesta(new OutdoorRequest(ACTION_GET_ALL_OPTIONAL,optional));
						listOptional = (List<OptionalTO>) response.getData();
						strings = new ArrayList<>();
						for(OptionalTO optional: listOptional){
							strings.add(optional.getNome());
						}
						data = FXCollections.observableArrayList(strings);
						chbSelezionaOptional.setItems(data);
					}
				}
			}
		};
		
		stackNuovaEscursione.visibleProperty().addListener(visibilityListener);
		
		chbSelezionaOptional.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				String choice = chbSelezionaOptional.getItems().get(newValue.intValue());
				if(!listAreaOptionalScelti.getItems().contains(choice)){
					listAreaOptionalScelti.getItems().add(listAreaOptionalScelti.getItems().size(), choice);
					listAreaOptionalScelti.scrollTo(choice);
					listAreaOptionalScelti.edit(listAreaOptionalScelti.getItems().size() - 1);
				}
			}
		});
		
		txtCostoBase.textProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, 
		        String newValue) {
		        if (!newValue.matches("\\d*")) {
		        	txtCostoBase.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    }
		});
		
		txtMin.textProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, 
		        String newValue) {
		        if (!newValue.matches("\\d*")) {
		        	txtMin.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    }
		});
		
		txtMax.textProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, 
		        String newValue) {
		        if (!newValue.matches("\\d*")) {
		        	txtMax.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    }
		});
	}
	
	/**
	 * Evento associato alla rimozione di un optional dalla lista, prima
	 * del salvataggio della escursione
	 */
	@FXML protected void rimuoviOptional(){
		String choice = listAreaOptionalScelti.getSelectionModel().getSelectedItem();
		if(choice != null)
			listAreaOptionalScelti.getItems().remove(choice);
		else{
			Alert alert = new Alert(AlertType.ERROR, "Errore! Nessun optional scelto!", ButtonType.OK);
			alert.setTitle("OutDoorSport1.0");
			alert.showAndWait();
		}
	}
	
	
	@FXML protected void btnCancel(MouseEvent event) throws DatabaseException {	
		resetField();
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_ESCURSIONI,ViewsCache.getNestedAnchorPane()));
	}
	
	
	@FXML protected void creaNuovaEscursione() {
		salvaEscursione();
	}
	
	/**
	 * Metodo che implementa la logica di inserimento di una nuova
	 * escursione. Vengono effettuati tutti i controlli prima di 
	 * mandare i dati al database
	 */
	private void salvaEscursione() {
		try{
		escursione.setNome(txtNomeEscursione.getText());
		escursione.setNumMin(Integer.parseInt(txtMin.getText()));
		escursione.setNumMax(Integer.parseInt(txtMax.getText()));
		escursione.setPrezzo(Double.parseDouble(txtCostoBase.getText()));
		escursione.setDescrizione(txtDescrizione.getText());
		for(TipoEscursioneTO te : listTipoEscursione){
			if(te.getNome().equals(chbTipoEscursione.getValue())){
				escursione.setTipoEscursione(te);
				break;
			}
		}

		Set<OptionalTO> temp = new HashSet<>();
		for(OptionalTO op : listOptional){
			for(int i=0; i<listAreaOptionalScelti.getItems().size(); i++){
				if(listAreaOptionalScelti.getItems().get(i).equals(op.getNome()))
					temp.add(op);
			}
		}
		escursione.setOptionals(temp);
		
		String result = checkErrorsEscursione(escursione);
		if(result.equals("")){
			OutdoorResponse response = this.eseguiRichiesta(new OutdoorRequest(ACTION_SALVA_ESCURSIONE,escursione));
			if(response.toString().equals(SUCCESS)){
				this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_ESCURSIONI,ViewsCache.getNestedAnchorPane()));
				resetField();
			}
			else
				lblErrore.setText("Errore! L'Escursione � gi� presente nel Sistema!");
		}else
			lblErrore.setText(result);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	

	
	public void resetField(){
		txtNomeEscursione.setText("");
		txtCostoBase.setText("");
		txtDescrizione.setText("");
		txtMax.setText("");
		txtMin.setText("");
		lblErrore.setText("");
	
	
	}
	

@Override
protected void controlloDataEscursione(EscursioneTO escursione) {
	if(dataEscursione.getValue() == null)
		escursione.setData("");
	else{
		if(dataEscursione.getValue().getYear() >= LocalDate.now().getYear())
		{
			if(dataEscursione.getValue().getMonthValue() >= LocalDate.now().getMonthValue())
			{
				escursione.setData(dataEscursione.getValue().toString());
			
			}
				
		}
	}
}
}


