package app.presentation.views.managerEscursione;

import java.io.IOException;

import app.presentation.fc.FrontController;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.presentation.views.BaseController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;



public class ControllerDashboardESC extends BaseController{

	private OutdoorRequest richiesta;
	private OutdoorResponse risposta;

	
	@FXML private AnchorPane anchorContentESC;


	
	
	@Override
	protected void initialize() {
		
	}	
	
	
	@FXML protected void btnListaEscursioni(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_LISTA_ESCURSIONI,anchorContentESC));
		 }
	
	
	@FXML protected void btnProfilo(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_PROFILO_MANAGER_ESC,anchorContentESC));
	}
	
	
	

	@FXML protected void lblLogout(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_LOGIN));
	}
	

	
}