package app.presentation.views.managerEscursione;


import app.presentation.ac.ViewsCache;
import app.presentation.request.OutdoorRequest;
import app.presentation.views.BaseController;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;
import app.to.ManagerDiSistemaTO;
import app.to.PartecipanteTO;
import app.to.impl.FactoryTO;
import app.utility.SessionData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;


public class ControllerProfiloESC extends BaseController{
	
	@FXML private StackPane stackProfiloESC;
	@FXML private Label lblNome;
	@FXML private Label lblUsername;
	@FXML private Label lblEmail;
	@FXML private Label lblCitta;
	@FXML private Label lblIndirizzo;
	@FXML private Label lblCF;
	@FXML private Label lblDataN;
	@FXML AnchorPane pane;
	
	private ManagerDiEscursioneTO manager = null;
	
	/**
	 * Costruttore che avvalora il partecipante in memoria
	 */
	public ControllerProfiloESC() {
		if(manager == null){
			manager = (ManagerDiEscursioneTO) FactoryTO.getFactoryTO(EnumTO.ManagerDiEscursione);

		}
	}
	
	@Override
	protected void initialize() {
		
		ChangeListener<Boolean> visibilityListener = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				if(newValue){
					
					manager = (ManagerDiEscursioneTO) SessionData.getSessionData(manager.getClass().getSimpleName());
					lblNome.setText(manager.getNome() + " " + manager.getCognome());
					lblUsername.setText(manager.getUsername());
					lblEmail.setText(manager.getEmail());
				//	lblDataN.setText(partecipante.getDataNasc());
					lblCF.setText(manager.getCf());
					lblIndirizzo.setText(manager.getIndirizzo());
					lblCitta.setText(manager.getCitta());
					
				}
			}
		};
		
		stackProfiloESC.visibleProperty().addListener(visibilityListener);
		
		
	}
	
	
	
	@FXML protected void btnModificaAccesso(MouseEvent event) {	
		 this.eseguiRichiesta(new OutdoorRequest(VIEW_MODIFICA_ACCESSO,ViewsCache.getNestedAnchorPane()));
	}
	

	

}
