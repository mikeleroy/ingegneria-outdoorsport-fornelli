package app.presentation.views;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.presentation.fc.FrontController;
import app.presentation.request.OutdoorRequest;
import app.presentation.response.OutdoorResponse;
import app.to.EscursioneTO;
import app.to.UtenteTO;
import app.utility.Actions;
import app.utility.Rules;
import app.utility.Views;
import javafx.fxml.FXML;
import javafx.stage.Stage;

/**
 * Classe astratta BaseController che conterr� i metodi in comune per i controller
 * 
 * @author michele fornelli
 *
 */

public abstract class BaseController implements Actions, Views,Rules{
	protected String viewName;
	@FXML protected Stage stage;


	
	/**
	 * Metodo che inizializza tutti i campi della finestra
	 */	
	@FXML protected abstract void initialize();
	
	/**
	 * Metodo che invia una richiesta ai livelli pi� bassi per
	 * reperire delle informazioni. Restituisce una risposta
	 * in base alla richiesta 
	 * 
	 * @param request: richiesta da inviare
	 * @return response: risposta in base alla richiesta
	 */
	protected OutdoorResponse eseguiRichiesta(OutdoorRequest request){
		return FrontController.getInstance().eseguiRichiesta(request);
	}
	
	/** 
	 * @param str: stringa in ingresso
	 * @return stringa con la prima lettera maiuscola
	 */
	public String capitalizeWord(String str){
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	
	/**
	 * Funzione che restituisce la stringa degli errori rispetto alle informazioni inserite in maniera non corretta 
	 * per registrare il manager di sistema nella configurazione iniziale
	 * 
	 * @param utente: manager di sistema
	 * @return result: stringa errori
	 */
	protected String checkErrors(UtenteTO utente){
		String result = "";

		//checkDatePicker(utente);

		int i = 0;

		for (Field f : utente.getClass().getSuperclass().getDeclaredFields()) {
			f.setAccessible(true);
			try {
				if ((f.get(utente) == null || f.get(utente).equals(""))) {
					if(!(f.getName().equals("tipoUtente") || f.getName().equals("statoUtente") || f.getName().equals("idUtente"))){
						if(!f.getName().equals("email")){
							result += f.getName() + ", ";
							i++;
							if(i == 2){
								result += "\n";
								i = 0;
							}
						}
					}
				}

			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		for (Field f : utente.getClass().getDeclaredFields()) {
			f.setAccessible(true);
			try {
				if (f.get(utente) == null || f.get(utente).equals("")) {
					result += f.getName() + ", ";
					i++;
					if(i == 2){
						result += "\n";
						i = 0;
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}


		Pattern pattern = Pattern.compile(REGEX);
		Matcher matcher = pattern.matcher(utente.getEmail());

		if(!matcher.matches()){
			result += "email";
		}

		if(!result.equals(""))
			result += " non corretti!";

		return result;
	}

	

}
