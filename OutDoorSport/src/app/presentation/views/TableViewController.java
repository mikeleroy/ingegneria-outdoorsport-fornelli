package app.presentation.views;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * 
 * Classe astratta che incorpora tutti i controller che servono 
 * nella realizzazione delle tabelle
 * 
 * @author michele fornelli
 *
 */

public abstract class TableViewController extends BaseController{

	public TableViewController() {}
	
	/**
	 * Metodo che associa la colonna ai dati che deve visualizzare
	 * 
	 * @param colonna della View
	 * @param nome dell'attributo di riferimento per quei dati
	 * @return col: la colonna con le associazioni
	 */
	protected <S,T> TableColumn<S, T> initColumn(TableColumn<S, T> col, String columnName){
		col.setCellValueFactory(new PropertyValueFactory<S, T>(columnName));
		return col;
	}
}