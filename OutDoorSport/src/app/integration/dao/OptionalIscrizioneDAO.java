package app.integration.dao;

import app.integration.daoUtil.DatabaseException;
import app.to.OptionalIscrizioneTO;
import app.to.StatoOptionalTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda gli optional legati ad una iscrizione
 * di un partecipante.
 * Sono presenti i metodi di lettura applicabili.
 * 
 * @author michele fornelli
 *
 */
public interface OptionalIscrizioneDAO extends BaseDAO<OptionalIscrizioneTO> {



}