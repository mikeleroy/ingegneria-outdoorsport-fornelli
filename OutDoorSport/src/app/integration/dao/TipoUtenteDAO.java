package app.integration.dao;

import app.integration.daoUtil.DatabaseException;
import app.to.TipoUtenteTO;
import app.to.UtenteTO;

/**
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda i tipi di
 * utenti. Sono presenti i metodi di lettura applicabili
 * 
 * @author michele fornelli
 *
 * 
 */
public interface TipoUtenteDAO extends BaseDAO<TipoUtenteTO> {

	/**
	 * Restituisce il tipo di utente del manager di sistema
	 * 
	 * @return
	 * @throws DatabaseException
	 */
	TipoUtenteTO getTipoUtenteMds() throws DatabaseException;

	/**
	 * Restituisce il tipo di utente del manager di escursione
	 * 
	 * @return
	 * @throws DatabaseException
	 */
	TipoUtenteTO getTipoUtenteMde() throws DatabaseException;

	/**
	 * Restituisce il tipo di utente del partecipante
	 * 
	 * @return
	 * @throws DatabaseException
	 */
	TipoUtenteTO getTipoUtentePar() throws DatabaseException;

}
