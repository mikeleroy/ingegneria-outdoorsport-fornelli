package app.integration.dao;

import java.util.ArrayList;
import java.util.List;

import app.integration.daoUtil.DatabaseException;
import app.to.EscursioneTO;
import app.to.ManagerDiEscursioneTO;
import app.to.PartecipanteTO;
import app.to.TipoEscursioneTO;

/**
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda le
 * Escursioni. Sono presenti tutti i metodi di scrittura e aggiornamento cos�
 * come anche di lettura di un tipo particolare di escursioni (attive, gestite
 * da un particolare manager o di un tipo particolare). Per escursioni attive
 * s'intende una escursione con lo stato: aperta alle iscrizioni, chiusa o in
 * corso.
 * 
 * @author michele fornelli
 *
 */
public interface EscursioneDAO extends BaseDAO<EscursioneTO> {

	/**
	 * @param id
	 * @return l'entit� con quel determinato id
	 * @throws DatabaseException
	 */
	EscursioneTO getById(Integer id) throws DatabaseException;
	
	/**
	 * @return le Escursioni aperte alle iscrizioni
	 * @throws DatabaseException
	 */
	List<EscursioneTO> getEscursioniAperte() throws DatabaseException;
	
	/**
	 * 
	 * @param escursione
	 * @return restituisce le Escursione escluso quella annullata
	 * @throws DatabaseException
	 */
	EscursioneTO annullaEscursione(EscursioneTO escursione) throws DatabaseException;
	
	/**
	 * @param mde
	 * @return le Escursioni di un determinato Manager di Escursione
	 * @throws DatabaseException
	 */
	List<EscursioneTO> getEscursioniByMDE(ManagerDiEscursioneTO mde) throws DatabaseException;
	
	/**
	 * Verifica se esiste o meno l'escursione che si vuole inserire
	 * 
	 * @return vero se l'escursione esiste, falso altrimenti
	 * @throws DatabaseException 
	 */
	boolean checkEscursione(EscursioneTO escursione)  throws DatabaseException;
	
	/**
	 * @return le escursioni a cui � iscritto il partecipante
	 * @throws DatabaseException 
	 */
	public List<EscursioneTO> getEscursioniPAR(PartecipanteTO partecipante) throws DatabaseException;

}