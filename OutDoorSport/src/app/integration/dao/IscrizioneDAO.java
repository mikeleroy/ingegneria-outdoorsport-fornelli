package app.integration.dao;

import java.util.ArrayList;
import java.util.List;

import app.integration.daoUtil.DatabaseException;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.PartecipanteTO;
import app.to.StatoIscrizioneTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda le iscrizione.
 * Sono presenti i metodi di lettura e modifica applicabili.
 * 
 * @author michele fornelli
 *
 */
public interface IscrizioneDAO extends BaseDAO<IscrizioneTO>{
	

	/**
	 * @param iscrizione
	 * @return l'iscrizione cambiando lo stato in annullata
	 * @throws DatabaseException
	 */
	IscrizioneTO annullaIscrizione(IscrizioneTO iscrizione) throws DatabaseException;
	
	/**
	 * @param iscrizione
	 * @return vero se esiste l'iscrizione, falso altrimenti
	 * @throws DatabaseException
	 */
	boolean checkIscrizione(IscrizioneTO iscrizione) throws DatabaseException;
	
	/**
	 * @return i partecipanti iscritti a una escursione
	 * @throws DatabaseException
	 */
	List<IscrizioneTO> getAllIscrittiFromEscursione(EscursioneTO escursione) throws DatabaseException;
	
	/**
	 * @return l'iscrizione del partecipante a una determinata escursione
	 * @throws DatabaseException
	 */
	IscrizioneTO getIscrizioneFromEscursione(EscursioneTO escursione, PartecipanteTO partecipante) throws DatabaseException;	
}
