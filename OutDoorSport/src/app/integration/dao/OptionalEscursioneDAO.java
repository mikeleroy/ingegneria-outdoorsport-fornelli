package app.integration.dao;

import java.util.List;

import app.integration.daoUtil.DatabaseException;
import app.to.EscursioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalTO;

/** 
 * Interfaccia che rappresenta i Data Access Object per 
 * tutte le operazioni CRUD degli Optional collegati alle Escursioni.
 * 
 * Sono presenti i metodi di lettura e modifica.
 * 
 * @author michele fornelli
 *
 */

public interface OptionalEscursioneDAO extends BaseDAO<OptionalEscursioneTO>{

	/**
	 * @return Optional di una determinata Escursione
	 * @throws DatabaseException
	 */
	public List<OptionalEscursioneTO> getAllOptionalEscursione(int idEscursione) throws DatabaseException;
	
	/**
	 * Metodo che restituisce l'ID dell'associazione Optional-Escursione
	 * 
	 * @param escursione
	 * @param optional
	 * @return ID dell'associazione
	 * @throws DatabaseException
	 */
	public List<OptionalEscursioneTO> getAssociationID(EscursioneTO escursione, OptionalTO optional) throws DatabaseException;
}
