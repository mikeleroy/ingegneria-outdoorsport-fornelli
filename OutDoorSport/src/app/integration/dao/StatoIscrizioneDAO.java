package app.integration.dao;

import app.integration.daoUtil.DatabaseException;
import app.to.StatoIscrizioneTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda gli stati iscrizione.
 * Sono presenti i metodi di lettura applicabili.
 * 
 * @author michele fornelli
 *
 */
public interface StatoIscrizioneDAO extends BaseDAO<StatoIscrizioneTO> {

	/**
	 * Restituisce lo stato attivo di una iscrizione
	 * @return
	 * @throws DatabaseException
	 */
	StatoIscrizioneTO getStatoAttivo() throws DatabaseException;
	
	/**
	 * Restituisce lo stato disattivo di una iscrizione
	 * @return
	 * @throws DatabaseException
	 */
	StatoIscrizioneTO getStatoDisattivo() throws DatabaseException;

	/**
	 * Restituisce lo stato terminato di una iscrizione
	 * @return
	 * @throws DatabaseException
	 */
	StatoIscrizioneTO getStatoIscrizioneTerminato() throws DatabaseException;

}
