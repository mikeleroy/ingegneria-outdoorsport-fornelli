package app.integration.dao;

import app.integration.daoUtil.DatabaseException;
import app.to.EscursioneTO;
import app.to.StatoOptionalTO;


/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda gli stati optional.
 * Sono presenti i metodi di lettura applicabili.
 * 
 * @author michele fornelli
 *
 */
public interface StatoOptionalDAO extends BaseDAO<StatoOptionalTO> {

	/**
	 * Restituisce lo stato optional disattivo
	 * @return
	 * @throws DatabaseException
	 */
	StatoOptionalTO getStatoDisattivo() throws DatabaseException;

	/**
	 * Restituisce lo stato optional attivo
	 * @return
	 * @throws DatabaseException
	 */
	StatoOptionalTO getStatoAttivo() throws DatabaseException;

}