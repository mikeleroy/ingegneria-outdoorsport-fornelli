package app.integration.dao;

import java.util.List;

import app.integration.daoUtil.DatabaseException;
import app.to.ManagerDiEscursioneTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda i manager di escursione.
 * Sono presenti i metodi di lettura applicabili.
 * 
 * @author michele fornelli
 * 
 */
public interface ManagerDiEscursioneDAO extends UtenteDAO<ManagerDiEscursioneTO>{

	/**
	 * Restituisce la lista di tutti i manager di escursione
	 * @return
	 * @throws DatabaseException
	 */
	List<ManagerDiEscursioneTO> getAllManagerDiEscursione() throws DatabaseException;

}