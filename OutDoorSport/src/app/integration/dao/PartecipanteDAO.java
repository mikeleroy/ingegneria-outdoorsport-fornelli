package app.integration.dao;

import java.util.ArrayList;
import java.util.List;

import app.integration.daoUtil.DatabaseException;
import app.to.PartecipanteTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda i partecipanti.
 * Sono presenti i metodi di lettura applicabili.
 * 
 * @author michele fornelli
 *
 */
public interface PartecipanteDAO extends UtenteDAO<PartecipanteTO>{

	/**
	 * Restituisce il partecipante il cui codice fiscale � uquale al cf in input
	 * @param cf
	 * @return
	 * @throws DatabaseException
	 */
	PartecipanteTO readByCF(String cf) throws DatabaseException;

	/**
	 * Restituisce il partecipante il cui username � uquale all'username in input
	 * @param cf
	 * @return
	 * @throws DatabaseException
	 */
	PartecipanteTO readByUsername(String username) throws DatabaseException;

	/**
	 * Restituisce la lista di tutti i partecipanti
	 * @return
	 * @throws DatabaseException
	 */
	List<PartecipanteTO> getAllPartecipante() throws DatabaseException;

}