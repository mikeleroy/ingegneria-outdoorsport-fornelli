package app.integration.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.query.Query;

import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.HibernateUtil;
import app.to.BaseTO;
import app.to.UtenteTO;

/**
 * <b>Integration Tier</b> L'interfaccia rappresenta una generalizzazione del
 * pattern DAO per tutti i DAO presenti all'interno del package
 * In particolare la generalizzazione fa riferimento a tutte le operazioni
 * comuni che � possibile eseguire su un database, ovvero: inserimento,
 * modifica, cancellazione e lettura (CRUD). Sono forniti metodi di supporto per
 * ritrovare una singola entit� oppure tutte le entit� di un particolare
 * tipo.
 * 
 * Inoltre sono forniti anche metodi per l'esecuzione di una query in base al
 * suo nome. Il nome sar� utile in quanto grazie a questo � possibile ottenere
 * la query (presente in una risorsa esterna) su cui effettuare le dovute
 * sostituzioni soprattutto nel caso di query parametriche.
 * 
 * @author michele fornelli
 *
 * @param <T>
 *            Specifica il tipo di entit� da utilizare
 */

public interface BaseDAO<T extends Serializable> {

	/**
	 * Memorizza un'istanza di tipo T nel database
	 * 
	 * @param entity
	 *            L'entit� da memorizzare
	 * @return l'entit� di tipo T
	 * @throws DatabaseException
	 */
	public T create(final T entity) throws DatabaseException;

	
	/**
	 * Inserisce l'entit� di tipo T nel database o,
	 * se esiste, la modifica
	 * 
	 * @param entity
	 * @return l'entit� di tipo T
	 * @throws DatabaseException
	 */
	T createOrUpdate(final T entity) throws DatabaseException;
	
	
	/**
	 * Restituisce una singola istanza di tipo T
	 * 
	 * @param id
	 *            id dell'istanza
	 * @return
	 * @throws DatabaseException
	 */
	public T findOne(final Integer id) throws DatabaseException;

	/**
	 * Ritorna tutte le entit� di tipo T
	 * 
	 * @return Una lista contenente tutte le istanze di tipo T
	 * @throws DatabaseException
	 */
	public List<T> getAll() throws DatabaseException;

	/**
	 * Modifica un'entit� di tipo T
	 * 
	 * @param entity
	 *            L'entit� da modificare
	 * @return
	 * @throws DatabaseException
	 */
	public T update(final T entity) throws DatabaseException;

	/**
	 * Esegue la query identificata da queryName a cui verranno passati i
	 * parametri presenti nella lista parameters.
	 * 
	 * @param queryName
	 *            Nome della query
	 * @param parameters
	 *            Parametri da utilizzare nella query
	 * @return Una lista di entit� risultato della query.
	 * 
	 * @throws DatabaseException
	 */
	<P> List<T> executeParamQuery(String queryName, List<P> parameters) throws DatabaseException;

	/**
	 * Esegue la query identificata da queryName che rappresenta una query non
	 * parametrica.
	 * 
	 * @param queryName
	 *            Nome della query
	 * @return Una lista di entit� risultato della query.
	 * @throws DatabaseException
	 */
	List<T> executeQuery(String queryName) throws DatabaseException;

}