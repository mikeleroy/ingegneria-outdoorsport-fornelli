package app.integration.dao;

import java.util.List;

import app.integration.daoUtil.DatabaseException;
import app.to.StatoEscursioneTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda gli stati escursioni.
 * Sono presenti i metodi di lettura applicabili.
 * 
 * @author michele fornelli
 *
 */
public interface StatoEscursioneDAO extends BaseDAO<StatoEscursioneTO>{

	/**
	 * Restituisce lo stato escursione annullata
	 * @return
	 * @throws DatabaseException
	 */
	StatoEscursioneTO getStatoEscursioneAnnullata() throws DatabaseException;

	/**
	 * Restituisce lo stato escursione in corso
	 * @return
	 * @throws DatabaseException
	 */
	StatoEscursioneTO getStatoEscursioneInCorso() throws DatabaseException;

	/**
	 * Restituisce lo stato escursione aperta alle iscrizioni
	 * @return
	 * @throws DatabaseException
	 */
	StatoEscursioneTO getStatoEscursioneAperta() throws DatabaseException;

	/**
	 * Restituisce lo stato escursione chiusa alle iscrizioni
	 * @return
	 * @throws DatabaseException
	 */
	StatoEscursioneTO getStatoEscursioneChiusa() throws DatabaseException;

	/**
	 * Restituisce lo stato escursione terminata
	 * @return
	 * @throws DatabaseException
	 */
	StatoEscursioneTO getStatoEscursioneTerminata() throws DatabaseException;

}
