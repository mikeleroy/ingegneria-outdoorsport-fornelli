package app.integration.dao;

import java.util.List;

import app.integration.daoUtil.DatabaseException;
import app.to.ManagerDiSistemaTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda il manager di sistema.
 * Sono presenti i metodi di lettura.
 * 
 * @author michele fornelli
 *
 */
public interface ManagerDiSistemaDAO extends UtenteDAO<ManagerDiSistemaTO>{
	
	/**
	 * 
	 * @return vero se esiste nella sorgente dati il manager di sistema, falso altrimenti
	 * @throws DatabaseException
	 */
	public boolean checkMds() throws DatabaseException;
	
}