package app.integration.dao;

import app.integration.daoUtil.DatabaseException;
import app.to.StatoUtenteTO;

/** 
 * Data Access Object per tutte le operazioni CRUD per quanto riguarda gli stati utente.
 * Sono presenti i metodi di lettura applicabili.
 * 
 * @author michele fornelli
 *
 */
public interface StatoUtenteDAO extends BaseDAO<StatoUtenteTO> {
	
	/**
	 * Restituisce lo stato utente attivo
	 * @return
	 * @throws DatabaseException
	 */
	StatoUtenteTO getStatoAttivo() throws DatabaseException;
	
	/**
	 * Restituisce lo stato utente disattivo
	 * @return
	 * @throws DatabaseException
	 */	
	StatoUtenteTO getStatoDisattivo() throws DatabaseException;
}
