package app.integration.dao.hib;

import app.integration.dao.TipoUtenteDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.TipoUtenteTO;

class HibTipoUtenteDAO extends HibBaseDAO<TipoUtenteTO> implements TipoUtenteDAO {

	public HibTipoUtenteDAO() {
		this.setClasse(toFact.getFactoryTO(EnumTO.TipoUtente));
	}

	@Override
	public TipoUtenteTO getTipoUtenteMds() throws DatabaseException {
		return this.findOne(0);
	}

	@Override
	public TipoUtenteTO getTipoUtenteMde() throws DatabaseException {
		return this.findOne(1);
	}

	@Override
	public TipoUtenteTO getTipoUtentePar() throws DatabaseException {
		return this.findOne(2);
	}

}
