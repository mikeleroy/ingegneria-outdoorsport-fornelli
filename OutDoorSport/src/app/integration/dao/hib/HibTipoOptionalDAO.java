package app.integration.dao.hib;

import app.integration.dao.TipoOptionalDAO;
import app.to.EnumTO;
import app.to.TipoOptionalTO;

class HibTipoOptionalDAO extends HibBaseDAO<TipoOptionalTO> implements TipoOptionalDAO {

	HibTipoOptionalDAO(){
		this.setClasse(toFact.getFactoryTO(EnumTO.TipoOptional));
	}

}