package app.integration.dao.hib;

import java.util.ArrayList;
import java.util.List;

import app.integration.dao.PartecipanteDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.PartecipanteTO;

class HibPartecipanteDAO extends HibUtenteDAO<PartecipanteTO> implements PartecipanteDAO {

	HibPartecipanteDAO() {
		this.setClasse(toFact.getFactoryTO(EnumTO.Partecipante));
	}

	@Override
	public PartecipanteTO readByCF(String cf) throws DatabaseException {

		List<String> param = new ArrayList<String>();
		param.add(cf);

		List<PartecipanteTO> list = super.executeParamQuery("getUtenteByCF", param);
		PartecipanteTO res = (PartecipanteTO)list.get(0);

		return res;
	}

	@Override
	public PartecipanteTO readByUsername(String username)
			throws DatabaseException {
		List<String> param = new ArrayList<String>();
		param.add(username);

		List<PartecipanteTO> list = super.executeParamQuery("getUtenteByUsername", param);
		PartecipanteTO res = (PartecipanteTO)list.get(0);

		return res;
	}

	@Override
	public List<PartecipanteTO> getAllPartecipante() throws DatabaseException {
		return super.executeQuery("getAllPartecipante");
	}


}