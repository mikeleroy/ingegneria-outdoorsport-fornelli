package app.integration.dao.hib;

import app.integration.dao.TipoEscursioneDAO;
import app.to.EnumTO;
import app.to.TipoEscursioneTO;

class HibTipoEscursioneDAO extends HibBaseDAO<TipoEscursioneTO> implements TipoEscursioneDAO {

	HibTipoEscursioneDAO(){
		this.setClasse(toFact.getFactoryTO(EnumTO.TipoEscursione));
	}

}