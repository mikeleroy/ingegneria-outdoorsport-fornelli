package app.integration.dao.hib;

import java.util.ArrayList;
import java.util.List;

import app.integration.dao.UtenteDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.BaseTO;
import app.to.EnumTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;

class HibUtenteDAO<T extends UtenteTO> extends HibBaseDAO<T> implements UtenteDAO<T> {

	private UtenteTO utente = null;
	
	/**
	 * Il costruttore inizializza l'entit� Utente da utilizzare 
	 * in tutte le operazioni del DAO.
	 */
	public HibUtenteDAO() {
		if(utente == null){
			utente = (UtenteTO) toFact.getFactoryTO(EnumTO.Utente);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T getUtente(UtenteTO uto) throws DatabaseException {
		UtenteTO res = null;

		List<String> param = new ArrayList<String>();
		param.add(uto.getEmail());
		param.add(uto.getPassword());

		List<T> list = this.executeParamQuery("getUtente", param);

		if (list.size() == 1) {
			res = list.get(0);
		} else {
			res = (UtenteTO) toFact.getFactoryTO(EnumTO.Utente);
		}
		return (T) res;
	}

	@SuppressWarnings("unchecked")
	private T getUtenteBy(String queryName, List<?> params) throws DatabaseException {

		UtenteTO res = null;
		List<T> list = super.executeParamQuery(queryName, params);

		if (list.size() == 0) {
			res = (UtenteTO) toFact.getFactoryTO(EnumTO.Utente);
		} else {
			res = (UtenteTO) list.get(0);
		}

		return (T) res;
	}

	@Override
	public T getByUsername(String username) throws DatabaseException {

		List<String> param = new ArrayList<String>();
		param.add(username);

		return this.getUtenteBy("getByUsername", param);
	}

	@Override
	public T getByEmail(String email) throws DatabaseException {

		List<String> param = new ArrayList<String>();
		param.add(email);

		return this.getUtenteBy("getByEmail", param);
	}

	@Override
	public T getByID(Integer id) throws DatabaseException {

		List<Integer> param = new ArrayList<Integer>();
		param.add(id);

		return getUtenteBy("getByID", param);
	}

	/**
	 * @param utente
	 * @return vero se � un utente nullo, falso altrimenti
	 */
	private boolean isNullUtente(UtenteTO uto){
		return uto.getIdUtente() == null;
	}

	@Override
	public boolean esisteUsername(UtenteTO uto) throws DatabaseException {
		boolean res = false;

		List<String> param = new ArrayList<String>();
		param.add(uto.getUsername());

		UtenteTO user = this.getUtenteBy("getByUsername", param);

		res = !this.isNullUtente(user);

		return res;
	}

	@Override
	public boolean esisteEmail(UtenteTO uto) throws DatabaseException {
		boolean res = false;

		List<String> param = new ArrayList<String>();
		param.add(uto.getEmail());

		UtenteTO checkUser = (UtenteTO) this.getUtenteBy("getByEmail", param);

		res = !this.isNullUtente(checkUser);

		return res;
	}
	
	

}