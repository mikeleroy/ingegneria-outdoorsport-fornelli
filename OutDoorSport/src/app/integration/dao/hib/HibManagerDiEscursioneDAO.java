package app.integration.dao.hib;

import java.util.List;

import app.integration.dao.ManagerDiEscursioneDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.ManagerDiEscursioneTO;

class HibManagerDiEscursioneDAO extends HibUtenteDAO<ManagerDiEscursioneTO> implements ManagerDiEscursioneDAO{

	HibManagerDiEscursioneDAO(){
		this.setClasse(toFact.getFactoryTO(EnumTO.ManagerDiEscursione));
	}

	@Override
	public List< ManagerDiEscursioneTO > getAll() throws DatabaseException{
		List<ManagerDiEscursioneTO> res = super.getAll();

		return res;
	}

	@Override
	public List<ManagerDiEscursioneTO> getAllManagerDiEscursione() throws DatabaseException {
		return super.executeQuery("getAllManagerDiEscursione");
	}

}