package app.integration.dao.hib;

import java.util.ArrayList;
import java.util.List;

import app.integration.dao.OptionalEscursioneDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EscursioneTO;
import app.to.OptionalEscursioneTO;
import app.to.OptionalTO;

/** 
 * Classe che implementa i Data Access Object per 
 * tutte le operazioni CRUD per gli Optional collegati alle escursioni.
 * 
 * @author michele fornelli
 *  
 */

class HibOptionalEscursioneDAO extends HibBaseDAO<OptionalEscursioneTO> implements OptionalEscursioneDAO{

	public HibOptionalEscursioneDAO() {}

	@Override
	public List<OptionalEscursioneTO> getAllOptionalEscursione(int idEscursione) throws DatabaseException {
		List<Integer> param = new ArrayList<Integer>();
		param.add(idEscursione);
		List<OptionalEscursioneTO> list = super.executeParamQuery("getAllOptionalEscursione", param);
		return list;
	}

	@Override
	public List<OptionalEscursioneTO> getAssociationID(EscursioneTO escursione, OptionalTO optional) throws DatabaseException {
		List<Integer> param = new ArrayList<>();
		param.add(escursione.getIdEscursione());
		param.add(optional.getIdOptional());
		List<OptionalEscursioneTO> list = super.executeParamQuery("getAssociationID", param);
		return list;
	}

}
