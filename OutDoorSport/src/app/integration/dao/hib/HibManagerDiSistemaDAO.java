package app.integration.dao.hib;

import java.util.List;

import app.integration.dao.ManagerDiSistemaDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.ManagerDiSistemaTO;

class HibManagerDiSistemaDAO extends HibUtenteDAO<ManagerDiSistemaTO> implements ManagerDiSistemaDAO{

	HibManagerDiSistemaDAO(){
		this.setClasse(toFact.getFactoryTO(EnumTO.ManagerDiSistema));
	}

	@Override
	public boolean checkMds() throws DatabaseException {
		boolean res;
		List<ManagerDiSistemaTO> list = super.executeQuery("checkMds");

		if(list.size() != 0){
			res = true;
		}else{
			res = false;
		}

		return res;
	}
}