package app.integration.dao.hib;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.query.Query;

import app.integration.dao.BaseDAO;
import app.integration.daoUtil.DatabaseException;
import app.integration.daoUtil.HibernateUtil;
import app.to.BaseTO;
import app.to.UtenteTO;
import app.to.impl.FactoryTO;

public abstract class HibBaseDAO<T extends BaseTO> implements BaseDAO<T> {

	private Session session = null;
	private Transaction tx = null;
	private BaseTO classeTO;
	protected static FactoryTO toFact;

	/**
	 * metodo che aggiorna la classe corrente(classeTO) che vogliamo utilizzare
	 * 
	 * @param classeTO
	 */
	protected void setClasse(final BaseTO classeTO) {
		this.classeTO = classeTO;
	}

	/**
	 * @return la sessione creata nella classe SessionUtil
	 * @throws DatabaseException
	 * @throws HibernateException
	 */
	private Session getSession() {
		return HibernateUtil.getSessionFactory().openSession();
	}

	private void openSession() throws HibernateException, DatabaseException {
		session = null;
		tx = null;
		try {
			session = this.getSession();
			tx = session.beginTransaction();
		} catch (JDBCConnectionException e) {
			tx.rollback();
		}
	}

	private void closeSession() {
		session.close();
	}

	/*
	 * private Session getSession(){ return
	 * SessionUtil.getSessionFactory().openSession(); }
	 */


	@Override
	public T create(final T entity) throws DatabaseException {

		openSession();
		try {
			session.saveOrUpdate(entity);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			closeSession();
		}

		return entity;
	}
	
	
	@Override
	public T createOrUpdate(final T entity) throws DatabaseException{
		Transaction transaction = null;
		Session session = null;

		try {
			session = this.getSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(entity);
			transaction.commit();
		} catch (Exception e){
			if (transaction != null){
				transaction.rollback();
			}
			throw new DatabaseException(e.getMessage(), e);
		}finally{
			session.close();
		}

		return entity;
	}
	
	
	


	@Override
	public T findOne(Integer id) throws DatabaseException {
		openSession();
		T result = null;
		try {

			result = (T) session.get(this.classeTO.getClass().getName(), id);
			tx.commit();

		} catch (HibernateException e) {
			tx.rollback();
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			session.close();
		}

		return result;
	}


	@Override
	public List<T> getAll() throws DatabaseException {
		openSession();
		List<T> query = null;
		try {

			query = session.createQuery("FROM " + this.classeTO.getClass().getName()).list();

			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			session.close();
		}

		return query;
	}

	
	@Override
	public T update(final T entity) throws DatabaseException {
		openSession();
		try {
			session.update(entity);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			closeSession();
		}

		return entity;
	}


	@Override
	public <P> List<T> executeParamQuery(String queryName, List<P> parameters) throws DatabaseException {

		List<T> result = null;

		try {
			openSession();

			Query query = session.getNamedQuery(queryName);
			int index = 0;

			for (Object param : parameters) {
				query.setParameter(index, param);
				index++;
			}

			result = query.list();
			tx.commit();

		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			closeSession();
		}

		return result;
	}

	
	@Override
	public List<T> executeQuery(String queryName) throws DatabaseException {
		openSession();
		List<T> result = null;

		try {

			// Query query = session.getNamedQuery(queryName);
			Query query = session.createNamedQuery(queryName);

			result = query.list();

			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			closeSession();
		}

		return result;
	}
}