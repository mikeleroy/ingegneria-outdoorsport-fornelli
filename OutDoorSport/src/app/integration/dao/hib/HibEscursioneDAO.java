package app.integration.dao.hib;

import java.util.ArrayList;
import java.util.List;

import app.integration.dao.EscursioneDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.ManagerDiEscursioneTO;
import app.to.PartecipanteTO;
import app.to.TipoEscursioneTO;
import app.to.UtenteTO;

class HibEscursioneDAO extends HibBaseDAO<EscursioneTO> implements EscursioneDAO {

	private EscursioneTO escursione = null;
	/**
	 * Il costruttore inizializza l'entit� Escursione da utilizzare 
	 * in tutte le operazioni del DAO.
	 */
	public HibEscursioneDAO() {
		if(escursione == null){
			escursione = (EscursioneTO) toFact.getFactoryTO(EnumTO.Escursione);
			this.setClasse(toFact.getFactoryTO(EnumTO.Escursione));
		}
	}
	
	@Override
	public EscursioneTO getById(Integer id) throws DatabaseException{
		EscursioneTO response = super.findOne(id);
		return response;
	}
	
	@Override
	public List<EscursioneTO> getEscursioniPAR(PartecipanteTO partecipante) throws DatabaseException{
		List<Integer> param = new ArrayList<Integer>();
		param.add(partecipante.getIdUtente());
		return super.executeParamQuery("getEscursioniPAR", param);
	}
	
	@Override
	public List<EscursioneTO> getEscursioniAperte() throws DatabaseException{
		return super.executeQuery("getEscursioniAperte");
	}
	
	@Override
	public EscursioneTO annullaEscursione(EscursioneTO escursione)
			throws DatabaseException {
		return super.update(escursione);	
	}

	@Override
	public List<EscursioneTO> getEscursioniByMDE(ManagerDiEscursioneTO mde) throws DatabaseException {
		List<Integer> param = new ArrayList<Integer>();
		param.add(mde.getIdUtente());
		List<EscursioneTO> response = super.executeParamQuery("getEscursioniByMDE", param);
		return response;
	}
	
	@Override
	public boolean checkEscursione(EscursioneTO escursione) throws DatabaseException{
		boolean response = false;
		List<String> param = new ArrayList<String>();
		param.add(escursione.getNome());
		EscursioneTO newEscursione = (EscursioneTO) this.getEscursioneByQuery("getEscursioneByName", param);
		response = !this.isNullEscursione(newEscursione);
		return response;
	}
	
	/**
	 * @param escursione
	 * @return vero se � una escursione � nulla, falso altrimenti
	 */
	private boolean isNullEscursione(EscursioneTO escursione){
		if(escursione.getNome() == null)
			return true;
		else
			return false;
	}

	/**
	 * @param queryName
	 * @param params
	 * @return un'istanza di Escursione tramite query
	 * @throws DatabaseException
	 */
	@SuppressWarnings({ })
	private EscursioneTO getEscursioneByQuery(String queryName, List<?> params) throws DatabaseException{
		EscursioneTO response = null;
		List<EscursioneTO> list = super.executeParamQuery(queryName, params);
		if(list.size() == 0){
			response = escursione;
		} else {
			response = (EscursioneTO)list.get(0);
		}
		return response;
	}

}
