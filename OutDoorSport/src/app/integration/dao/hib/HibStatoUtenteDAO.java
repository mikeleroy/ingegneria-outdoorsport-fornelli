package app.integration.dao.hib;

import app.integration.dao.StatoUtenteDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.StatoUtenteTO;

class HibStatoUtenteDAO extends HibBaseDAO<StatoUtenteTO> implements StatoUtenteDAO {

	HibStatoUtenteDAO() {
		this.setClasse(toFact.getFactoryTO(EnumTO.StatoUtente));
	}

	@Override
	public StatoUtenteTO getStatoAttivo() throws DatabaseException {
		return this.findOne(1);
	}

	@Override
	public StatoUtenteTO getStatoDisattivo() throws DatabaseException {
		return this.findOne(0);
	}

}