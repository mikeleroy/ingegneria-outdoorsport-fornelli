package app.integration.dao.hib;

import app.integration.dao.BaseDAO;
import app.integration.daoUtil.EnumDAO;

public class HibFactoryDAO{


	public static BaseDAO getFactoryDAO(EnumDAO type) {
		switch (type) {
		case EscursioneDAO:
			return new HibEscursioneDAO();
		case IscrizioneDAO:
			return new HibIscrizioneDAO();
		case ManagerDiEscursioneDAO:
			return new HibManagerDiEscursioneDAO();
		case ManagerDiSistemaDAO:
			return new HibManagerDiSistemaDAO();
		case OptionalDAO:
			return new HibOptionalDAO();
		case PartecipanteDAO:
			return new HibPartecipanteDAO();
		case StatoEscursioneDAO:
			return new HibStatoEscursioneDAO();
		case StatoIscrizioneDAO:
			return new HibStatoIscrizioneDAO();
		case StatoOptionalDAO:
			return new HibStatoOptionalDAO();
		case StatoUtenteDAO:
			return new HibStatoUtenteDAO();
		case TipoEscursioneDAO:
			return new HibTipoEscursioneDAO();
		case TipoOptionalDAO:
			return new HibTipoOptionalDAO();
		case TipoUtenteDAO:
			return new HibTipoUtenteDAO();
		case UtenteDAO:
			return new HibUtenteDAO<>();
		case OptionalEscursioneDAO:
			return new HibOptionalEscursioneDAO();
		case OptionalIscrizioneDAO:
			return new HibOptionalIscrizioneDAO();
		default:
			return null;
		}
	}



}

