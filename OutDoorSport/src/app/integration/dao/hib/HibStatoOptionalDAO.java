package app.integration.dao.hib;

import app.integration.dao.StatoOptionalDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.StatoOptionalTO;

class HibStatoOptionalDAO extends HibBaseDAO<StatoOptionalTO> implements StatoOptionalDAO {

	HibStatoOptionalDAO() {
		this.setClasse(toFact.getFactoryTO(EnumTO.StatoOptional));
	}

	@Override
	public StatoOptionalTO getStatoDisattivo() throws DatabaseException {
		return this.findOne(0);
	}

	@Override
	public StatoOptionalTO getStatoAttivo() throws DatabaseException {
		return this.findOne(0);
	}

}
