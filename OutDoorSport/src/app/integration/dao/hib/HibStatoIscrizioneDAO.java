package app.integration.dao.hib;

import app.integration.dao.StatoIscrizioneDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.StatoIscrizioneTO;

class HibStatoIscrizioneDAO extends HibBaseDAO<StatoIscrizioneTO> implements StatoIscrizioneDAO{

	HibStatoIscrizioneDAO() {
		this.setClasse(toFact.getFactoryTO(EnumTO.StatoIscrizione));
	}

	@Override
	public StatoIscrizioneTO getStatoAttivo() throws DatabaseException {
		return this.findOne(1);
	}

	@Override
	public StatoIscrizioneTO getStatoDisattivo() throws DatabaseException {
		return this.findOne(0);
	}
	
	@Override
	public StatoIscrizioneTO getStatoIscrizioneTerminato() throws DatabaseException {
		return this.findOne(2);
	}

}