package app.integration.dao.hib;

import java.util.ArrayList;
import java.util.List;

import app.integration.dao.IscrizioneDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.EscursioneTO;
import app.to.IscrizioneTO;
import app.to.PartecipanteTO;
import app.to.StatoIscrizioneTO;
import app.to.impl.FactoryTO;

/** 
 * Classe che implementa i Data Access Object per 
 * tutte le operazioni CRUD per Iscrizione.
 * 
 * @author michele fornelli
 *
 */

class HibIscrizioneDAO extends HibBaseDAO<IscrizioneTO> implements IscrizioneDAO{

	private IscrizioneTO iscrizione;

	/**
	 * Il costruttore inizializza l'entit� Iscrizione da utilizzare 
	 * in tutte le operazioni del DAO.
	 */
	
	public HibIscrizioneDAO() {
		if(iscrizione == null)
		this.setClasse(toFact.getFactoryTO(EnumTO.Iscrizione));
		}
	
	/**
	 * @return la lista di tutte le iscrizioni
	 * @throws DatabaseException
	 */
	@Override
	public List<IscrizioneTO> getAll() throws DatabaseException{
		List<IscrizioneTO> response = super.getAll();
		return response;
	}
	
	@Override
	public IscrizioneTO annullaIscrizione(IscrizioneTO iscrizione) throws DatabaseException {
		super.update(iscrizione);
		return iscrizione;
	}
	
	@Override
	public boolean checkIscrizione(IscrizioneTO iscrizione) throws DatabaseException {
		List<Integer> param = new ArrayList<>();
		param.add(iscrizione.getEscursione().getIdEscursione());
		param.add(iscrizione.getUtente().getIdUtente());
		List<IscrizioneTO> newIscrizione = super.executeParamQuery("checkIscrizione", param);
		if(newIscrizione.isEmpty())
			return false;
		else
			return true;
	}
	
	@Override
	public List<IscrizioneTO> getAllIscrittiFromEscursione(EscursioneTO escursione) throws DatabaseException {
		List<Integer> param = new ArrayList<>();
		param.add(escursione.getIdEscursione());
		List<IscrizioneTO> res = super.executeParamQuery("getAllIscrittiFromEscursione", param);
		return res;
	}

	@Override
	public IscrizioneTO getIscrizioneFromEscursione(EscursioneTO escursione, PartecipanteTO partecipante) throws DatabaseException {
		List<Integer> param = new ArrayList<>();
		param.add(escursione.getIdEscursione());
		param.add(partecipante.getIdUtente());
		List<IscrizioneTO> res = super.executeParamQuery("getIscrizioneFromEscursione", param);
		if(!res.isEmpty())
			return res.get(0);
		else
			return null;
	}

}