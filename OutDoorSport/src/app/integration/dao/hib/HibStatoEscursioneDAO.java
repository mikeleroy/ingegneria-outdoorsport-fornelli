package app.integration.dao.hib;

import java.util.List;

import app.integration.dao.StatoEscursioneDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.StatoEscursioneTO;

class HibStatoEscursioneDAO extends HibBaseDAO<StatoEscursioneTO> implements StatoEscursioneDAO {

	HibStatoEscursioneDAO() {
		this.setClasse(toFact.getFactoryTO(EnumTO.StatoEscursione));
	}

	@Override
	public StatoEscursioneTO getStatoEscursioneAnnullata() throws DatabaseException {
		List<StatoEscursioneTO> stato0 = super.executeQuery("getStatoCmpAnnullata");

		return stato0.get(0);
	}
	@Override
	public StatoEscursioneTO getStatoEscursioneAperta() throws DatabaseException {
		List<StatoEscursioneTO> stato1 = super.executeQuery("getStatoEscursioneAperta");

		return stato1.get(0);
	}

	@Override
	public StatoEscursioneTO getStatoEscursioneInCorso() throws DatabaseException {
		List<StatoEscursioneTO> stato2 = super.executeQuery("getStatoEscursioneInCorso");

		return stato2.get(0);
	}
	@Override
	public StatoEscursioneTO getStatoEscursioneChiusa() throws DatabaseException {
		List<StatoEscursioneTO> stato3 = super.executeQuery("getStatoEscursioneChiusa");

		return stato3.get(0);
	}
	@Override
	public StatoEscursioneTO getStatoEscursioneTerminata() throws DatabaseException {
		List<StatoEscursioneTO> stato4 = super.executeQuery("getStatoEscursioneTerminata");

		return stato4.get(0);
	}
}
