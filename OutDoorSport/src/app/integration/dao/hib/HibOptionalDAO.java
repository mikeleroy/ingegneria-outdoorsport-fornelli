package app.integration.dao.hib;

import java.util.ArrayList;
import java.util.List;

import app.integration.dao.OptionalDAO;
import app.integration.daoUtil.DatabaseException;
import app.to.EnumTO;
import app.to.OptionalTO;
import app.to.TipoOptionalTO;

class HibOptionalDAO extends HibBaseDAO<OptionalTO> implements OptionalDAO {

	HibOptionalDAO(){
		this.setClasse(toFact.getFactoryTO(EnumTO.Optional));
	}

	@Override
	public OptionalTO disattivaOptional(OptionalTO optTO) throws DatabaseException {
		return super.update(optTO);
	}

	@Override
	public List<OptionalTO> getOptionalAttiviByTipo(TipoOptionalTO toptTO) throws DatabaseException {
		List<String> param = new ArrayList<String>();
		param.add(toptTO.getNome());

		List<OptionalTO> res = super.executeParamQuery("getOptionalAttiviByTipo", param);


		return res;
	}

	@Override
	public List<OptionalTO> getOptionalByTipo(TipoOptionalTO toptTO) throws DatabaseException {
		List<String> param = new ArrayList<String>();
		param.add(toptTO.getNome());

		List<OptionalTO> res = super.executeParamQuery("getOptionalByTipo", param);


		return res;
	}
}