package app.integration.daoUtil;

/**
 * Classe che gestisce le eccezioni del database
 * 
 * @author michele fornelli
 *
 */

public class DatabaseException extends Exception {
	
	private static final long serialVersionUID = 1481903123234391875L;

	public DatabaseException(){
		super();
	}
	
	public DatabaseException(String message) {
		super(message);
	}

	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public DatabaseException(Throwable cause) {
		super(cause);
	}
}