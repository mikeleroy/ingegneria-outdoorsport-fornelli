package app.integration.daoUtil;

/**
 * enum che contiene le chiavi della richiesta del DAO
 * 
 * @author michele fornelli
 *
 */

public enum EnumDAO {

	ManagerDiEscursioneDAO, ManagerDiSistemaDAO, EscursioneDAO,  PartecipanteDAO,
	TipoEscursioneDAO, TipoOptionalDAO, OptionalDAO, UtenteDAO, IscrizioneDAO,
	StatoEscursioneDAO, StatoIscrizioneDAO, StatoOptionalDAO, StatoUtenteDAO,
	TipoUtenteDAO,OptionalEscursioneDAO,OptionalIscrizioneDAO
}