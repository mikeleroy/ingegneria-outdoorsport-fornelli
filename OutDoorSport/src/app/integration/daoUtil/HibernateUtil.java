package app.integration.daoUtil;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Classe che inizializza e configura la sessione per interagire con il database
 * 
 * @author michele fornelli
 *
 */

public class HibernateUtil {

	private static SessionFactory sessionFactory = buildSessionFactory();

	public static SessionFactory buildSessionFactory() {
		SessionFactory res = null;

		if (sessionFactory == null) {
			Configuration configuration = new Configuration();
			configuration.configure("/resources/config/hibernate.cfg.xml");
			// ServiceRegistry serviceRegistry = new
			// StandardServiceRegistryBuilder().applySettings(
			// configuration.getProperties()).build();
			try {
				// sessionFactory =
				// configuration.buildSessionFactory(serviceRegistry);
				sessionFactory = configuration.buildSessionFactory();

				res = sessionFactory;
			} catch (Exception e) {
				System.out.println(e.getMessage());

				try {
					throw new DatabaseException(e.getMessage());
				} catch (DatabaseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.out.println(e1.getMessage());

				}
			}
		} else {
			res = sessionFactory;
		}

		return res;

	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}