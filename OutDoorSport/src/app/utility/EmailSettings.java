package app.utility;

import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import app.to.EmailTO;
import app.to.UtenteTO;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 * Classe che permette di configurare l'email
 * 
 * @author michele fornelli
 *
 */

public class EmailSettings {
	/**
	 * Costruttore della classe EmailConfig
	 */
	public EmailSettings() {}

	/** 
	 * @return props: Restituisce le proprietÓ dell'email.
	 */
	private Properties setEmailProperties(){
		Properties props = new Properties();

		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.port", 587);

		return props;
	}

	/**
	 * Metodo che viene utilizzato per mandare l'email, notificando l'avvenuto invio, in caso contrario mostra un
	 * messaggio di errore.
	 * @param email: Email in ingresso da mandare
	 */
	public void sendEmail(EmailTO email) {
		ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
		emailExecutor.execute(new Runnable(){

			@Override
			public void run() {
				final String username = "outdoorsportfornelli@gmail.com";
				final String password = "fornelli";

				Properties props = setEmailProperties();

				Session session = Session.getInstance(props, new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

				try {
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(username));


					message.setSubject(email.getOggetto());
					message.setText(email.getMessaggio());

					for(UtenteTO utente: email.getDestinatari()){
						message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(utente.getEmail()));
						Transport.send(message);
					}
					
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							Alert alert = new Alert(AlertType.INFORMATION, "Email di servizio inviata con successo!", ButtonType.OK);
							alert.setTitle("OutDoorSport 1.0");
							alert.setHeaderText("Email Service");
							alert.showAndWait();
						}
					});
					
				} catch (MessagingException e) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							Alert alert = new Alert(AlertType.ERROR, "Invio dell'email di servizio fallita.", ButtonType.OK);
							alert.setTitle("OutDoorSport 1.0");
							alert.setHeaderText("Email Service");
							alert.showAndWait();
						}
					});
				}
				
			}});
		emailExecutor.shutdown();
	}
}
