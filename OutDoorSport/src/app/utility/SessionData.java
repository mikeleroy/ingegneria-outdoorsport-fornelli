package app.utility;

import java.util.HashMap;

import app.presentation.request.OutdoorRequest;
import javafx.scene.layout.AnchorPane;

/**
 * Classe astratta che rappresenta il contenitore per i dati di sessione dell'utente che 
 * si autentica al sistema
 * 
 * @author michele fornelli
 *
 */
public abstract class SessionData{

	/**
	 * Istanza hashmap che tiene traccia dei dati attuali.
	 */
	private static HashMap<String, Object> sessionData = new HashMap<>();

	/**
	 * Metodo che serve per settare i dati di sessione in base alla chiave in ingresso
	 * @param key: chiave in ingresso.
	 */
	protected void setSessionData(OutdoorRequest key){
		if(key.getData() != null){
			if(sessionData.containsKey(key.getData().getClass().getSimpleName()))
				sessionData.replace(key.getData().getClass().getSimpleName(), key.getData());
			else
				sessionData.put(key.getData().getClass().getSimpleName(), key.getData());
		}
	}


	/**
	 * Restituisce il dato della sessione corrente in base 
	 * alla chiave 
	 * 
	 * @param key: chiave del valore
	 * @return il dato rispetto alla chiave
	 */
	public static Object getSessionData(String key){
		Object obj = sessionData.get(key);
		return obj;
	}
	

	/**
	 * Verifica se seiste o meno un dato con una 
	 * determinata chiave
	 * 
	 * @param key: chiave dell'hashmap
	 * @return vero se esiste, falso altrimenti
	 */
	public static boolean existSessionData(String key){
		return sessionData.containsKey(key);
	}
	
	/**
	 * Metodo che resetta la sessione
	 */
	protected void resetSessionData(){
		sessionData.clear();
	}
	
	
	
}
