package app.utility;

/**
 * Interfaccia che contiene tutte le costanti chiavi-valore che servono come Regole o parole-chiave 
 * per la realizzazione del progetto 
 * 
 * @author michele fornelli
 *
 */


public interface Rules {
	public static final String TYPE_PAR ="Partecipante";
	public static final String TYPE_MDE ="ManagerDiEscursione";
	public static final String TYPE_MDS ="ManagerDiSistema";
	public static final String MALE ="M";
	public static final String FEMALE = "F";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILED = "FAILED";
	public static final String REGEX = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
	public static final String FOLDER_CERTIFICATE = "certificatiSRC";

}
