package app.utility;

/**
 * Interfaccia che contiene tutte le costanti chiavi-valore per eseguire le
 * azioni che il front controller pu� trasferire al livello di Business
 * 
 * @author michele fornelli
 *
 */

public interface Actions {
	// utente
	public static final String ACTION_LOGIN = "actionLogin";
	public static final String ACTION_NUOVO_PARTECIPANTE = "nuovoPartecipante";
	public static final String ACTION_CARICA_CERTIFICATO_SRC = "caricaCertificatoSRC";
	public static final String ACTION_MODIFICA_ACCESSO_UTENTE = "modificaAccessoUtente";
	public static final String ACTION_RECUPERA_PASSWORD = "recuperaPassword";
	public static final String ACTION_NUOVA_PASSWORD = "nuovaPassword";

	// partecipante
	public static final String ACTION_PAR_MODIFICA = "modificaPartecipante";
	public static final String ACTION_GET_ALL_ESCURSIONI_APERTE = "getAllEscursioniAperte";
	public static final String ACTION_CHECK_ISCRIZIONE = "checkIscrizione";
	public static final String ACTION_GET_ALL_ESCURSIONI_PAR = "getAllEscursioniPAR";
	public static final String ACTION_GET_ISCRIZIONE_FROM_ESCURSIONE = "getIscrizioneFromEscursione";
	public static final String ACTION_DELETE_ISCRIZIONE_FROM_ESCURSIONE_PAR = "deleteIscrizioneFromEscursione";
	public static final String ACTION_UPDATE_OPTIONAL_FROM_ISCRIZIONE = "updateOptionalFromIscrizione";
	public static final String ACTION_CREATE_OPTIONAL_FROM_ISCRIZIONE = "createOptionalFromIscrizione";

	// manager escursione
	public static final String ACTION_SALVA_ESCURSIONE = "nuovaEscursione";
	public static final String ACTION_GET_ALL_OPTIONAL = "getAllOptionals";
	public static final String ACTION_GET_ALL_TIPI_ESCURSIONE = "getAllTipiEscursione";
	public static final String ACTION_GET_ALL_ESCURSIONI_MDE = "getAllEscursioniMDE";
	public static final String ACTION_GET_ALL_STATO_OPTIONAL = "getAllStatoOptional";
	public static final String ACTION_UPDATE_ESCURSIONE = "modificaEscursione";
	public static final String ACTION_GET_ALL_OPTIONALS = "getAllOptionals";
	public static final String ACTION_GET_ALL_OPTIONAL_ESCURSIONE = "getAllOptionalEscursione";
	public static final String ACTION_GET_ALL_STATO_ESCURSIONE = "getAllStatoEscursione";
	public static final String ACTION_ANNULLA_ESCURSIONE = "annullaEscursione";
	public static final String ACTION_GET_ALL_ISCRITTI_FROM_ESCURSIONE = "getAllIscrittiFromEscursione";
	public static final String ACTION_ANNULLA_ISCRIZIONE = "annullaIscrizione";

	// admin sistema
	public static final String ACTION_NUOVO_MDE = "nuovoManagerDiEscursione";
	public static final String ACTION_LISTA_MDE = "listaManagerDiEscursione";
	public static final String ACTION_GET_ALL_ESCURSIONI = "getAllEscursioni";
}
