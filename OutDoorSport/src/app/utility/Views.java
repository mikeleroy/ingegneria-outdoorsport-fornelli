package app.utility;

/**
 * Interfaccia che contiene tutte le costanti chiavi-valore per eseguire le
 * view che il front controller deve mandare al dispatcher 
 * 
 * @author michele fornelli
 *
 */


public interface Views {

	
	//Utente
	public static final String VIEW_LOGIN = "VIEW_LOGIN";
	public static final String VIEW_REGISTRA_UTENTE = "VIEW_REGISTRA_UTENTE";
	public static final String VIEW_MODIFICA_ACCESSO = "VIEW_MODIFICA_ACCESSO";
	public static final String VIEW_MODIFICA_SRC = "VIEW_MODIFICA_SRC";
	public static final String VIEW_RECUPERA_PASSWORD = "VIEW_RECUPERA_PASSWORD";
	public static final String VIEW_NUOVA_PASSWORD = "VIEW_NUOVA_PASSWORD";
	
	//Partecipante
	public static final String VIEW_DASHBOARD_PARTECIPANTE = "VIEW_DASHBOARD_PARTECIPANTE";
	public static final String VIEW_TROVA_ESCURSIONI_PAR = "VIEW_TROVA_ESCURSIONI_PAR";
//	public static final String VIEW_ISCRIZIONI_ESCURSIONI_PAR = "VIEW_ISCRIZIONI_ESCURSIONI_PAR";
	public static final String VIEW_ESCURSIONI_PAR = "VIEW_ESCURSIONI_PAR";
	public static final String VIEW_PROFILO_PAR = "VIEW_PROFILO_PAR";
	public static final String VIEW_MODIFICA_PAR = "VIEW_MODIFICA_PAR";
	public static final String VIEW_ISCRIZIONE_ESCURSIONE = "VIEW_ISCRIZIONE_ESCURSIONE";
	public static final String VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE = "VIEW_MODIFICA_ISCRIZIONE_ESCURSIONE";
	public static final String VIEW_SELEZIONA_OPTIONAL_ISCRIZIONE = "VIEW_SELEZIONA_OPTIONAL_ISCRIZIONE";
	public static final String VIEW_MODIFICA_OPTIONAL_ISCRIZIONE = "VIEW_MODIFICA_OPTIONAL_ISCRIZIONE";


	//Manager Escursione
	public static final String VIEW_DASHBOARD_MAN_ESCURSIONE = "VIEW_DASHBOARD_MAN_ESCURSIONE";
	public static final String VIEW_PROFILO_MANAGER_ESC = "VIEW_PROFILO_MANAGER_ESC";
	public static final String VIEW_LISTA_ESCURSIONI = "VIEW_LISTA_ESCURSIONI";
	public static final String VIEW_NUOVA_ESCURSIONE = "VIEW_NUOVA_ESCURSIONE";
	public static final String VIEW_LISTA_OPTIONAL = "VIEW_LISTA_OPTIONAL";
	public static final String VIEW_LISTA_PARTECIPANTI = "VIEW_LISTA_PARTECIPANTI";
	public static final String VIEW_MODIFICA_MANAGER_ESC = "VIEW_MODIFICA_MANAGER_ESC";
	public static final String VIEW_DETTAGLI_ESCURSIONE_MDE = "VIEW_DETTAGLI_ESCURSIONE_MDE";
	public static final String VIEW_ISCRITTI_ESCURSIONE = "VIEW_ISCRITTI_ESCURSIONE";
	public static final String VIEW_MODIFICA_ESCURSIONE = "VIEW_MODIFICA_ESCURSIONE";

	
	//Manager Sistema
	public static final String VIEW_DASHBOARD_MAN_SISTEMA = "VIEW_DASHBOARD_MAN_SISTEMA";
	public static final String VIEW_PROFILO_MANAGER_SIS = "VIEW_PROFILO_MANAGER_SIS";
	public static final String VIEW_ESCURSIONI_SIS = "VIEW_ESCURSIONI_SIS";
	public static final String VIEW_LISTA_MANAGER_SIS = "VIEW_LISTA_MANAGER_SIS";
	public static final String VIEW_AGGIUNGI_MANAGER_ESC = "VIEW_AGGIUNGI_MANAGER_ESC";
	public static final String VIEW_MODIFICA_MANAGER_SIS = "VIEW_MODIFICA_MANAGER_SIS";
	public static final String VIEW_DETTAGLI_ESCURSIONE_SIS = "VIEW_DETTAGLI_ESCURSIONE_SIS";
	public static final String VIEW_ISCRITTI_ESCURSIONE_SIS = "VIEW_ISCRITTI_ESCURSIONE_SIS";


}
